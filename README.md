## End-to-end(functional) autotests are designed using WebdriverIO tool.  

Libraries which are helpful and used when writing code, running tests and getting results:

- `mocha` - JavaScript test framework for asynchronous testing;
- `chai` - assert/expect library for checking and compering results;
- `wdio` - test runner which helps to manage test suites;
- `chance` - helping tool for testing random testing values;
- `allure` - tool for displaying test results
- `applitools` - tool for ui testing (comparing differences on screenshots)
 
### Below is a table with a current end-to-end tests coverage:

| #  | Functionality:                             | Description:                         |Run periodicity:|Comments:        |
| ---|:-------------------------------------------|:-------------------------------------|:---------------|:----------------|
|  1 |MG Admin dashboard functionality MG Admin views medical group dashboard|Checking for proper working:<p>- event counters, <p>-patient archivation/unarchivation, <p>- assigning/unassigning Clinical Trial to patient,<p>- adding/removing Notes, <p>- main links and buttons|After deploying new functionality or refactoring works|  
|  2 |Amsler test math: calculation of mean deviation coefficient Amsler Test|Checking the correct calculation of Mean Deviation coefficient using formula: Mean Deviation = Sum of each (Type Size * on Type coefficient)|After refactoring works which could be related to Amsler test |Currently for testing results on C100 grid for one of eyes   
|  3 |Fields validation rules Objects|Checking field validation rules for following objects:<p> `Clinical Trial; CRO Company; CRO Member; Medical Group; Medical Group Member Patient`|After refactoring works which could be related to fields and its validation rules|  
|  4 |Creation of base Objects|<p>Checking of creation: <p>`Clinical Trial; CRO Member; Medical Group` *by CRO Admin* <p> and <p>`Medical Group Member; Patient` by *Medical Group Admin* |After refactoring works which could be related to fields and its validation rules|  
|  5 |Adding and configuring Rules|<p>**Checking of**:<p> - Adding Informative/Actionable Rules to CT from Rules Library;<p>- Errors when creating/configuring Rule;<p>- Email variables copying/pasting; Reminders for Actionable Rules appearing/changing/correct saving;<p>- Modal windows with comments when updating/activating/deactivating Rules; <p>- Errors for text area of modal windows |After refactoring works which could be related to Rules functionality|  
|  6 |Checking Event object functionality|<p>**Checking of**:<p> - proper Events filters working, by: Priority, Type, Priority+Type, Status, "Show invalid?";<p>- correct "My unprocessed Events" number after changing status from "Pending" and after changing Assignee; <p>- correct changing/saving of Priority;<p>- leaving comments and its correct recording in "Audit log" and "Change log";<p>- link of Rule in "Additional Info" tab is correct;<p>- "notification bell number" for MG user when changing status from "Pending" and back<p>- proper changing Event type from "Actionable" to "Informative" and vice versa;<p>- MG Member cannot change Event type;<p>- proper behaviour of Reminders block when changing Event type "Actionable->Informative->Actionable";<p>- "Invalid Event status" functionality based on MG User marks the Event as InValid, including UI tests for "blurring";<p>- MG Member (not Admin) cannot change Event status to "Completed" if Assignee is other MG Member |After refactoring works which could be related to Events functionality|  
|  7 |CheckingAuditLog object functionality|<p>Checking of AuditLog page properties: <p>1). Actions (Login, Update profile, Create, Create comment, Update, Update status, Archive, Unarchive);<p> 2). Filter combinations ("Who", "Who"+"Action->Login", "Who"+"Action->Update Profile"+"Entity->MGMember/MG") |After any refactoring works on AuditLog page|  
|  8 |Filter objects|<p>- assigning/unassigning MG Member to CT and checking that filter MG Member by Clinical Trial works properly; <p>- searching by existing/not existing Patient name and checking results |After refactoring works with MG Member, CT objects, filter functionality|  
|  9 |9.1. "Not Active" statuses of objects|<p>Checking that after unassigning MG from CT by CRO Admin:<p>- MG becomes "Not Active" for CT on CRO Admin side; <p>- CT becomes "Not Active" for MG on MG Admin side <p>|After any permission/statuses updates for system objects|Awaiting for validation updates: [RKS-350 "Add confirm for all UnAssign action trough all system"](https://jira.gera-it.com/browse/RKS-350)  
|    |9.2. Assigning/UnAssigning Patient to MG Member functionality|<p> -Assign/unAssign Patient from different profiles;<p> -Check "Primary" and "Backup" labels assigning;<p> -Checking that labels are not resetting after page refreshing;<p> -Verify the ability for Patient having only one Responsible Member with a "Primary" label|After any updates in "assign MG Member to Patient" functionality|
	
## To install and run all the tests use:

````
npm install
````
````
./node_modules/.bin/wdio wdio.conf.js;
````
_or_
````
 ./node_modules/.bin/wdio wdio.standalone.conf.js 
````
_(for standalone mode)_

Moreover, all the tests are divided into suites, like `clinical_trial`,`patient`, `mg_dashboard`, `objects` etc.  
The list of all available suites can be found in `wdio.conf.js`/`wdio.standalone.conf.js` files
##### To run a specific suite use, ex.: 
`./node_modules/.bin/wdio wdio.conf.js --suite objects`
##### To run a single test use, ex.: 
`./node_modules/.bin/wdio wdio.conf.js --spec ./src/specs/objects/rule_object/AddRule.js`
