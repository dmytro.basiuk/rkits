import { arrowDown, pageDown, pollingTime, timeout } from '../model/Constants';

export default class BaseElement {
    constructor(value) {
        this.value = value;
    }

    get element() {
        return browser.element(this.value);
    }

    get elements() {
        return browser.elements(this.value);
    }

    shouldBeVisible() {
        this.element.waitForEnabled();
        this.element.waitForExist();
        this.element.waitForVisible();
        this.element.scroll(this.element);
        return this.element;
    }

    shouldNotExist(ms, reverse) {
        this.element.waitForExist(ms, reverse);
        return this.element;
    }

    shouldAllNotExist(ms, reverse) {
        this.elements.waitForExist(ms, reverse);
        return this.elements;
    }

    click() {
        this.shouldBeVisible().click();
    }

    actionLeftClick(x, y){
        this.shouldBeVisible().leftClick(x, y);
        return this;
    }

    shouldNotBeEnabled(ms, reverse){
        this.element.waitForEnabled(ms, reverse);
        return this.element;
    }

    shouldHaveSomeText() {
        this.element.waitForText();
        return this.element;
    }

    shouldBeAllVisible() {
        this.elements.waitForVisible();
        return this.elements;
    }

    scrollTo(element){
        this.element.scroll(element);
        return this.element;
    }

    elementExists() {
        this.element.waitForExist();
        return this.element;
    }

    ifElementExists() {
        this.element.isExisting();
        return this.element;
    }

    enterValue(text) {
        this.shouldBeVisible().setValue(text);
    }

    getValueText() {
        return this.shouldBeVisible().getText();
    }

    getAllValuesTexts() {
        return this.shouldBeAllVisible().getText();
    }

    getAllAttributesValues(attributeName) {
        return this.shouldBeAllVisible().getAttribute(attributeName);
    }

    getAttributeValue(attributeName) {
        return this.shouldBeVisible().getAttribute(attributeName);
    }

    getPageSource(){
        return this.shouldBeVisible().getHTML();
    }

    getCSS(propertyName){
        return this.shouldBeVisible().getCssProperty(propertyName);
    }

    getUrl() {
        return this.shouldBeAllVisible().getUrl();
    }

    addCharacter(key, times) {
        this.click();
        browser.keys(key.repeat(times));
    }

    pushTheButton(button){
        //this.click();
        browser.keys(button);
        return this;
    }

    //To clear entire input field use the function below:
    clearEntireField(){
        return this.shouldBeVisible().clearElement();
    }

    /* this function will get value of passed locator
    verify it in passed condition until spent polling time will reach timeout
    if condition will be true before loop reaches max timeout - loop will end
    it will return value of passed locator nevertheless so you could verify it*/
    waitUntil(condition, value) {
        let spent = 0;
        do {
            browser.pause(pollingTime);
            spent += pollingTime;
        } while (!condition(this.getValueText(), value) && spent < timeout);
        return this.getValueText();
    }
}
