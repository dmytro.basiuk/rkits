// export const actionOfLastRecord = '//tbody/tr[1]/td[2]/span';
// export const actionOfPreLastRecord = '//tbody/tr[2]/td[2]/span';

export const auditLogFilters = {
    entity: {
        select: 'select#q_trackable_type_eq'
    },
    action: {
        select: 'select#q_action_eq'
    },
    who: {
        select: "//select[@id='q_by_who']"
    },
    applyButton: '//button[text()="Apply"]',
    resetButton: '//a[text()="Reset"]',
    noResultsMessage: '//h3[text()="No records found"]'
};

export const auditLogTable = {
    recordDescription: '//tbody/tr/td/div',
    actionOfLastRecord: '//tbody/tr[1]/td[2]/span',
    actionOfPreLastRecord: '//tbody/tr[2]/td[2]/span',
    whoValues: '//tbody/tr/td[3]/a'
};
