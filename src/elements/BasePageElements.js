export const chooseFileButton = 'div[class="custom-file-upload"]';
export const pictures = ['photo1','photo2','photo3','photo4','photo5','photo6','photo7','photo8','photo9','photo10','photo11','photo12','photo13','photo14', 'photo15'];

//Filter menu
export const filterButton = 'a[href="#collapseFilters"]';
export const applyFilterButton = 'input[value="Apply"]';
export const resetFilterButton = '//div/a[text()="Reset"]';

export const switchInvalidEventFilter = 'div[class*="switch-button"] label';
export const actionableEventIcon = 'span[class*="event-icon"]';
