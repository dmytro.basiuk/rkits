export const ctNameField = 'input#clinical_trial_name';
export const ctDetailsField = 'textarea#clinical_trial_details';
export const ctObjectiveField = 'textarea#clinical_trial_objective';
export const ctRecruitmentGoalField = 'input#clinical_trial_recruitment_goal';
export const submitButton = 'input[value="Submit"]';
export const ctWasCreatedMessage = 'Clinical trial was successfully created.';
