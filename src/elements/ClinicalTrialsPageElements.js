//for CRO Admin
export const actionCTButton = 'a[data-toggle="dropdown"]>div[class*="mdi mdi-more"]:first-of-type';
export const linkToCT = 'div[class="card-body"] tr:first-of-type>td:first-of-type>a';

//for MG Admin
export const firstCT = 'td>a[href="/clinical_trials/1"]';
export const secondCT = 'td>a[href="/clinical_trials/2"]';
