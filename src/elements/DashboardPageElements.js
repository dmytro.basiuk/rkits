export const dashboardTitle = '//div[@class="dashboard-head-title"]';
export const addPatientButton = '//a[@href="/patients/new"]/div';
export const addMGMemberButton = '//a[@href="/members/new"]/div';
export const viewAllPatientsLink = '//div[@class="dashboard-head-patients"]/a[text()="View All"]';
export const patientName = 'div[class="card-header"]>div>a';
export const firstPatientName = 'div.row>div:first-of-type div[class="card-header"]>div>a[href*="patients"]';
export const firstPatientNotes = '//div[@class="row"]/div[1]//div[@class="test-score-block"]//a[@class="notes-popover ellipsis"]';
export const patientNameAction = 'div[class="card-header"]>div[class*="tools"]>a';
export const actionForLastPatient = 'div[class="row"] div[class="card patient-card"]:first-child div[class="tools"]';
export const assignedCTs = 'strong+a[href*="clinical_trials"]';



