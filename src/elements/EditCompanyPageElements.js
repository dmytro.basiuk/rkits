export const companyNameField = 'input#company_name';
export const companyAddressField = 'input#company_address';
export const companyDetailsField = 'textarea#company_details';
export const submitButton = 'input[value="Submit"]';
