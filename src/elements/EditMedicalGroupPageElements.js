export const mgNameField = 'input#medical_group_name';
export const mgAddressField = 'input#medical_group_address';
export const mgDetailsField = 'textarea#medical_group_details';
export const mgNPIField = 'input#medical_group_npi';
export const mgSubmitButton = 'input[value="Submit"]';
