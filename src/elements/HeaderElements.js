export const mainHeaderIcon = 'div[class="be-navbar-header"]';
export const headerPageTitle = 'div[class="page-title"]';
export const notificationBell = 'span[class="badge badge-pill badge-warning"]';
export const settingsButton = 'a>span[class$=mdi-more]';
export const editCompanyButton = '//a[@class="connection-item"]/span[text()="Edit Company"]';
export const membersLink = 'a[href="/members"]';
export const medicalGroupsLink = 'a[href="/medical_groups"]';
export const patientsLink = 'a[href="/patients"]';
export const clinicalTrialsLink = 'a[href="/clinical_trials"]';
export const auditLogLink = 'a[href="/activities"]';
export const addButton = '//a[@role="button"]/b[text()="Add"]';
export const headerAccountBlock = 'div[class="nav-user-block"]';
export const headerAccountUserName = 'div[class="nav-user-name"]';
export const headerAccountLink = '//a[contains(text(), "Account")]';
export const headerAccountSignOutLink = '//a[contains(text(), "Sign out")]';

