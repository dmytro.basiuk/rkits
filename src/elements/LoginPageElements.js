export const loginEmailField = 'div>#user_email';
export const loginPasswordField = 'div>#user_password';
export const loginButton = 'div>input[value="Log in"]';
