export const MgMemberFirstNameField = 'input#medical_group_member_first_name';
export const MgMemberLastNameField = 'input#medical_group_member_last_name';
export const MgMemberEmailField = 'input#medical_group_member_email';
export const MgMemberPhoneField = 'input#medical_group_member_phone';
export const MgMemberAddressField = 'input#medical_group_member_address';
export const MgMemberPasswordField = 'input#medical_group_member_password';

export const MgMemberSelectStaffType = '//select[@id="medical_group_member_staff_type"]';
export const MgMemberSelectedStaffType = '//select[@id="medical_group_member_staff_type"]/option[@selected="selected"]';
export const MgMemberStaffTypeDoctor = '//select[@id="medical_group_member_staff_type"]/option[text()="Doctor"]';
export const MgMemberStaffTypeProvider = '//select[@id="medical_group_member_staff_type"]/option[text()="Provider"]';
export const MgMemberStaffTypeStaff = '//select[@id="medical_group_member_staff_type"]/option[text()="Staff"]';

export const MgMemberSelectRole = '//select[@id="medical_group_member_role"]';
export const MgMemberSelectedRole = '//select[@id="medical_group_member_role"]/option[@selected="selected"]';
export const MgMemberAdminRole = '//select[@id="medical_group_member_role"]/option[text()="Medical Group Admin"]';
export const MgMemberMemberRole = '//select[@id="medical_group_member_role"]/option[text()="Medical Group Member"]';

export const submitButton = 'input[value="Submit"]';
export const backButton = 'a[class="btn btn-secondary"]';
