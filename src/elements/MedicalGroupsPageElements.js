export const actionMGGroupButton = 'a[data-toggle="dropdown"]>div[class*="mdi mdi-more"]:first-of-type';
export const mgWasCreatedMessage = 'Medical group was successfully created.';
