export const upperMemberName = 'tbody>tr:first-of-type>td:first-of-type>a[href*="members"]';
export const allMemberNames = 'tbody>tr>td:first-of-type>a[href*="members"]';
export const upperMemberNumberOfCTAssignedLink = 'tbody>tr:first-of-type a[class*="btn"]';

export const filterMGByCTInput = 'input[id*="clinical_trials-selectized"]';
export const applyMGFilterButton = '//button[text()="Apply"]';
export const resetMGFilterButton = '//a[@href="/members"][text()="Reset"]';

export const actionMGMemberButton = 'a[data-toggle="dropdown"]>div[class*="mdi mdi-more"]:first-of-type';
export const mgMemberCreatedMessage = 'Member was successfully created.';
