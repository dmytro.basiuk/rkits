export const PatientFirstNameField = 'input#medical_group_patient_first_name';
export const PatientLastNameField = 'input#medical_group_patient_last_name';
export const PatientEmailField = 'input#medical_group_patient_email';
export const PatientPhoneField = 'input#medical_group_patient_phone';
export const PatientAddressField = 'input#medical_group_patient_address';

export const PatientSelectGender = '//select[@id="medical_group_patient_gender"]';
export const PatientSelectedGender = '//select[@id="medical_group_patient_gender"]/option[@selected="selected"]';
export const PatientMaleGender = '//select[@id="medical_group_patient_gender"]/option[text()="Male"]';
export const PatientFemaleGender = '//select[@id="medical_group_patient_gender"]/option[text()="Female"]';

export const PatientSelectStudyEye = '//select[@id="medical_group_patient_study_eye"]';
export const PatientSelectedStudyEye = '//select[@id="medical_group_patient_study_eye"]/option[@selected="selected"]';
export const PatientStudyEyeBoth = '//select[@id="medical_group_patient_study_eye"]/option[text()="Both"]';
export const PatientStudyEyeRight = '//select[@id="medical_group_patient_study_eye"]/option[text()="Right"]';
export const PatientStudyEyeLeft = '//select[@id="medical_group_patient_study_eye"]/option[text()="Left"]';
export const PatientNotesField = 'textarea#medical_group_patient_notes';

export const submitButton = 'input[value="Submit"]';
