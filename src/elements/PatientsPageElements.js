export const actionPatientsButton = 'a[data-toggle="dropdown"]>div[class*="mdi mdi-more"]:first-of-type';
export const patientsTitle = 'div[class="list-head-title"]';
export const backToDashboardButton = 'div[class="head-btn-back"]';
export const nextPageButton = 'a[rel="next"]';
export const viewGrid = 'a[href*="patients?view_mode=grid"]';
export const viewList = 'a[href*="patients?view_mode=list"]';
export const patientSearchField = 'input#q_search_by_patient';
export const patientSearchApplyButton = '//button[text()="Apply"]';
export const patientSearchResetButton = '//a[@href="/patients"][text()="Reset"]';
export const noSearchResultsMessage = '//h3[text()="No records found"]';
export const patientWasCreatedMessage = 'Patient was successfully created.';

