//Modal window elements
export const modalStatusCommentField = 'textarea#event_state_comment_comment';
export const modalCommentField = 'textarea#comment_comment';
export const modalSubmitButton = 'button[type="submit"]';

//Main Event info block
export const notActivePriorityButton = 'span[class="priority"] a[class="btn btn-sm btn-secondary "]>div+div';
export const editAssigneeButton = 'span[class*="assignee event-inline-edit"] span';
export const selectSecondAssignee = 'select[id="event_assignee_id"]>option:nth-of-type(2)';
export const ctRecordButton = '//a[@class = "btn btn-sm btn-primary"][text()="CT Record"]';
export const submitAssigneeButton = 'button[type="submit"]';
export const restoreEventButton = '//span/a[text()="Restore"]';
export const disabledCompleteButton = 'button[class*="disabled"]>div[class="text-success"]';
//selectors for UI tests
//export const eventCardHeader = 'div[class="card"][id*="event"]>div[class="card-header"]';

//Additional info
export const eventRuleLink = 'div[class*="event"]>a[href*="rules"]';
export const actionableDisabledButton = '//div[@class="btn-group"]/a/div[text()="Actionable"]';
export const actionableEnabledButton = '//button/div[text()="Actionable"]';
export const informativeDisabledButton = '//div[@class="btn-group"]/a[text()="Informative"]';
export const markAsInvalidButton = '//span/a[text()="Mark as Invalid"]';

//Reminders block
export const remindersBlock = 'div[class="card card-reminders"]';
export const eventReminderPendingHours = '//div[@class="event-col-title"]/strong[text()="Pending"]/parent::div/following-sibling::div//strong';
export const eventReminderProcessingHours = '//div[@class="event-col-title"]/strong[text()="Processing"]/parent::div/following-sibling::div//strong';

export const eventReminderPendingCurrentOff = '//div[@class="event-col-title"]/strong[text()="Pending"]/parent::div/following-sibling::div//a[@class="btn btn-sm btn-secondary active"]/span[text()="Off"]';
export const eventReminderProcessingCurrentOff = '//div[@class="event-col-title"]/strong[text()="Processing"]/parent::div/following-sibling::div//a[@class="btn btn-sm btn-secondary active"]/span[text()="Off"]';
export const eventReminderPendingTurnOnButton = '//div[@class="event-col-title"]/strong[text()="Pending"]/parent::div/following-sibling::div//a[@class="btn btn-sm btn-secondary active"][text()="On"]';
export const eventReminderProcessingTurnOnButton = '//div[@class="event-col-title"]/strong[text()="Processing"]/parent::div/following-sibling::div//a[@class="btn btn-sm btn-secondary active"][text()="On"]';

export const showLastStatusCommentButton = 'a[aria-controls*="comment_update"]';
export const lastStatusComment = 'div[id*="comment_update"] p:first-of-type';
export const lastComment = 'div[class="timeline-content"]:first-of-type div[class="timeline-summary"]>p:nth-of-type(2)';
export const leaveCommentButton = 'a[href*="comments/new"]';

//Messages
export const eventWasUpdatedMessage = 'Event was successfully updated.';
export const eventCommentWasAddedMessage = 'Comment was successfully added.';


