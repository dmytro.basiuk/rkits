//export const showLastEventButton = 'tr[id*=event]:first-of-type>td[class="actions"]>a';
export const showLastEventButton = '//tr[contains(@id,"event")][1]//a[text()="Show"]';
//export const editLastEventButton = 'tr[id*=event]:nth-of-type(1)>td[class="actions"]>button[id*=edit]';
export const editLastEventButton = '//tr[contains(@id,"event")][1]//button[contains(@id,"edit")]';
export const editLastEventPriorityHigh = 'div[class*="priority event"] div[class="text-danger"]';

export const lastEventPriority = 'tr[id*=event]:nth-of-type(1) td[class="priority-label"] span[class*="badge"]';
export const eventsPriorities = 'tr[id*=event] td[data-label="Priority"] span[class*="badge"]';

export const lastEventStatus = 'tr[id*=event]:nth-of-type(1) span[data-title*="Status: "]';
export const eventsStatuses = 'tr[id*=event] span[data-title*="Status: "]';


