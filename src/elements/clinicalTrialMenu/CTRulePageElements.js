export const ruleNameField = 'input[name="rule[name]"]';
export const ruleRecipientsField = 'div[class*="selectize-input"]';
export const ruleAssigneeField = 'select#rule_assignee_id';

export const ruleRemindersBlockHidden = 'fieldset[class="form-group rule-reminders"][style="display: none;"]';
export const ruleRemindersBlockVisible = 'fieldset[class="form-group rule-reminders"]';


export const ruleEmailVariablesLink = 'a[aria-controls="collapseVariables"]';
export const ruleVariableRecipientName = '{recipient_name}';

export const ruleEmailSubjectPatientField = 'input#rule_email_subject_patient';
export const submitRuleButton = 'input[type="submit"]';
export const submitRuleChangesButton = 'button[type="submit"]';
