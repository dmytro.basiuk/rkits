export const rulesLibraryButton = '//a[text()="Library"]';
export const addRuleButton = '//td/a[text()="Add Rule"]';
export const lastRuleActionable = 'tr:nth-of-type(1) span[data-title="Actionable rule"]';
export const lastRuleDeactivated = 'tr:nth-of-type(1) span[class="badge badge-secondary"]';
export const lastRuleAction = 'tr:nth-of-type(1) a[class="dropdown-toggle"]';


//Messages
export const ruleWasCreatedMessage = 'Rule was successfully created.';
export const ruleWasUpdatedMessage = 'Rule was successfully updated.';
export const ruleNameIsAlreadyTakenMessage = 'Name has already been taken';
export const ruleAssigneeCantBeBlankMessage = "Assignee can't be blank";
export const ruleRecipientsCantBeBlankMessage = "Recipients can't be blank";
export const ruleSuccessActionMessage = 'Rule was successfully';


//Configuring Rule - Comment window
export const ruleCommentField = 'textarea#rule_state_comment_comment';
export const ruleCommentSubmitButton = 'button[type="submit"]';
export const ruleCommentCancelButton = 'a[data-dismiss="modal"]';
//Comment window messages
export const ruleCommentEmptyError = "Comment can't be blank";
export const ruleCommentTooLongError = 'Comment is too long (maximum is 500 characters)';
