export const changeAssignedObjectButton = 'a[href*="change"]';

export const upperCTLink = 'tr[id*="clinical_trial"]:first-of-type td:first-of-type>a';
export const upperCTUnAssignButton = '//tr[contains(@id,"clinical_trial")][1]//a[text()="UnAssign"]';
export const upperCTAssignButton = '//tr[contains(@id,"clinical_trial")][1]//a[text()="Assign"]';
