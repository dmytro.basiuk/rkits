export const patientMenuPatientName = 'div[class="page-head-title"]';
export const patientMenuUpperCTTitleLink = '//tr[contains(@id,"clinical_trial")][1]/td[1]/a';
export const patientMenuUpperCTAssignButton = '//tr[contains(@id,"clinical_trial")][1]//a[text()="Assign"]';
export const patientMenuUpperCTUnAssignButton = '//tr[contains(@id,"clinical_trial")][1]//a[text()="UnAssign"]';

export const patientMenuAssignCTButton = '//td[@class="actions"]/a[text()="Assign"]';
export const patientMenuUnAssignCTButton = '//td[@class="actions"]/a[text()="UnAssign"]';



export const patientMenuChangeCTButton = 'a[href*="change"]';
