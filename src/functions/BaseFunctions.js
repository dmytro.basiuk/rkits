export function openLoginPage() {
    browser.windowHandleSize({ width: 1920, height: 1080 });
    //browser.windowHandleSize({ width: 1366, height: 768 });
    return browser.url('/users/sign_in');
}

export function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

export function performModalWindowAction(action) {
    browser.pause(2000);
    return action;
}

export function refreshingCurrentPage(){
    browser.refresh();
    browser.pause(1000);
    return this;
}
