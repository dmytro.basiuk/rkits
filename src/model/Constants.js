// while writing automated tests or anywhere else you need anything random.
const chance = require('chance').Chance();
//const chance = new Chance();

import {pictures} from '../elements/BasePageElements';

export const threshold = 3;
export const timeout = 15000;
export const pollingTime = 500;

export const random3Figures = Math.floor((Math.random() * 900) + 1).toString();
export const random23Figures = '7'.repeat(23);
export const random10Characters = 'u'.repeat(55);
export const random55Characters = 'p'.repeat(55);
export const random56Characters = 'w'.repeat(56);
export const random76Characters = 'a'.repeat(76);
export const random75Characters = 'b'.repeat(75);
export const random256Characters = 'c'.repeat(256);
export const randomFirstName2 = chance.first();
export const randomFirstName = chance.first();
export const randomFirstMaleName = chance.first({gender: 'male'});
export const randomFirstFemaleName = chance.first({gender: 'female'});
export const randomLastName = chance.last();
export const randomLastName2 = chance.last();
export const randomClinicalTrialName = 'Clinical Trial Test '+chance.integer({ min: 1, max: 999999999 });
export const randomMedicalGroupName = 'Medical Group Test '+chance.integer({ min: 1, max: 999999999 });
export const randomCTRuleTitle = 'Rule Title '+chance.integer({ min: 1, max: 999999999 });
export const randomCTRuleTitle2 = 'Rule Title '+chance.integer({ min: 1, max: 999999999 });
export const randomCTRuleTitle3 = 'Rule Title '+chance.integer({ min: 1, max: 999999999 });
export const randomCTRuleTitle4 = 'Rule Title '+chance.integer({ min: 1, max: 999999999 });
export const random1Character = Math.random().toString(36).substr(2, 1);
export const random2Characters = Math.random().toString(36).substr(2, 2);
export const randomSentence = chance.sentence();
export const randomSentence2 = chance.sentence();
export const random501Characters = chance.string({ length: 501, pool: 'ab cdefg' });
export const randomPassword8Figures = chance.integer({ min: 11111111, max: 18765432 });
export const testName = 'Default company';
export const randomEmail = 'test'+Math.floor((Math.random() * 900000000) + 1)+'@example.com';
export const randomEmail2 = 'test'+Math.floor((Math.random() * 900000000) + 1)+'@example.com';
export const randomPhoneNumber = '+1'+Math.floor((Math.random() * 9000000000) + 1);
export const randomPicture = pictures[Math.floor(Math.random() * pictures.length)];
export const randomPicture2 = pictures[Math.floor(Math.random() * pictures.length)];
export const testAddress = '4002 Wallace St, San Diego, CA 92110, USA';
export const testNPINumber = Math.floor((Math.random() * 9000000000) + 1).toString();
export const testDetails = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt\n' +
    'ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet';
export const ctTestDetails = 'test details for CT';
export const ctTestObjective = 'test objective for CT';
export const edgeRecruitmentGoalNumber = '999999999';
export const pageDown = '\uE00F';
export const arrowDown = '\uE015';
