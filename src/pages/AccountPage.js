import BasePage from './BasePage';
import Element from '../components/Element';
import {
    MgMemberAdminRole,
    MgMemberMemberRole,
    MgMemberSelectedRole,
    MgMemberSelectedStaffType,
    MgMemberSelectRole,
    MgMemberSelectStaffType,
    MgMemberStaffTypeDoctor,
    MgMemberStaffTypeProvider,
    MgMemberStaffTypeStaff,
    submitButton
} from '../elements/MGMemberPageElements';
import {
    PatientSelectedGender,
    PatientSelectedStudyEye,
    PatientSelectStudyEye,
    PatientSelectGender,
    PatientStudyEyeLeft, PatientStudyEyeRight, PatientStudyEyeBoth
} from '../elements/PatientPageElements';

export default class AccountPage extends BasePage {



}
