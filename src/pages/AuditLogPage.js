import BasePage from './BasePage';
import Element from '../components/Element';
import { auditLogFilters } from '../elements/AuditLogPageElements';

export default class AuditLogPage extends BasePage {

    static filterRecordsInAuditLogByWho(name){
        Element.of(auditLogFilters.who.select).click();
        Element.of(auditLogFilters.who.select+`/option[text()="${name}"]`).click();
        Element.of(auditLogFilters.applyButton).click();
        return this;
    }

    static filterRecordsInAuditLogByAction(action){
        Element.of(auditLogFilters.action.select).click();
        Element.of(auditLogFilters.action.select+`>option[value="${action}"]`).click();
        Element.of(auditLogFilters.applyButton).click();
        return this;
    }

    static filterRecordsInAuditLogByEntity(entity){
        Element.of(auditLogFilters.entity.select).click();
        Element.of(auditLogFilters.entity.select+`>option[value="${entity}"]`).click();
        Element.of(auditLogFilters.applyButton).click();
        return this;
    }



}
