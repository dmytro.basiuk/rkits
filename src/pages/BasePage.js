import Element from '../components/Element';
import { addButton, headerAccountBlock, headerAccountLink, headerAccountSignOutLink } from '../elements/HeaderElements';
import { chooseFileButton } from '../elements/BasePageElements';
import { lastComment } from '../elements/clinicalTrialMenu/CTEventPageElements';
import { submitButton } from '../elements/ClinicalTrialPageElements';

export default class Page {

    static clickTheButton(button){
        return Element.of(button).click();
    }

    elementIsDisplaying(element){
        Element.of(element).shouldBeVisible();
        return this;
    }

    elementsAreDisplaying(element){
        Element.of(element).shouldBeAllVisible();
        return this;
    }

    elementIsExisting(element){
        Element.of(element).elementExists();
        return this;
    }

    elementIsNotExisting(element){
        Element.of(element).shouldNotExist(3000, true);
        return this;
    }

    elementsAreNotExisting(element){
        Element.of(element).shouldAllNotExist(3000, true);
        return this;
    }

    goToHeaderCategory(category){
        Element.of('span[class="icon mdi mdi-more"]').click();
        Element.of(category).click();
        return this;
    }

    selectToAdd(object){
        Element.of(addButton).click();
        Element.of(`//ul//a[text()="${object}"]`).click();
        return this;
    }

    static pressSubmitButton() {
        Element.of(submitButton).click();
        return this;
    }

    static getTextOfElement(element){
        return Element.of(element).getValueText();
    }

    static getTextOfElements(elements){
        return Element.of(elements).getAllValuesTexts();
    }

    static getAttributeTextOfElement(element, attribute){
        return Element.of(element).getAttributeValue(attribute);
    }

    static getNumberOfElementText(element){
        const variable = Element.of(element).getValueText();
        const variableConverted = Number(variable);
        return variableConverted;
    }


    inputToField(field, inputString){
        Element.of(field).clearEntireField();
        Element.of(field).enterValue(inputString);
        return this;
    }

    static attachingFile(file){
        Element.of(chooseFileButton).shouldBeVisible();
        browser.chooseFile('input[type="file"]', file);
        //browser.pause(7000);
        return this;
    }

    static gettingPageSource(element){
        return Element.of(element).getPageSource();
    }

    verifySystemMessage(message){
        Element.of(`//h4[@id="flash_notice"][text()="${message}"]`).shouldBeVisible();
        return this;
    }

    verifySystemMessageContains(message){
        Element.of(`//h4[@id="flash_notice"][contains(text(),"${message}")]`).shouldBeVisible();
        browser.pause(1000);
        return this;
    }

    verifySystemErrorMessage(message){
        Element.of(`//div[@class="invalid-feedback"][text()="${message}"]`).shouldBeVisible();
        return this;
    }

    verifyAttributeEqual(inputedValue, changedTitle){
        expect(inputedValue).to.be.equal(changedTitle);
        return this;
    }

    verifyAttributeDeepEqual(inputedValue, changedTitle){
        expect(inputedValue).to.deep.equal(changedTitle);
        return this;
    }

    verifyAttributeNotEqual(inputedValue, changedTitle){
        expect(inputedValue).to.not.equal(changedTitle);
        return this;
    }

    verifyAttributeContains(inputedValue, changedTitle){
        expect(inputedValue).to.include(changedTitle);
        return this;
    }

    verifyAttributeIsGreater(inputedValue, changedValue){
        expect(changedValue).to.be.above(inputedValue);
        return this;
    }

    verifyErrorMessage(message){
        //Element.of(`input#company_name+div=${message}`).shouldBeVisible();
        Element.of(`//div[@class="invalid-feedback"][text()="${message}"]`).shouldBeVisible();
        return this;
    }

    static verifyAllValuesEqual(values, sortValue){
        values.forEach(function(el){
            expect(sortValue).to.be.equal(el);
        });
    }

    static verifyAllValuesContains(values, sortValue){
        values.forEach(function(el){
            expect(el).to.include(sortValue);
        });
    }

    //Precondition for using switching tabs functions: dashboard tab should be opened first, frontend - second
    switchToFrontEnd(){
        let windowHandles = browser.windowHandles();
        let frontEnd = windowHandles.value[1];
        browser.window(frontEnd);
        return this;
    }

    switchToDashboard(){
        let windowHandles = browser.windowHandles();
        let dashboard = windowHandles.value[0];
        browser.window(dashboard);
        return this;
    }

    cleanBrowserSession(){
        browser.deleteCookie();
        //browser.reload();
        return this;
    }

    countElements(locator){
        Element.of(locator).shouldBeVisible();
        let result = $$(locator).filter(function(entries) {
            return entries.isVisible();
        });
        return result.length;
    }

    goToAccountPage(){
        Element.of(headerAccountBlock).click();
        Element.of(headerAccountLink).click();
        return this;
    }

    loggingOut(){
        Element.of(headerAccountBlock).click();
        Element.of(headerAccountSignOutLink).click();
        return this;

    }
}


