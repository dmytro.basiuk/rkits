import BasePage from './BasePage';
import Element from '../components/Element';
import {
    CROmemberAdminRole,
    CROmemberMemberRole,
    CROmemberSelectedRole,
    CROmemberSelectRole,
    submitButton
} from '../elements/CROMemberPageElements';

export default class CROMemberPage extends BasePage {

    pressSubmitButton(){
        Element.of(submitButton).click();
        return this;
    }

    getCROMemberAddress(){
        return Element.of('input#company_member_address').getAttributeValue('value');
    }

    getCurrentCROMemberRole(){
        return Element.of('//select[@id="company_member_role"]/option[@selected="selected"]').getValueText();
    }

    changeMemberRole(){
        const initialSelectedRole = Element.of(CROmemberSelectedRole).getValueText();
        if(
            initialSelectedRole === 'Company Admin'){
            Element.of(CROmemberSelectRole).click();
            Element.of(CROmemberMemberRole).click();
        } else {
            Element.of(CROmemberSelectRole).click();
            Element.of(CROmemberAdminRole).click();
        }
        Element.of(submitButton).click();
    }

    changeToInitialCROMemberRole(initialMemberRole){
        Element.of(CROmemberSelectRole).click();
        Element.of(`//select[@id="company_member_role"]/option[text()="${initialMemberRole}"]`).click();
        Element.of(submitButton).click();
    }

}
