import BasePage from './BasePage';
import Element from '../components/Element';
import { submitButton } from '../elements/ClinicalTrialPageElements';

export default class ClinicalTrialPage extends BasePage {

    pressSubmitButton() {
        Element.of(submitButton).click();
        return this;
    }

    static getCTObjective(){
        return Element.of('textarea#clinical_trial_objective').getValueText();
    }

    static getCTRecruitmentGoal(){
        return Element.of('input#clinical_trial_recruitment_goal').getAttributeValue('value').toString();
    }
}
