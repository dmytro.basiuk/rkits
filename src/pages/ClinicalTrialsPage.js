import BasePage from './BasePage';
import Element from '../components/Element';
import { editCompanyButton, settingsButton } from '../elements/HeaderElements';
import { actionCTButton, linkToCT, secondCT } from '../elements/ClinicalTrialsPageElements';
import { nextPageButton } from '../elements/PatientsPageElements';

export default class ClinicalTrialsPage extends BasePage {

    goToEditCompany(){
        Element.of(settingsButton).click();
        Element.of(editCompanyButton).click();
        return this;
    }

    performActionWithLastClinicalTrial(action) {
        Element.of(actionCTButton).click();
        Element.of(`//ul[@class="dropdown-menu dropdown-add show"]/a[@class="dropdown-item"][text()="${action}"]`).click();
        return this;
    }

    getCTName(){
        return Element.of(linkToCT).getValueText().toString();
        //return Element.of('div[class="card-body"] tr:first-of-type>td:first-of-type>a').getValueText().toString();
    }

    getCTDetails(){
        return Element.of('div[class="card-body"] tr:first-of-type>td:nth-of-type(2)').getValueText();
    }

    goToCTDataPage(){
        Element.of(secondCT).click();
        Element.of('//ul[@class="sidebar-elements"]//span[text()="CT Data"]').click();
        return this;
    }

    static findCTAndClick(ct){
        while(Element.of('//div[@class="card-body"]//tr/td/a').shouldBeVisible()) {
            let pageSource = browser.getSource();
            if(pageSource.includes(ct) === true){
                Element.of(`//div[@class="card-body"]//tr/td/a[text()="${ct}"]`).click();
                break;
            }
            else (Element.of(nextPageButton).click());
        }
    }


}
