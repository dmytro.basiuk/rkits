import BasePage from './BasePage';
import Element from '../components/Element';
import {
    companyAddressField,
    companyDetailsField,
    companyNameField,
    submitButton
} from '../elements/EditCompanyPageElements';

export default class CompanyPage extends BasePage {

    pressSubmitButton(){
        Element.of(submitButton).click();
        return this;
    }

    getCompanyTitle(){
        return Element.of('div.page-title').getValueText().toString();
    }

    getCompanyAddress(){
        return Element.of('input#company_address').getAttributeValue('value');
    }

    getCompanyDetails(){
        return Element.of('textarea#company_details').getValueText();
    }

}

