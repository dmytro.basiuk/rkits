import BasePage from './BasePage';
import Element from '../components/Element';
import {
    assignedCTs,
    dashboardTitle,
    firstPatientName,
    patientName,
    patientNameAction
} from '../elements/DashboardPageElements';
import { patientMenuChangeCTButton } from '../elements/patientMenu/PatientClinicalTrialsElements';

export default class DashboardPage extends BasePage {

    static checkingDashboardTitle(){
        return Element.of(dashboardTitle).shouldBeVisible();
    }

    static getPatientName(){
        return Element.of(patientName).getValueText();
    }

    static getFirstPatientName(){
        return Element.of(firstPatientName).getValueText();
    }

    static getAssignedCTs(){
        const value = Element.of(assignedCTs).getValueText();
        const valueNumber = parseInt(value, 10);
        return valueNumber;
    }

    static assigningCTAction(action){
        Element.of(assignedCTs).click();
        Element.of(patientMenuChangeCTButton).click();
        Element.of(action).click();
        return this;
    }

    static performingPatientAction(action){
        Element.of(patientNameAction).click();
        Element.of(`${patientNameAction}+div>a[href*="${action}"]`).click();
        return this;
    }

    static getEventValue(status){
        return Element.of(`div[class*="content-around"]>a[data-title = "Status: ${status}"]`).getValueText();
    }

    static clickOnEventValue(status){
        return Element.of(`div[class*="content-around"]>a[data-title = "Status: ${status}"]`).click();
    }

    static checkStatusOfCT(ct, status){
        const ctStatus = Element.of(`//a[text()="${ct}"]/following-sibling::span`).getValueText();
        expect(ctStatus).to.be.equal(status);
        return this;
    }

}
