import BasePage from './BasePage';
import Element from '../components/Element';
import { mgSubmitButton } from '../elements/EditMedicalGroupPageElements';

export default class EditMedicalGroupPage extends BasePage {

    pressSubmitButton() {
        Element.of(mgSubmitButton).click();
        return this;
    }

    getMGName(){
        return Element.of('input#medical_group_name').getAttributeValue('value');
    }

    getMGAddress() {
        return Element.of('input#medical_group_address').getAttributeValue('value');
    }

    getMGDetails() {
        return Element.of('textarea#medical_group_details').getAttributeValue('value');
    }

    getMGNPINumber() {
        return Element.of('input#medical_group_npi').getAttributeValue('value').toString();
    }
}
