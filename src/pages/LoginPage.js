import Element from '../components/Element';
import BasePage from './BasePage';
import { loginButton, loginEmailField, loginPasswordField } from '../elements/LoginPageElements';

export function openLoginPage() {
    browser.windowHandleSize({ width: 1920, height: 1080 });
    return browser.url('/users/sign_in');
}

export default class LoginPage extends BasePage {

    login(email, password){
        Element.of(loginEmailField).enterValue(email);
        Element.of(loginPasswordField).enterValue(password);
        Element.of(loginButton).click();
        return this;
    };


}
