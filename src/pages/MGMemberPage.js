import BasePage from './BasePage';
import Element from '../components/Element';
import {
    MgMemberAdminRole,
    MgMemberMemberRole,
    MgMemberSelectedRole,
    MgMemberSelectedStaffType,
    MgMemberSelectRole,
    MgMemberSelectStaffType,
    MgMemberStaffTypeDoctor,
    MgMemberStaffTypeProvider,
    MgMemberStaffTypeStaff,
    submitButton
} from '../elements/MGMemberPageElements';

export default class MGMemberPage extends BasePage {

    pressSubmitButton(){
        Element.of(submitButton).click();
        return this;
    }

    getMGMemberAddress(){
        return Element.of('input#medical_group_member_address').getAttributeValue('value');
    }

    changeMgMemberRole(){
        const initialSelectedRole = Element.of(MgMemberSelectedRole).getValueText();
        if(
            initialSelectedRole === 'Medical Group Admin'){
            Element.of(MgMemberSelectRole).click();
            Element.of(MgMemberMemberRole).click();
        } else {
            Element.of(MgMemberSelectRole).click();
            Element.of(MgMemberAdminRole).click();
        }
        Element.of(submitButton).click();
    }

    changeToInitialMgMemberRole(initialMemberRole){
        Element.of(MgMemberSelectRole).click();
        Element.of(`//select[@id="medical_group_member_role"]/option[text()="${initialMemberRole}"]`).click();
        Element.of(submitButton).click();
    }

    changeMGMemberStaffType(){
        const initialSelectedStaffType = Element.of(MgMemberSelectedStaffType).getValueText();
        if(
            initialSelectedStaffType === 'Staff'){
            Element.of(MgMemberSelectStaffType).click();
            Element.of(MgMemberStaffTypeDoctor).click();
        } else if (initialSelectedStaffType === 'Doctor') {
            Element.of(MgMemberSelectStaffType).click();
            Element.of(MgMemberStaffTypeProvider).click();
        } else {
            Element.of(MgMemberSelectStaffType).click();
            Element.of(MgMemberStaffTypeStaff).click();
        }
        Element.of(submitButton).click();
    }

    changeToInitialStaffType(initialType){
        Element.of(MgMemberSelectStaffType).click();
        Element.of(`//select[@id="medical_group_member_staff_type"]/option[text()="${initialType}"]`).click();
        Element.of(submitButton).click();
    }

}
