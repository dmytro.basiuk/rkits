import Element from '../components/Element';
import Page from './BasePage';
import { allMemberNames, applyMGFilterButton, filterMGByCTInput } from '../elements/MembersPageElements';

export default class MGMembersPage extends Page {

    static filterMGMemberByCT(CT){
        Element.of(filterMGByCTInput).enterValue(CT);
        Element.of(filterMGByCTInput).pushTheButton('Enter');
        Element.of(applyMGFilterButton).click();
        return this;
    }

    static verifyNameNotInAList(unassignedMember){
        const allMGMemberNames = Element.of(allMemberNames).getAllValuesTexts();
        if(Array.isArray(allMGMemberNames) === true) {
            allMGMemberNames.forEach(function(el) {
                expect(el).to.be.not.equal(unassignedMember);
            });
        }
        else (
            expect(allMGMemberNames).to.be.not.equal(unassignedMember));
        return this;
    }

    static verifyNameInAList(assignedMember){
        const allMGMemberNames = Element.of(allMemberNames).getAllValuesTexts();
        expect(allMGMemberNames).to.include(assignedMember);
        return this;
    }

}
