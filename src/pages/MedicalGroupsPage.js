import BasePage from './BasePage';
import Element from '../components/Element';
import { actionMGGroupButton } from '../elements/MedicalGroupsPageElements';

export default class MedicalGroupsPage extends BasePage {

    performActionWithLastMedicalGroup(action) {
        Element.of(actionMGGroupButton).click();
        Element.of(`//ul[@class="dropdown-menu dropdown-add show"]/a[@class="dropdown-item"][text()="${action}"]`).click();
        return this;
    }
}
