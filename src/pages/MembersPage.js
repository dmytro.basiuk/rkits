import BasePage from './BasePage';
import Element from '../components/Element';
import { actionMGMemberButton } from '../elements/MembersPageElements';

export default class MembersPage extends BasePage {

    performActionWithLastMember(action){
        Element.of(actionMGMemberButton).click();
        Element.of(`//ul[@class="dropdown-menu dropdown-add show"]/a[@class="dropdown-item"][text()="${action}"]`).click();
        return this;
    }

    checkMemberAttributeIsDifferent(initialRole, changedRole){
        expect(initialRole).to.not.equal(changedRole);
        return this;
    }

    getMemberFirstName(){
        return Element.of('div[class="card-body"] td:first-of-type>a:first-of-type').getValueText().toString();
    }

    getCROMemberEmail(){
        return Element.of('div[class="card-body"] tr:first-of-type>td:nth-of-type(2)').getValueText().toString();
    }

    getMGMemberEmail(){
        return Element.of('div[class="card-body"] tr:first-of-type>td:nth-of-type(3)').getValueText().toString();
    }

    getCROMemberRole(){
        return Element.of('div[class="card-body"] tr:first-of-type>td:nth-of-type(3)').getValueText().toString();
    }

    getMgMemberRole(){
        return Element.of('div[class="card-body"] tr:first-of-type>td:nth-of-type(4)').getValueText().toString();
    }

    getMgMemberStaffType(){
        return Element.of('div[class="card-body"] tr:first-of-type>td:nth-of-type(5)').getValueText().toString();
    }

}
