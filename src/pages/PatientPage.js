import BasePage from './BasePage';
import Element from '../components/Element';
import {
    MgMemberAdminRole,
    MgMemberMemberRole,
    MgMemberSelectedRole,
    MgMemberSelectedStaffType,
    MgMemberSelectRole,
    MgMemberSelectStaffType,
    MgMemberStaffTypeDoctor,
    MgMemberStaffTypeProvider,
    MgMemberStaffTypeStaff,
    submitButton
} from '../elements/MGMemberPageElements';
import {
    PatientSelectedGender,
    PatientSelectedStudyEye,
    PatientSelectStudyEye,
    PatientSelectGender,
    PatientStudyEyeLeft, PatientStudyEyeRight, PatientStudyEyeBoth
} from '../elements/PatientPageElements';

export default class PatientPage extends BasePage {

    pressSubmitButton(){
        Element.of(submitButton).click();
        return this;
    }

    getPatientAddress(){
        return Element.of('input#medical_group_patient_address').getAttributeValue('value');
    }

    getPatientSelectedGender(){
        return Element.of(PatientSelectedGender).getValueText();
    }

    changePatientGenderTo(genderToSelect){
        Element.of(PatientSelectGender).click();
        Element.of(genderToSelect).click();
        return this;
    }

    verifySelectedValue(selectedValue, savedValue){
        expect(selectedValue).to.be.equal(savedValue);
        return this;
    }
    //
    // changeMgMemberRole(){
    //     const initialSelectedRole = Element.of(MgMemberSelectedRole).getValueText();
    //     if(
    //         initialSelectedRole === 'Medical Group Admin'){
    //         Element.of(MgMemberSelectRole).click();
    //         Element.of(MgMemberMemberRole).click();
    //     } else {
    //         Element.of(MgMemberSelectRole).click();
    //         Element.of(MgMemberAdminRole).click();
    //     }
    //     Element.of(submitButton).click();
    // }
    //
    // changeToInitialMgMemberRole(initialMemberRole){
    //     Element.of(MgMemberSelectRole).click();
    //     Element.of(`//select[@id="medical_group_member_role"]/option[text()="${initialMemberRole}"]`).click();
    //     Element.of(submitButton).click();
    // }
    //
    changePatientStudyEye(){
        const initialSelectedStudyEye = Element.of(PatientSelectedStudyEye).getValueText();
        if(
            initialSelectedStudyEye === 'Both'){
            Element.of(PatientSelectStudyEye).click();
            Element.of(PatientStudyEyeLeft).click();
        } else if (initialSelectedStudyEye === 'Left Eye') {
            Element.of(PatientSelectStudyEye).click();
            Element.of(PatientStudyEyeRight).click();
        } else {
            Element.of(PatientSelectStudyEye).click();
            Element.of(PatientStudyEyeBoth).click();
        }
        Element.of(submitButton).click();
    }

    changeToInitialStudyEye(initialSelection){
        Element.of(PatientSelectStudyEye).click();
        Element.of(`//select[@id="medical_group_patient_study_eye"]/option[text()="${initialSelection}"]`).click();
        Element.of(submitButton).click();
    }

}
