/* eslint-disable no-console */
import BasePage from './BasePage';
import Element from '../components/Element';
import {
    actionPatientsButton,
    backToDashboardButton,
    nextPageButton, patientSearchApplyButton, patientSearchField,
    patientsTitle
} from '../elements/PatientsPageElements';
import { applyMGFilterButton, filterMGByCTInput } from '../elements/MembersPageElements';

export default class PatientsPage extends BasePage {

    static checkingPatientsTitle(){
        return Element.of(patientsTitle).shouldBeVisible();
    }

    static searchingAndPerformingPatientAction(name, action){
        while(Element.of(backToDashboardButton).shouldBeVisible()){
            let pageSource = browser.getSource();
            console.log('page source2', pageSource);
            if(pageSource.includes(name) === true)
            {
                Element.of(`//a[text()="${name}"]/parent::div/following-sibling::div[@class="tools"]`).click();
                Element.of(`//a[text()="${name}"]/parent::div/following-sibling::div[@class="tools show"]//a[text()="${action}"]`).click();
                break;
            }
            else (Element.of(nextPageButton).click());
        }
    }

    performActionWithLastPatient(action) {
        Element.of(actionPatientsButton).click();
        Element.of(`//ul[@class="dropdown-menu dropdown-add show"]/a[@class="dropdown-item"][text()="${action}"]`).click();
        return this;
    }

    getPatientName(){
        return Element.of('td:first-of-type>a:first-of-type').getValueText().toString();
    }

    getPatientEmail(){
        return Element.of('tr:first-of-type>td:nth-of-type(2)').getValueText().toString();
    }

    getPatientStudyEye(){
        return Element.of('tr:first-of-type>td:nth-of-type(4)').getValueText().toString();
    }

    checkPatientAttributeIsDifferent(initial, changed){
        expect(initial).to.not.equal(changed);
        return this;
    }

    static searchingPatientByName(name){
        Element.of(patientSearchField).enterValue(name);
        Element.of(patientSearchApplyButton).click();
        return this;
    }
}
