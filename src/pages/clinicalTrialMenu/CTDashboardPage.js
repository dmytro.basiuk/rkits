import BasePage from '../BasePage';
import Element from '../../components/Element';
import { myUnprocessedEventsNumber } from '../../elements/clinicalTrialMenu/CTDashboardPageElements';
import CTMenuBasePage from './CTMenuBasePage';

export default class CTDashboardPage extends CTMenuBasePage {

    static getMyUnprocessedEventsNumber(){
        const text = Element.of(myUnprocessedEventsNumber).getValueText();
        const integer = parseInt(text, 10);
        return integer;
    }

}
