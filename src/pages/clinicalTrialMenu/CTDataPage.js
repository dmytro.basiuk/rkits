import BasePage from '../BasePage';
import Element from '../../components/Element';
import CTMenuBasePage from './CTMenuBasePage';

export default class CTDataPage extends CTMenuBasePage {

    //Check whether last 3 CT records include results of Amsler test, and if so, press "Show" for this CT record
    //The loop stops when user appears on a CT record page
    static openCTRecordWithAmslerTest() {
        const allTests = Element.of('//tr[1]/td[@data-label="Tests"]/div/div').getAllAttributesValues('data-title');
        let amsler = function(element) {
            return element === 'Amsler';
        };
        //Checking whether one of tests which were passed in last CT Record is 'Amsler'
        if (
            allTests.some(amsler)
        ) {
            Element.of('//tbody/tr[1]/td[5]/a').click();
        } else {
            throw "INFORMATION: last CT Record doesn't have results for Amsler test.\n" +
            ' Please, leave CT Record with Amsler test result, and then repeat this autotest';
        }
    }

}
