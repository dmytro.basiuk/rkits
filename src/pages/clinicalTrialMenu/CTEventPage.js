/* eslint-disable no-console */
import Element from '../../components/Element';
import {
    editAssigneeButton,
    eventReminderPendingCurrentOff,
    eventReminderPendingTurnOnButton,
    eventReminderProcessingCurrentOff,
    eventReminderProcessingTurnOnButton,
    lastComment,
    lastStatusComment, markAsInvalidButton,
    notActivePriorityButton,
    selectSecondAssignee,
    submitAssigneeButton
} from '../../elements/clinicalTrialMenu/CTEventPageElements';
import CTMenuBasePage from './CTMenuBasePage';

export default class CTEventPage extends CTMenuBasePage {

    static changingEventStatus(to){
        return Element.of(`//div[text()="${to}"]`).click();
    }

    static changingEventAssignee(){
        Element.of(editAssigneeButton).click();
        Element.of(selectSecondAssignee).click();
        Element.of(submitAssigneeButton).click();
        return this;
    }

    static gettingNotActivePriority(){
        return Element.of(notActivePriorityButton).getValueText();
    }

    static clickingLogTab(logTitle){
        return Element.of(`//a/h4[text()="${logTitle}"]`).click();
    }

    static gettingLastStatusComment(){
        return Element.of(lastStatusComment).getValueText();
    }

    static gettingLastComment(){
        return Element.of(lastComment).getValueText();
    }

    static getReminderHours(reminderTitle){
        return Element.of(reminderTitle).getValueText();
    }

    turningOnReminders(){
        Element.of(eventReminderPendingTurnOnButton).click();
        Element.of(eventReminderProcessingTurnOnButton).click();
        return this;
    }

}
