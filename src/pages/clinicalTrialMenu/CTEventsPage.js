/* eslint-disable no-console */
import BasePage from '../BasePage';
import CTMenuBasePage from './CTMenuBasePage';
import Element from '../../components/Element';
import {
    editLastEventButton, editLastEventPriorityHigh, eventsPriorities, eventsStatuses,
    lastEventPriority,
    lastEventStatus
} from '../../elements/clinicalTrialMenu/CTEventsPageElements';
import { applyFilterButton, filterButton, switchInvalidEventFilter } from '../../elements/BasePageElements';

export default class CTEventsPage extends CTMenuBasePage {

    static getLastEventPriority(){
        return Element.of(lastEventPriority).getValueText();
    }

    static getLastEventStatus(){
        return Element.of(lastEventStatus).getAttributeValue('data-title');
    }

    static putLastEventPriorityHigh(){
        Element.of(editLastEventButton).click();
        Element.of(editLastEventPriorityHigh).click();
        return this;
    }

    static verifyingAllPrioritiesAre(priority){
        const allEventsPriorities = Element.of(eventsPriorities).getAllValuesTexts();
        allEventsPriorities.forEach(function(el) {
            expect(el).to.be.equal(priority);
        });
        return this;
    }

    static verifyingAllStatusesAre(status){
        const allEventsStatuses = Element.of(eventsStatuses).getAllAttributesValues('data-title');
        allEventsStatuses.forEach(function(el) {
            expect(el).to.include(status);
        });
        return this;
    }

    static filterBy(filter, value){
        Element.of(`select[name="q[${filter}_eq]"]`).click();
        Element.of(`select[name="q[${filter}_eq]"]>option[value="${value}"]`).click();
        Element.of(applyFilterButton).click();
        return this;
    }

    static switchingInvalidEventsFilters(){
        Element.of(switchInvalidEventFilter).click();
        Element.of(applyFilterButton).click();
        return this;
    }

}
