import Element from '../../components/Element';
import CTMenuBasePage from './CTMenuBasePage';
import { nextPageButton } from '../../elements/PatientsPageElements';

export default class CTMGPatientsPage extends CTMenuBasePage {

    static checkStatusOfMG(mgTitle, status){
        while (Element.of('//a[text()="Statistics"]').shouldBeVisible()){
            let pageSource = browser.getSource();
            if(pageSource.includes(mgTitle) === true){
                const mgStatus = Element.of(`//a[text()="${mgTitle}"]/following-sibling::span`).getValueText();
                expect(mgStatus).to.be.equal(status);
                break;
            }
            else (Element.of(nextPageButton).click());
        }
    }
}
