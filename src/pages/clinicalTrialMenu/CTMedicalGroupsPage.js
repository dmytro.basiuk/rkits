import BasePage from '../BasePage';
import Element from '../../components/Element';
import CTMenuBasePage from './CTMenuBasePage';

export default class CTMedicalGroupsPage extends CTMenuBasePage {

    static doAssignActionForMG(mgTitle, action){
        return Element.of(`//td[text()="${mgTitle}"]/parent::tr[contains(@id,"medical_group")]//a[text()="${action}"]`).click();
    }

}
