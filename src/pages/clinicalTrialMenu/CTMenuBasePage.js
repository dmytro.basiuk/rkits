import BasePage from '../BasePage';
import Element from '../../components/Element';
import { myUnprocessedEventsNumber } from '../../elements/clinicalTrialMenu/CTDashboardPageElements';

export default class CTMenuBasePage extends BasePage {

    static goToCTMenuSection(section){
        return Element.of(`li>a[href*="${section}"]`).click();
    }

    static goToCTMenuSectionAsCRO(section){
        return Element.of(`//li/a/span[text()="${section}"]`).click();
    }

}
