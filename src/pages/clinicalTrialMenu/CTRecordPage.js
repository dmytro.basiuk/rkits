/* eslint-disable no-console */
import BasePage from '../BasePage';
import Element from '../../components/Element';
import { meanDeviationCoefficient } from '../../elements/clinicalTrialMenu/CRRecordPageElements';
import roundUp from '../../utils/convert-units/roundUp';
import CTMenuBasePage from './CTMenuBasePage';

export default class CTRecordPage extends CTMenuBasePage {

    static getMeanDeviationCoefficient(){
        const md = Element.of(meanDeviationCoefficient).getValueText();
        const mdConverted = Number(md);
        return mdConverted;
    }

    static openMDTab(tabName){
        return Element.of(`//a[@aria-selected='false'][contains(text(), '${tabName}')]`).click();
    }
    //Getting titles of affected areas and transforming it to appropriate Mean Deviation coefficients
    //For Central100 (left or right eye)
    static getAreasConvertToMDCoefficients() {
        let arr = Element.of('//div[contains(@id, "central-by-each")]/table/tbody/tr/td[1]').getAllValuesTexts();
        console.log('all areas affected', arr);

        // if there are couple affected areas change it to appropriate coefficients as for array
        if(Array.isArray(arr) === true) {

            //Looking for uncertain area, and if found, change value to '0'
            if (arr.includes('uncertain 1')) {
                let indexUncertain = arr.indexOf('uncertain 1');
                console.log('uncertain index', indexUncertain);
                arr.splice(indexUncertain, 1, '0');
            }
            else {
                console.log('there is no uncertain area');
            }

            //Looking for crooked area, and if found, change value to '0'
            if (arr.includes('crooked 1')) {
                let indexCrooked = arr.indexOf('crooked 1');
                console.log('crooked index', indexCrooked);
                arr.splice(indexCrooked, 1, '0');
            }
            else {
                console.log('there is no crooked area');
            }

            //Looking for double area, and if found, change value to '0'
            if (arr.includes('double 1')) {
                let indexDouble = arr.indexOf('double 1');
                console.log('double index', indexDouble);
                arr.splice(indexDouble, 1, '0');
            }
            else {
                console.log('there is no double area');
            }

            //Looking for dark area, and if found, change value to '-0.075'
            if (arr.includes('dark 1')) {
                let indexDark = arr.indexOf('dark 1');
                console.log('dark index', indexDark);
                arr.splice(indexDark, 1, '-0.075');
            }
            else {
                console.log('there is no dark area');
            }

            //Looking for heavy area, and if found, change value to '-0.05625'
            if (arr.includes('heavy 1')) {
                let indexHeavy = arr.indexOf('heavy 1');
                console.log('heavy index', indexHeavy);
                arr.splice(indexHeavy, 1, '-0.05625');
            }
            else {
                console.log('there is no heavy area');
            }

            //Looking for medium area, and if found, change value to '-0.0375'
            if (arr.includes('medium 1')) {
                let indexMedium = arr.indexOf('medium 1');
                console.log('medium index', indexMedium);
                arr.splice(indexMedium, 1, '-0.0375');
            }
            else {
                console.log('there is no medium area');
            }

            //Looking for light area, and if found, change value to '-0.01875'
            if (arr.includes('light 1')) {
                let indexLight = arr.indexOf('light 1');
                console.log('light index', indexLight);
                arr.splice(indexLight, 1, '-0.01875');
            }
            else {
                console.log('there is no light area');
            }

            console.log('new array with changed value', arr);
        }

        // if there is one affected area change it to appropriate coefficient as a string
        else {
            if(arr === 'uncertain 1'){
                return arr.replace('uncertain 1', '0');
            } else{console.log('this one area which affected is not "uncertain"');}

            if(arr === 'crooked 1'){
                return arr.replace('crooked 1', '0');
            } else{console.log('this one area which affected is not "crooked"');}

            if(arr === 'double 1'){
                return arr.replace('double 1', '0');
            } else{console.log('this one area which affected is not "double"');}

            if(arr === 'dark 1'){
                return arr.replace('dark 1', '-0.075');
            } else{console.log('this one area which affected is not "dark"');}

            if(arr === 'heavy 1'){
                return arr.replace('heavy 1', '-0.05625');
            } else{console.log('this one area which affected is not "heavy"');}

            if(arr === 'medium 1'){
                return arr.replace('medium 1', '-0.0375');
            } else{console.log('this one area which affected is not "medium"');}

            if(arr === 'light 1'){
                return arr.replace('light 1', '-0.01875');
            } else{console.log('this one area which affected is not "light"');}
        }
        return arr;
    }

    //For Central100
    // Right or Left Eye
    static getOverlappingSizesAmslerAreas(){
        return Element.of('//div[contains(@id, "central-by-each")]/table/tbody/tr/td[3]').getAllAttributesValues('data-sort');
    }

    calculateMD(coefficients, sizes){
        //If there is more than one coefficient and it's size multiple and round MD value as for Arrays
        if(Array.isArray(coefficients) === true) {

            //Multiplying areas coefficients with their appropriate overlapping sizes
            let mult = coefficients.map(function(num, idx) {
                return num * sizes[idx];
            });
            console.log('after multiplying values of arrays', mult);

            //Calculating the sum of all values after multiplying -> receive MD value before rounding
            const reducer = (accumulator, currentValue) => accumulator + currentValue;
            const sum = mult.reduce(reducer);
            console.log('final calculated value of MD', sum);

            //Rounding up MD value
            const roundUpValue = roundUp(sum, 3);
            console.log('rounded MD value', roundUpValue);
            return roundUpValue;
        }
        //If there is one coefficient and it's size multiple and round MD value as for String
        else{
            const sum = coefficients*sizes;
            console.log("MD when there is one coefficient and it's size", sum);
            //Rounding up MD value
            const roundUpValue = roundUp(sum, 2);
            console.log("rounded MD value(one coefficient and it's size)", roundUpValue);
            return roundUpValue;
        }

    }

    verifyMDValues(displayedMD, calculatedMD){
        expect(displayedMD).to.be.equal(calculatedMD);
        return this;
    }

}
