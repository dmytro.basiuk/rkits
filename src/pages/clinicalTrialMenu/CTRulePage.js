/* eslint-disable no-console */
import BasePage from '../BasePage';
import Element from '../../components/Element';
import {
    ruleAssigneeField, ruleEmailSubjectPatientField,
    ruleEmailVariablesLink,
    ruleRecipientsField, ruleVariableRecipientName
} from '../../elements/clinicalTrialMenu/CTRulePageElements';
import CTMenuBasePage from './CTMenuBasePage';

export default class CTRulePage extends CTMenuBasePage {

    static selectEventType(eventType){
        Element.of('select[name="rule[event_type]"]').click();
        Element.of(`select>option[value="${eventType}"]`).click();
    }

    static chooseRuleAssignee(){
        Element.of(ruleAssigneeField).click();
        Element.of(`${ruleAssigneeField}>option:nth-of-type(2)`).click();
        return this;
    }

    static chooseRecipient(){
        Element.of(ruleRecipientsField).click();
        Element.of(`${ruleRecipientsField}+div div[class*="option"]`).click();
        return this;
    }

    static copyPasteEmailVariable(variable){
        Element.of(ruleEmailVariablesLink).click();
        Element.of(`a[data-clipboard-text="${variable}"]`).click();
        Element.of(ruleEmailSubjectPatientField).clearEntireField();
        Element.of(ruleEmailSubjectPatientField).pushTheButton(['Control', 'v', 'Control']);
        return this;
    }

    static getEmailSubject() {
        return Element.of(ruleEmailSubjectPatientField).getAttributeValue('value');
    }

    static reminderSelectHours(status, hours){
        Element.of(`//strong[@class="${status}"]/parent::div/parent::div/following-sibling::div/div/select`).click();
        Element.of(`//strong[@class="${status}"]/parent::div/parent::div/following-sibling::div/div/select/option[@value="${hours}"]`).click();
        return this;
    }

    static turnOnOffReminder(status){
        Element.of(`//strong[@class="${status}"]/parent::div/parent::div/following-sibling::div/following-sibling::div//span`).click();
        return this;
    }

    static getReminderSelectedHours(status){
        return Element.of(`//strong[@class="${status}"]/parent::div/parent::div/following-sibling::div/div/select/option[@selected="selected"]`).getValueText();
    }

    static checkingReminderIsTurnedOn(status){
        Element.of(`//strong[@class="${status}"]/parent::div/parent::div/following-sibling::div/following-sibling::div//input[@checked="checked"]`).shouldBeVisible();
        return this;
    }

    static checkingReminderIsTurnedOff(status){
        Element.of(`//strong[@class="${status}"]/parent::div/parent::div/following-sibling::div/following-sibling::div//input[@checked="checked"]`).shouldNotExist(3000, true);
        return this;
    }



}
