/* eslint-disable no-console */
import BasePage from '../BasePage';
import Element from '../../components/Element';
import { lastRuleAction } from '../../elements/clinicalTrialMenu/CTRulesPageElements';
import CTMenuBasePage from './CTMenuBasePage';

export default class CTRulesPage extends CTMenuBasePage {

    static actionLastRule(action){
        Element.of(lastRuleAction).click();
        Element.of(`tr:nth-of-type(1) a[class="dropdown-item"][href*="${action}"]`).click();
        return this;
    }

}
