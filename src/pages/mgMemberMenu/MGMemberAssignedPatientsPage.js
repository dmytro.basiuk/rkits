/* eslint-disable no-console */
import Element from '../../components/Element';
import BasePage from '../BasePage';
import { assignButton } from '../../elements/patientMenu/PatientRMPageElements';

export default class MGMemberAssignedPatientsPage extends BasePage {

    static assigningPatient(){
        const patientToAssignName = Element.of(assignButton+'/parent::span/parent::div/parent::td/parent::tr/td/a').getValueText();
        //const patientToAssignSelector = assignButton+'/parent::span/parent::div/parent::td/parent::tr//preceding-sibling::tr/td/a[contains(text(),"'+mgMemberToAssignName+'"]';
        const patientToAssignSelector = assignButton+'/parent::span/parent::div/parent::td/parent::tr/td/a[contains(text(),"'+patientToAssignName+'")]';
        Element.of(assignButton).click();
        return patientToAssignSelector;
    }

    static unAssigningPatient(patientName){
        Element.of(`//a[text()="${patientName}"]/parent::td/following-sibling::td/div/span/a[text()="UnAssign"]`).click();
        return this;
    }

    static settingPrimaryForPatient(patient){
        Element.of(`//a[text()="${patient}"]/parent::td/parent::tr/td/div/div/a[text()="Set Primary"]`).click();
        browser.pause(1000);
        return this;
    }

    static returningToMentionedPatientDashboard(patientName){
        Element.of(`//td/a[text()="${patientName}"]`).click();
        return this;
    }

    static verifyingPatientHasLabel(patient, label){
        const primaryPatient = Element.of(`//a[text()="${patient}"]/parent::td/span[@class="badge badge-${label}"]`);
        primaryPatient.elementExists();
        return this;
    }

    static verifyingUnassignedPatient(patient){
        const unAssignedMGMember = Element.of(`//a[text()="${patient}"]/parent::td/following-sibling::td/div/span/a[text()="Assign"]`);
        unAssignedMGMember.elementExists();
        return this;
    }
}
