/* eslint-disable no-console */
import BasePage from '../BasePage';
import Element from '../../components/Element';

export default class MGMemberClinicalTrialsPage extends BasePage {

    static assignActionCTtoMGMember(CT, action){
        return Element.of(`//td[text()="${CT}"]/parent::tr//a[text()="${action}"]`).click();
    }

}
