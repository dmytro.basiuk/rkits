/* eslint-disable no-console */
import BasePage from '../BasePage';
import Element from '../../components/Element';

export default class MGMemberMenuBasePage extends BasePage {

    static goToMGMemberMenuSection(section){
        return Element.of(`//a/span[text()="${section}"]`).click();
    }
}
