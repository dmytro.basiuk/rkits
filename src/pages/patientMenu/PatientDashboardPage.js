/* eslint-disable no-console */
import BasePage from '../BasePage';
import Element from '../../components/Element';
import PatientMenuBasePage from './PatientMenuBasePage';

export default class PatientDashboardPage extends PatientMenuBasePage {

    static doPatientDashboardActions(action){
        return Element.of(`a[class="btn action-image-block"][href*="${action}"]`).click();
    }


}
