/* eslint-disable no-console */
import Element from '../../components/Element';
import PatientMenuBasePage from './PatientMenuBasePage';

export default class PatientEventsPage extends PatientMenuBasePage {

    static getEventsNumber(){
        const eventHeader = Element.of('div[class*="card-header card-header-divider"]').getValueText();
        const eventHeaderNumber = eventHeader.match(/\d+/)[0];
        console.log('eventHeaderNumber', eventHeaderNumber);
        return eventHeaderNumber;
    }
}
