/* eslint-disable no-console */
import BasePage from '../BasePage';
import Element from '../../components/Element';

export default class PatientMenuBasePage extends BasePage {

    static goToPatientMenuSection(section){
        return Element.of(`li>a[href*="${section}"]`).click();
    }


}
