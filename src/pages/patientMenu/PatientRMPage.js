/* eslint-disable no-console */
import Element from '../../components/Element';
import PatientMenuBasePage from './PatientMenuBasePage';
import { assignButton } from '../../elements/patientMenu/PatientRMPageElements';

export default class PatientRMPage extends PatientMenuBasePage {

    static assigningMGMember(){
        const mgMemberToAssignName = Element.of(assignButton+'/parent::span/parent::div/parent::td/parent::tr/td/a').getValueText();
        //const mgMemberToAssignSelector = assignButton+'/parent::span/parent::div/parent::td/parent::tr//preceding-sibling::tr/td/a[contains(text(),"'+mgMemberToAssignName+'"]';
        const mgMemberToAssignSelector = assignButton+'/parent::span/parent::div/parent::td/parent::tr/td/a[contains(text(),"'+mgMemberToAssignName+'")]';
        Element.of(assignButton).click();
        return mgMemberToAssignSelector;
    }

    static settingPrimaryMGMember(mgMember){
        Element.of(`//a[text()="${mgMember}"]/parent::td/parent::tr/td/div/div/a[text()="Set Primary"]`).click();
        return this;
    }

    static settingBackupMGMember(mgMember){
        Element.of(`//a[text()="${mgMember}"]/parent::td/parent::tr/td/div/div/a[text()="Set Backup"]`).click();
        return this;
    }

    static verifyingUnassignedMember(mgMember){
        const unAssignedMGMember = Element.of(`//a[text()="${mgMember}"]/parent::td/following-sibling::td/div/span/a[text()="Assign"]`);
        unAssignedMGMember.elementExists();
        return this;
    }

    static verifyingMemberHasLabel(mgMember, label){
        const primaryMGMember = Element.of(`//a[text()="${mgMember}"]/parent::td/span[@class="badge badge-${label}"]`);
        primaryMGMember.elementExists();
        return this;
    }

    static verifyingMemberDoesNotHaveLabel(mgMember, label){
        Element.of(`//a[text()="${mgMember}"]/parent::td/span[@class="badge badge-${label}"]`).shouldNotExist(3000, true);
        return this;
    }

    static unAssigningMGMember(mgMember){
        Element.of(`//a[text()="${mgMember}"]/parent::td/following-sibling::td/div/span/a[text()="UnAssign"]`).click();
        return this;
    }
}
