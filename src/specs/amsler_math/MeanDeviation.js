/* eslint-disable no-console */
import { openLoginPage } from '../../functions/BaseFunctions';
import { at } from '../../utils/PageFactory';
import loginPage from '../../pages/LoginPage';
import { mgAdminMainEmail, mgAdminPassword } from '../../model/Credentials';
import clinicalTrialsPage from '../../pages/ClinicalTrialsPage';
import { clinicalTrialsLink } from '../../elements/HeaderElements';
import ctDataPage from '../../pages/clinicalTrialMenu/CTDataPage';
import ctRecordPage from '../../pages/clinicalTrialMenu/CTRecordPage';
import basePage from '../../pages/BasePage';

describe('Testing calculation for Mean Deviation coefficient',() => {
    //1). Leave a CT Record with Amsler C100 result for one of eyes before running this test
    //2). This test can be used for calculation of MD value when there is a C100 result for one of eyes.
    //In case there are results for both - left and right eyes, - only MD for left eye will be tested
    beforeEach('Login as Medical Group Admin user and go to "Clinical Trial->CT Data page', () => {
        openLoginPage();
        at(loginPage)
            .login(mgAdminMainEmail, mgAdminPassword);
        at(basePage)
            .goToHeaderCategory(clinicalTrialsLink);
        at(clinicalTrialsPage).goToCTDataPage();
        ctDataPage.openCTRecordWithAmslerTest();
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Check Mean Deviation calculations', () => {
        const displayedMD = ctRecordPage.getMeanDeviationCoefficient();
        ctRecordPage.openMDTab('Area');
        const areasCoef = ctRecordPage.getAreasConvertToMDCoefficients();
        console.log('affected areas - coefficients', areasCoef);

        const overlapSizes = ctRecordPage.getOverlappingSizesAmslerAreas();
        console.log('overlapping sizes', overlapSizes);

        const calculatedMD = at(ctRecordPage).calculateMD(areasCoef, overlapSizes);
        at(ctRecordPage).verifyMDValues(displayedMD,calculatedMD);
    });
});
