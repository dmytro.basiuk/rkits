import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import { companyAdminEmail, companyAdminPassword } from '../../../model/Credentials';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import basePage from '../../../pages/BasePage';
import companyPage from '../../../pages/CompanyPage';
import { random76Characters, testAddress, testName } from '../../../model/Constants';
import { companyAddressField, companyNameField } from '../../../elements/EditCompanyPageElements';

describe('Validation rules test cases for Company Address field',() => {
    beforeEach('Login as Company Admin user and go to "Edit Company" Page', () => {
        openLoginPage();
        at(loginPage)
            .login(companyAdminEmail, companyAdminPassword);
        at(clinicalTrialsPage)
            .goToEditCompany();
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Fill in "Name" and "Address" fields with a correct data and press "Submit" -> \n' +
        '"Company was successfully updated" message should appear.\n' +
        'Press "Edit company" button and check the correct saving', () => {
        at(companyPage)
            .inputToField(companyNameField, testName)
            .inputToField(companyAddressField, testAddress)
            .pressSubmitButton();
        at(basePage)
            .verifySystemMessage('Company was successfully updated.');
        at(clinicalTrialsPage)
            .goToEditCompany();
        const savedCompanyAddress = at(companyPage).getCompanyAddress();
        at(companyPage).verifyAttributeEqual(testAddress, savedCompanyAddress);
    });

    it('Fill in "Name" field with a correct data, leave "Address" field empty and press "Submit" ->\n' +
        '"Company was successfully updated" message should appear', () => {
        at(companyPage)
            .inputToField(companyNameField, testName)
            .inputToField(companyAddressField, '')
            .pressSubmitButton();
        at(basePage)
            .verifySystemMessage('Company was successfully updated.');
    });

    it('Input 76 characters and press "Submit" -> "Address is too long (maximum is 75 characters)" message should appear',() => {
        at(companyPage)
            .inputToField(companyNameField, testName)
            .inputToField(companyAddressField, random76Characters)
            .pressSubmitButton()
            .verifyErrorMessage('Address is too long (maximum is 75 characters)');
    });
});
