import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import { companyAdminEmail, companyAdminPassword } from '../../../model/Credentials';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import basePage from '../../../pages/BasePage';
import companyPage from '../../../pages/CompanyPage';
import { random256Characters, testDetails, testName } from '../../../model/Constants';
import { companyDetailsField, companyNameField } from '../../../elements/EditCompanyPageElements';

describe('Validation rules test cases for Company Details field',() => {
    beforeEach('Login as Company Admin user and go to "Edit Company" Page', () => {
        openLoginPage();
        at(loginPage)
            .login(companyAdminEmail, companyAdminPassword);
        at(clinicalTrialsPage)
            .goToEditCompany();
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Fill in "Name" and "Details" fields with a correct data and press "Submit" -> \n' +
        '"Company was successfully updated" message should appear. \n' +
        'Press "Edit company" button and check the correct saving', () => {
        at(companyPage)
            .inputToField(companyNameField, testName)
            .inputToField(companyDetailsField, testDetails)
            .pressSubmitButton();
        at(basePage)
            .verifySystemMessage('Company was successfully updated.');
        at(clinicalTrialsPage)
            .goToEditCompany();
        const savedCompanyDetails = at(companyPage).getCompanyDetails();
        at(companyPage).verifyAttributeEqual(testDetails, savedCompanyDetails);
    });

    it('Fill in "Name" field with a correct data, leave "Details" field empty and press "Submit" ->\n' +
        '"Company was successfully updated" message should appear', () => {
        at(companyPage)
            .inputToField(companyNameField, testName)
            .inputToField(companyDetailsField, '')
            .pressSubmitButton();
        at(basePage)
            .verifySystemMessage('Company was successfully updated.');
    });

    it('Input 256 characters and press "Submit" -> "Details description is too long (maximum 255 characters)" message should appear',() => {
        at(companyPage)
            .inputToField(companyNameField, testName)
            .inputToField(companyDetailsField, random256Characters)
            .pressSubmitButton()
            .verifyErrorMessage('Details description is too long (maximum 255 characters)');
    });
});
