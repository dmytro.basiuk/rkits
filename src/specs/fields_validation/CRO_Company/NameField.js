/* eslint-disable no-console */
import loginPage from '../../../pages/LoginPage';
import basePage from '../../../pages/BasePage';
import { companyAdminEmail, companyAdminPassword } from '../../../model/Credentials';
import { at } from '../../../utils/PageFactory';
import { openLoginPage } from '../../../functions/BaseFunctions';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import companyPage from '../../../pages/CompanyPage';
import { random1Character, random2Characters, random75Characters, random76Characters } from '../../../model/Constants';
import { companyNameField } from '../../../elements/EditCompanyPageElements';

describe('Validation rules test cases for Company Name field',() => {
    beforeEach('Login as Company Admin user and go to "Edit Company" Page', () => {
        openLoginPage();
        at(loginPage)
            .login(companyAdminEmail, companyAdminPassword);
        at(clinicalTrialsPage)
            .goToEditCompany();
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Leave the field empty and press "Submit" -> "Name cannot be blank" message should appear',() => {
        at(companyPage)
            .inputToField(companyNameField, '')
            .pressSubmitButton()
            .verifyErrorMessage("Name can't be blank");
    });

    it('Input 76 characters and press "Submit" -> "Name is too long" message should appear',() => {
        at(companyPage)
            .inputToField(companyNameField, random76Characters)
            .pressSubmitButton()
            .verifyErrorMessage('Name is too long (maximum is 75 characters)');
    });

    it('Input 1 character and press "Submit" -> "Name is too short" message should appear',() => {
        at(companyPage)
            .inputToField(companyNameField,random1Character)
            .pressSubmitButton()
            .verifyErrorMessage('Name is too short (minimum is 2 characters)');
    });

    it('Input 2 characters and press "Submit" -> "Company was successfully updated" message should appear.\n' +
        'Check that element "div.page-title" has changed to just submitted one',() => {
        at(companyPage)
            .inputToField(companyNameField,random2Characters)
            .pressSubmitButton();
        const savedCompanyTitle = at(companyPage).getCompanyTitle();
        at(basePage)
            .verifySystemMessage('Company was successfully updated.')
            .verifyAttributeEqual(random2Characters, savedCompanyTitle);
    });

    it('Input 75 characters and press "Submit" -> "Company was successfully updated" message should appear.',() => {
        at(companyPage)
            .inputToField(companyNameField,random75Characters)
            .pressSubmitButton();
        at(basePage)
            .verifySystemMessage('Company was successfully updated.');
    });

    it('Input 3 spaces and press "Submit" -> "Name cannot be blank" message should appear',() => {
        at(companyPage)
            .inputToField(companyNameField, '   ')
            .pressSubmitButton()
            .verifyErrorMessage("Name can't be blank");
    });

    it('Input 4 characters, 1 space, 3 figures, 2 special symbols and press "Submit" -> \n' +
        '"Company was successfully updated" message should appear',() => {
        at(companyPage)
            .inputToField(companyNameField, 'abcd 123%&')
            .pressSubmitButton();
        at(basePage)
            .verifySystemMessage('Company was successfully updated.');
    });

    // it('Input 2 special symbols and press "Submit" -> "Company name should contain letter" message should appear',() => {
    //     at(companyPage)
    //         .inputToNameField('%&')
    //         .pressSubmitButton()
    //         .verifyErrorMessage('Company name should contain letter');
    // });
});
