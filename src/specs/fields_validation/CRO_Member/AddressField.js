import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import { companyAdminEmail, companyAdminPassword } from '../../../model/Credentials';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import basePage from '../../../pages/BasePage';
import companyPage from '../../../pages/CompanyPage';
import { random76Characters, testAddress } from '../../../model/Constants';
import membersPage from '../../../pages/MembersPage';
import CROMemberPage from '../../../pages/CROMemberPage';
import { CROmemberAddressField } from '../../../elements/CROMemberPageElements';
import { membersLink } from '../../../elements/HeaderElements';

describe('Validation rules test cases for CRO Member Address field',() => {
    beforeEach('Login as Company Admin user and go to "Members" Page -> Last Record -> Edit', () => {
        openLoginPage();
        at(loginPage)
            .login(companyAdminEmail, companyAdminPassword);
        at(clinicalTrialsPage)
            .goToHeaderCategory(membersLink);
        at(membersPage).performActionWithLastMember('Edit');
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Fill in "Address" field with a correct data and press "Submit" -> \n' +
        '"User was successfully updated" message should appear.\n' +
        'Press "Edit" user button and check the correct saving', () => {
        at(CROMemberPage)
            .inputToField(CROmemberAddressField, testAddress)
            .pressSubmitButton();
        at(basePage)
            .verifySystemMessage('User was successfully updated.');
        at(membersPage)
            .performActionWithLastMember('Edit');
        const savedCROMemberAddress = at(CROMemberPage).getCROMemberAddress();
        at(companyPage).verifyAttributeEqual(testAddress, savedCROMemberAddress);
    });

    it('Leave "Address" field empty and press "Submit" ->\n' +
        '"Company was successfully updated" message should appear', () => {
        at(CROMemberPage)
            .inputToField(CROmemberAddressField, '')
            .pressSubmitButton();
        at(basePage)
            .verifySystemMessage('User was successfully updated.');
    });

    it('Input 76 characters and press "Submit" -> \n' +
        '"Address is too long (maximum is 75 characters)" message should appear',() => {
        at(CROMemberPage)
            .inputToField(CROmemberAddressField, random76Characters)
            .pressSubmitButton()
            .verifyErrorMessage('Address is too long (maximum is 75 characters)');
    });
});
