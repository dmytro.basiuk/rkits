import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import { companyAdminEmail, companyAdminPassword } from '../../../model/Credentials';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import membersPage from '../../../pages/MembersPage';
import basePage from '../../../pages/BasePage';
import CROMemberPage from '../../../pages/CROMemberPage';
import { randomEmail, randomFirstName, randomLastName } from '../../../model/Constants';
import { CROmemberEmailField, CROmemberFirstNameField, CROmemberLastNameField } from '../../../elements/CROMemberPageElements';
import { membersLink } from '../../../elements/HeaderElements';

describe('Validation rules test cases for CRO Member Email field',() => {
    beforeEach('Login as Company Admin user and go to "Members" Page -> Last Record -> Edit', () => {
        openLoginPage();
        at(loginPage)
            .login(companyAdminEmail, companyAdminPassword);
        at(clinicalTrialsPage)
            .goToHeaderCategory(membersLink);
        at(membersPage).performActionWithLastMember('Edit');
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Fill in "Email" field and all other fields with a correct data and press "Submit" ->\n' +
        '"User was successfully updated" message should appear.\n' +
        'Check that email of the last on a page member has been changed to just submitted one', () => {
        at(CROMemberPage)
            .inputToField(CROmemberFirstNameField, randomFirstName)
            .inputToField(CROmemberLastNameField, randomLastName)
            .inputToField(CROmemberEmailField, randomEmail)
            .pressSubmitButton();
        const savedEmail = at(membersPage).getCROMemberEmail();
        at(membersPage)
            .verifySystemMessage('User was successfully updated.')
            .verifyAttributeContains(savedEmail, randomEmail);
    });

    it('Leave the field empty, all other fields fill with a proper data and press "Submit" ->\n' +
        '"Email field cannot be blank" message should appear',() => {
        at(CROMemberPage)
            .inputToField(CROmemberFirstNameField,randomFirstName)
            .inputToField(CROmemberLastNameField, randomLastName)
            .inputToField(CROmemberEmailField, '')
            .pressSubmitButton()
            .verifyErrorMessage("Email can't be blank");
    });

    it('Input email address which has been already taken and press "Submit" ->\n' +
        '"Email has already been taken" message should appear',() => {
        at(CROMemberPage)
            .inputToField(CROmemberEmailField, companyAdminEmail)
            .pressSubmitButton()
            .verifyErrorMessage('Email has already been taken');
    });

    it('Input email without @ sign and press "Submit" -> "Email is invalid" message should appear',() => {
        at(CROMemberPage)
            .inputToField(CROmemberEmailField, 'testgmail.com')
            .pressSubmitButton()
            .verifyErrorMessage('Email is invalid');
    });
});
