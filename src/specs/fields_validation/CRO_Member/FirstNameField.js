import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import { companyAdminEmail, companyAdminPassword } from '../../../model/Credentials';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import membersPage from '../../../pages/MembersPage';
import basePage from '../../../pages/BasePage';
import CROMemberPage from '../../../pages/CROMemberPage';
import {
    random1Character,
    random2Characters,
    random55Characters,
    random56Characters,
    randomLastName
} from '../../../model/Constants';
import { CROmemberFirstNameField, CROmemberLastNameField } from '../../../elements/CROMemberPageElements';
import { membersLink } from '../../../elements/HeaderElements';

describe('Validation rules test cases for CRO Member First Name field',() => {
    beforeEach('Login as Company Admin user and go to "Members" Page -> Last Record -> Edit', () => {
        openLoginPage();
        at(loginPage)
            .login(companyAdminEmail, companyAdminPassword);
        at(clinicalTrialsPage)
            .goToHeaderCategory(membersLink);
        at(membersPage).performActionWithLastMember('Edit');
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Leave the field empty, all other fields fill with a proper data and press "Submit" ->\n' +
        '"First Name field cannot be blank" message should appear',() => {
        at(CROMemberPage)
            .inputToField(CROmemberFirstNameField,'')
            .inputToField(CROmemberLastNameField, randomLastName)
            .pressSubmitButton()
            .verifyErrorMessage("First name can't be blank");
    });

    it('Input 1 character and press "Submit" ->\n' +
        ' "First name is too short (minimum is 2 characters)" message should appear',() => {
        at(CROMemberPage)
            .inputToField(CROmemberFirstNameField, random1Character)
            .inputToField(CROmemberLastNameField, randomLastName)
            .pressSubmitButton()
            .verifyErrorMessage('First name is too short (minimum is 2 characters)');
    });

    it('Input 56 characters and press "Submit" -> \n' +
        '"First name is too long (maximum is 55 characters)" message should appear',() => {
        at(CROMemberPage)
            .inputToField(CROmemberFirstNameField, random56Characters)
            .inputToField(CROmemberLastNameField, randomLastName)
            .pressSubmitButton()
            .verifyErrorMessage('First name is too long (maximum is 55 characters)');
    });

    it('Input 2 characters and press "Submit" -> "User was successfully updated" message should appear.\n' +
        'Check that the name of the last on a page member has been changed to just submitted one',() => {
        at(CROMemberPage)
            .inputToField(CROmemberFirstNameField, random2Characters)
            .inputToField(CROmemberLastNameField, randomLastName)
            .pressSubmitButton();
        const savedMemberName = at(membersPage).getMemberFirstName();
        at(membersPage)
            .verifySystemMessage('User was successfully updated.')
            .verifyAttributeContains(savedMemberName, random2Characters);
    });

    it('Input 55 characters and press "Submit" -> "User was successfully updated" message should appear',() => {
        at(CROMemberPage)
            .inputToField(CROmemberFirstNameField, random55Characters)
            .inputToField(CROmemberLastNameField, randomLastName)
            .pressSubmitButton();
        at(membersPage)
            .verifySystemMessage('User was successfully updated.');
    });

    it('Input 3 spaces and press "Submit" -> \n' +
        '"First name field cannot be blank" message should appear',() => {
        at(CROMemberPage)
            .inputToField(CROmemberFirstNameField, '   ')
            .inputToField(CROmemberLastNameField, randomLastName)
            .pressSubmitButton()
            .verifyErrorMessage("First name can't be blank");
    });
});
