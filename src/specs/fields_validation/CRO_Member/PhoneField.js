import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import { companyAdminEmail, companyAdminPassword } from '../../../model/Credentials';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import membersPage from '../../../pages/MembersPage';
import basePage from '../../../pages/BasePage';
import CROMemberPage from '../../../pages/CROMemberPage';
import { CROmemberPhoneField } from '../../../elements/CROMemberPageElements';
import { random23Figures, randomPhoneNumber } from '../../../model/Constants';
import { membersLink } from '../../../elements/HeaderElements';

describe('Validation rules test cases for CRO Member Phone field',() => {
    beforeEach('Login as Company Admin user and go to "Members" Page -> Last Record -> Edit', () => {
        openLoginPage();
        at(loginPage)
            .login(companyAdminEmail, companyAdminPassword);
        at(clinicalTrialsPage)
            .goToHeaderCategory(membersLink);
        at(membersPage).performActionWithLastMember('Edit');
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Leave the field empty, all other fields fill with a proper data and press "Submit" ->\n' +
        '"User was successfully updated" message should appear', () => {
        at(CROMemberPage)
            .inputToField(CROmemberPhoneField, '')
            .pressSubmitButton();
        at(membersPage)
            .verifySystemMessage('User was successfully updated.');
    });

    it('Input 10 figures and press "Submit" -> \n' +
        '"User was successfully updated" message should appear" message should appear', () => {
        at(CROMemberPage)
            .inputToField(CROmemberPhoneField, randomPhoneNumber)
            .pressSubmitButton();
        at(membersPage)
            .verifySystemMessage('User was successfully updated.');
    });

    it('Input 23 figures and press "Submit" -> \n' +
        '"Phone is too long (maximum is 22 characters)" message should appear', () => {
        at(CROMemberPage)
            .inputToField(CROmemberPhoneField, random23Figures)
            .pressSubmitButton()
            .verifySystemErrorMessage('Phone is too long (maximum is 22 characters)');
    });

    it('Input 3 letters and press "Submit" -> \n' +
        '"Phone can contain only numbers" message should appear', () => {
        at(CROMemberPage)
            .inputToField(CROmemberPhoneField, 'abc')
            .pressSubmitButton()
            .verifySystemErrorMessage('Phone can contain only numbers');
    });

});
