import { capitalizeFirstLetter, openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import { companyAdminEmail, companyAdminPassword } from '../../../model/Credentials';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import membersPage from '../../../pages/MembersPage';
import basePage from '../../../pages/BasePage';
import CROMemberPage from '../../../pages/CROMemberPage';
import { membersLink } from '../../../elements/HeaderElements';

describe('Validation rules test cases for CRO Member Role field',() => {
    beforeEach("Login as Company Admin user and go to 'Members' Page", () => {
        openLoginPage();
        at(loginPage)
            .login(companyAdminEmail, companyAdminPassword);
        at(clinicalTrialsPage)
            .goToHeaderCategory(membersLink);
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it("Get Last Member's role, go to -> Last Record -> Edit -> change the role, press 'Submit' button, \n" +
        'check that role is changed, then switch the role back', () => {
        const initialMemberRole = at(membersPage).getCROMemberRole();
        at(membersPage).performActionWithLastMember('Edit');
        at(CROMemberPage).changeMemberRole();
        at(membersPage).verifySystemMessage('User was successfully updated.');
        const changedMemberRole = at(membersPage).getCROMemberRole();
        at(membersPage).checkMemberAttributeIsDifferent(initialMemberRole, changedMemberRole);
        //Switching Role back to initial status
        at(membersPage).performActionWithLastMember('Edit');
        const capitalizedInitialRole = capitalizeFirstLetter(initialMemberRole);
        at(CROMemberPage).changeToInitialCROMemberRole(capitalizedInitialRole);
        at(membersPage).verifySystemMessage('User was successfully updated.');
    },);
});
