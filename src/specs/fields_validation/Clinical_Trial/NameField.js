/* eslint-disable no-console */
import loginPage from '../../../pages/LoginPage';
import basePage from '../../../pages/BasePage';
import { companyAdminEmail, companyAdminPassword } from '../../../model/Credentials';
import { at } from '../../../utils/PageFactory';
import { openLoginPage } from '../../../functions/BaseFunctions';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import clinicalTrialPage from '../../../pages/ClinicalTrialPage';
import { random1Character, random2Characters, random55Characters, random56Characters } from '../../../model/Constants';
import { ctNameField } from '../../../elements/ClinicalTrialPageElements';
import { clinicalTrialsLink } from '../../../elements/HeaderElements';

describe('Validation rules test cases for CT "Name" field',() => {
    beforeEach('Login as Company Admin user and go to last record in CT list', () => {
        openLoginPage();
        at(loginPage)
            .login(companyAdminEmail, companyAdminPassword);
        at(clinicalTrialsPage)
            .goToHeaderCategory(clinicalTrialsLink)
            .performActionWithLastClinicalTrial('Edit');
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Leave the field empty and press "Submit" -> "Name cannot be blank" message should appear',() => {
        at(clinicalTrialPage)
            .inputToField(ctNameField, '')
            .pressSubmitButton()
            .verifyErrorMessage("Name can't be blank");
    });

    it('Input 56 characters and press "Submit" -> "Name is too long" message should appear',() => {
        at(clinicalTrialPage)
            .inputToField(ctNameField, random56Characters)
            .pressSubmitButton()
            .verifyErrorMessage('Name is too long (maximum is 55 characters)');
    });

    it('Input 1 character and press "Submit" -> "Name is too short" message should appear',() => {
        at(clinicalTrialPage)
            .inputToField(ctNameField,random1Character)
            .pressSubmitButton()
            .verifyErrorMessage('Name is too short (minimum is 2 characters)');
    });

    it('Input 2 characters and press "Submit" -> "Clinical trial was successfully updated." message should appear.\n' +
        'Check that name has changed to just submitted one',() => {
        at(clinicalTrialPage)
            .inputToField(ctNameField,random2Characters)
            .pressSubmitButton();
        at(basePage)
            .verifySystemMessage('Clinical trial was successfully updated.')
            .goToHeaderCategory(clinicalTrialsLink);
        const savedCTName = at(clinicalTrialsPage).getCTName();
        at(clinicalTrialsPage)
            .verifyAttributeEqual(random2Characters, savedCTName);
    });

    it('Input 55 characters and press "Submit" -> "Clinical trial was successfully updated." message should appear.',() => {
        at(clinicalTrialPage)
            .inputToField(ctNameField,random55Characters)
            .pressSubmitButton();
        at(basePage)
            .verifySystemMessage('Clinical trial was successfully updated.');
    });

    it('Input 3 spaces and press "Submit" -> "Name cannot be blank" message should appear',() => {
        at(clinicalTrialPage)
            .inputToField(ctNameField, '   ')
            .pressSubmitButton()
            .verifyErrorMessage("Name can't be blank");
    });

    it('Input 4 characters, 1 space, 3 figures, 2 special symbols and press "Submit" -> \n' +
        '"Clinical trial was successfully updated" message should appear',() => {
        at(clinicalTrialPage)
            .inputToField(ctNameField, 'abcd 123%&')
            .pressSubmitButton();
        at(basePage)
            .verifySystemMessage('Clinical trial was successfully updated.');
    });

    // it('Input 2 special symbols and press "Submit" -> "Company name should contain letter" message should appear',() => {
    //     at(companyPage)
    //         .inputToNameField('%&')
    //         .pressSubmitButton()
    //         .verifyErrorMessage('Company name should contain letter');
    // });
});
