import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import { companyAdminEmail, companyAdminPassword } from '../../../model/Credentials';
import basePage from '../../../pages/BasePage';
import { ctTestObjective, random256Characters, testName } from '../../../model/Constants';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import clinicalTrialPage from '../../../pages/ClinicalTrialPage';
import { ctNameField, ctObjectiveField } from '../../../elements/ClinicalTrialPageElements';
import { clinicalTrialsLink } from '../../../elements/HeaderElements';

describe('Validation rules test cases for Clinical Trial "Objective" field',() => {
    beforeEach('Login as Company Admin user and go to last record in CT list', () => {
        openLoginPage();
        at(loginPage)
            .login(companyAdminEmail, companyAdminPassword);
        at(clinicalTrialsPage)
            .goToHeaderCategory(clinicalTrialsLink)
            .performActionWithLastClinicalTrial('Edit');
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Fill in "Name" and "Objective" fields with a correct data and press "Submit" -> \n' +
        '"Clinical trial was successfully updated." message should appear. \n' +
        'Press "Edit CT" button and check the correct saving', () => {
        at(clinicalTrialPage)
            .inputToField(ctNameField, testName)
            .inputToField(ctObjectiveField, ctTestObjective)
            .pressSubmitButton();
        at(basePage)
            .verifySystemMessage('Clinical trial was successfully updated.')
            .goToHeaderCategory(clinicalTrialsLink);
        at(clinicalTrialsPage).performActionWithLastClinicalTrial('Edit');
        const savedCTObjective = clinicalTrialPage.getCTObjective();
        at(clinicalTrialPage)
            .verifyAttributeDeepEqual(ctTestObjective, savedCTObjective);
    });

    it('Fill in "Name" field with a correct data, leave "Details" field empty and press "Submit" ->\n' +
        '"Clinical trial was successfully updated." message should appear', () => {
        at(clinicalTrialPage)
            .inputToField(ctNameField, testName)
            .inputToField(ctObjectiveField, '')
            .pressSubmitButton();
        at(basePage)
            .verifySystemMessage('Clinical trial was successfully updated.');
    });

    it('Input 256 characters and press "Submit" -> "Objective description is too long (maximum 255 characters)" message should appear',() => {
        at(clinicalTrialPage)
            .inputToField(ctNameField, testName)
            .inputToField(ctObjectiveField, random256Characters)
            .pressSubmitButton()
            .verifyErrorMessage('Objective description is too long (maximum 255 characters)');
    });
});
