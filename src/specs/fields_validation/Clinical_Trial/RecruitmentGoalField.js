import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import { companyAdminEmail, companyAdminPassword } from '../../../model/Credentials';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import basePage from '../../../pages/BasePage';
import clinicalTrialPage from '../../../pages/ClinicalTrialPage';
import { ctRecruitmentGoalField } from '../../../elements/ClinicalTrialPageElements';
import { edgeRecruitmentGoalNumber, random3Figures } from '../../../model/Constants';
import { clinicalTrialsLink } from '../../../elements/HeaderElements';

describe('Validation rules test cases for CT "Recruitment goal" field',() => {
    beforeEach('Login as Company Admin user and go to last record in CT list', () => {
        openLoginPage();
        at(loginPage)
            .login(companyAdminEmail, companyAdminPassword);
        at(clinicalTrialsPage)
            .goToHeaderCategory(clinicalTrialsLink)
            .performActionWithLastClinicalTrial('Edit');
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Leave field empty and all other fields fill with a proper data and press "Submit" ->\n' +
        '"Clinical trial was successfully updated" message should appear', () => {
        at(clinicalTrialPage)
            .inputToField(ctRecruitmentGoalField, '')
            .pressSubmitButton();
        at(basePage)
            .verifySystemMessage('Clinical trial was successfully updated.');
    });

    it('Input correct number (ex. "100"), all other fields fill with a proper data and press "Submit".\n' +
        'Check that "Recruitment Goal" of an edited CT in a CT list is the same as was just submitted', () => {
        at(clinicalTrialPage)
            .inputToField(ctRecruitmentGoalField, random3Figures)
            .pressSubmitButton();
        at(basePage)
            .verifySystemMessage('Clinical trial was successfully updated.')
            .goToHeaderCategory(clinicalTrialsLink);
        at(clinicalTrialsPage)
            .performActionWithLastClinicalTrial('Edit');
        const savedRecruitmentGoal = clinicalTrialPage.getCTRecruitmentGoal();
        at(clinicalTrialPage)
            .verifyAttributeEqual(random3Figures, savedRecruitmentGoal);
    });

    it('Input edge number ("999999999"), all other fields fill with a proper data and press "Submit".\n' +
        'Check that "Recruitment Goal" of an edited CT in a CT list is the same as was just submitted', () => {
        at(clinicalTrialPage)
            .inputToField(ctRecruitmentGoalField, edgeRecruitmentGoalNumber)
            .pressSubmitButton();
        at(basePage)
            .verifySystemMessage('Clinical trial was successfully updated.')
            .goToHeaderCategory(clinicalTrialsLink);
        at(clinicalTrialsPage)
            .performActionWithLastClinicalTrial('Edit');
        const savedRecruitmentGoal = clinicalTrialPage.getCTRecruitmentGoal();
        at(clinicalTrialPage)
            .verifyAttributeEqual(edgeRecruitmentGoalNumber, savedRecruitmentGoal);
    });

    it('Input "-1" and press "Submit" -> \n' +
        '"Recruitment goal must be greater than 0" message should appear ', () => {
        at(clinicalTrialPage)
            .inputToField(ctRecruitmentGoalField, '-1')
            .pressSubmitButton()
            .verifySystemErrorMessage('Recruitment goal must be greater than 0');
    });

    it('Input "0" and press "Submit" -> \n' +
        '"Recruitment goal must be greater than 0" message should appear ', () => {
        at(clinicalTrialPage)
            .inputToField(ctRecruitmentGoalField, '0')
            .pressSubmitButton()
            .verifySystemErrorMessage('Recruitment goal must be greater than 0');
    });

    it('Input "1000000000" and press "Submit" -> \n' +
        '"Recruitment goal must be less than or equal to 999999999" message should appear', () => {
        at(clinicalTrialPage)
            .inputToField(ctRecruitmentGoalField, '1000000000')
            .pressSubmitButton()
            .verifySystemErrorMessage('Recruitment goal must be less than or equal to 999999999');
    });
});
