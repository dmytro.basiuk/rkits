import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import { companyAdminEmail, companyAdminPassword } from '../../../model/Credentials';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import basePage from '../../../pages/BasePage';
import mgPage from '../../../pages/EditMedicalGroupPage';
import { random76Characters, testAddress } from '../../../model/Constants';
import { mgAddressField } from '../../../elements/EditMedicalGroupPageElements';
import medicalGroupsPage from '../../../pages/MedicalGroupsPage';
import { medicalGroupsLink } from '../../../elements/HeaderElements';

describe('Validation rules test cases for medical group Address field',() => {
    beforeEach('Login as Company Admin user and go to "Medical Groups" Page -> Last Record -> Edit', () => {
        openLoginPage();
        at(loginPage)
            .login(companyAdminEmail, companyAdminPassword);
        at(clinicalTrialsPage)
            .goToHeaderCategory(medicalGroupsLink);
        at(medicalGroupsPage).performActionWithLastMedicalGroup('Edit');
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Fill in "Address" fields with a correct data and press "Submit" -> \n' +
        '"Medical Group was successfully updated" message should appear.\n' +
        'Go to "Edit Medical Group" mode and check the correct saving', () => {
        at(mgPage)
            .inputToField(mgAddressField, testAddress)
            .pressSubmitButton();
        at(basePage)
            .verifySystemMessage('Medical group was successfully updated.');
        at(medicalGroupsPage)
            .performActionWithLastMedicalGroup('Edit');
        const savedMGAddress = at(mgPage).getMGAddress();
        at(mgPage).verifyAttributeEqual(testAddress, savedMGAddress);
    });

    it('Leave "Address" field empty and press "Submit" ->\n' +
        '"Company was successfully updated" message should appear', () => {
        at(mgPage)
            .inputToField(mgAddressField, '')
            .pressSubmitButton();
        at(basePage)
            .verifySystemMessage('Medical group was successfully updated.');
    });

    it('Input 76 characters and press "Submit" -> "Address is too long (maximum is 75 characters)" message should appear',() => {
        at(mgPage)
            .inputToField(mgAddressField, random76Characters)
            .pressSubmitButton()
            .verifyErrorMessage('Address is too long (maximum is 75 characters)');
    });
});
