import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import { companyAdminEmail, companyAdminPassword } from '../../../model/Credentials';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import basePage from '../../../pages/BasePage';
import mgPage from '../../../pages/EditMedicalGroupPage';
import companyPage from '../../../pages/CompanyPage';
import { mgDetailsField } from '../../../elements/EditMedicalGroupPageElements';
import { random256Characters, testDetails } from '../../../model/Constants';
import medicalGroupsPage from '../../../pages/MedicalGroupsPage';
import { medicalGroupsLink } from '../../../elements/HeaderElements';

describe('Validation rules test cases for medical group Details field',() => {
    beforeEach('Login as Company Admin user and go to "Medical Groups" Page -> Last Record -> Edit', () => {
        openLoginPage();
        at(loginPage)
            .login(companyAdminEmail, companyAdminPassword);
        at(clinicalTrialsPage)
            .goToHeaderCategory(medicalGroupsLink);
        at(medicalGroupsPage).performActionWithLastMedicalGroup('Edit');
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Fill in "Details" fields with a correct data and press "Submit" -> \n' +
        '"Medical Group was successfully updated" message should appear. \n' +
        'Go to "Edit Medical Group" mode and check the correct saving', () => {
        at(mgPage)
            .inputToField(mgDetailsField, testDetails)
            .pressSubmitButton();
        at(medicalGroupsPage)
            .verifySystemMessage('Medical group was successfully updated.')
            .performActionWithLastMedicalGroup('Edit');
        const savedMGDetails = at(mgPage).getMGDetails();
        at(mgPage).verifyAttributeEqual(testDetails, savedMGDetails);
    });

    it('Leave "Details" field empty and press "Submit" ->\n' +
        '"Medical Group was successfully updated." message should appear', () => {
        at(companyPage)
            .inputToField(mgDetailsField, '')
            .pressSubmitButton();
        at(medicalGroupsPage)
            .verifySystemMessage('Medical group was successfully updated.');
    });

    it('Input 256 characters and press "Submit" -> "Details description is too long (maximum 255 characters)" message should appear',() => {
        at(companyPage)
            .inputToField(mgDetailsField, random256Characters)
            .pressSubmitButton()
            .verifyErrorMessage('Details description is too long (maximum 255 characters)');
    });
});
