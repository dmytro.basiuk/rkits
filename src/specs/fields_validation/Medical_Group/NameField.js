/* eslint-disable no-console */
import loginPage from '../../../pages/LoginPage';
import basePage from '../../../pages/BasePage';
import { companyAdminEmail, companyAdminPassword } from '../../../model/Credentials';
import { at } from '../../../utils/PageFactory';
import { openLoginPage } from '../../../functions/BaseFunctions';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import medicalGroupsPage from '../../../pages/MedicalGroupsPage';
import mgPage from '../../../pages/EditMedicalGroupPage';
import {
    random1Character,
    random2Characters, random55Characters,
    random56Characters,
    random75Characters,
    random76Characters
} from '../../../model/Constants';
import { mgNameField } from '../../../elements/EditMedicalGroupPageElements';
import { medicalGroupsLink } from '../../../elements/HeaderElements';

describe('Validation rules test cases for medical group Name field',() => {
    beforeEach('Login as Company Admin user and go to "Medical Groups" Page -> Last Record -> Edit', () => {
        openLoginPage();
        at(loginPage)
            .login(companyAdminEmail, companyAdminPassword);
        at(clinicalTrialsPage)
            .goToHeaderCategory(medicalGroupsLink);
        at(medicalGroupsPage).performActionWithLastMedicalGroup('Edit');
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Leave the field empty and press "Submit" -> "Name cannot be blank" message should appear',() => {
        at(mgPage)
            .inputToField(mgNameField, '')
            .pressSubmitButton()
            .verifyErrorMessage("Name can't be blank");
    });

    it('Input 56 characters and press "Submit" -> "Name is too long" message should appear',() => {
        at(mgPage)
            .inputToField(mgNameField, random56Characters)
            .pressSubmitButton()
            .verifyErrorMessage('Name is too long (maximum is 55 characters)');
    });

    it('Input 1 character and press "Submit" -> "Name is too short" message should appear',() => {
        at(mgPage)
            .inputToField(mgNameField,random1Character)
            .pressSubmitButton()
            .verifyErrorMessage('Name is too short (minimum is 2 characters)');
    });

    it('Input 2 characters and press "Submit" -> "Medical group was successfully updated." message should appear.\n' +
        'Check that element "div.page-title" has changed to just submitted one',() => {
        at(mgPage)
            .inputToField(mgNameField,random2Characters)
            .pressSubmitButton();
        at(medicalGroupsPage)
            .verifySystemMessage('Medical group was successfully updated.')
            .performActionWithLastMedicalGroup('Edit');
        const savedMedicalGroupTitle = at(mgPage).getMGName();
        at(mgPage)
            .verifyAttributeEqual(random2Characters, savedMedicalGroupTitle);
    });

    it('Input 55 characters and press "Submit" -> "Medical group was successfully updated." message should appear.',() => {
        at(mgPage)
            .inputToField(mgNameField,random55Characters)
            .pressSubmitButton();
        at(basePage)
            .verifySystemMessage('Medical group was successfully updated.');
    });

    it('Input 3 spaces and press "Submit" -> "Name cannot be blank" message should appear',() => {
        at(mgPage)
            .inputToField(mgNameField, '   ')
            .pressSubmitButton()
            .verifyErrorMessage("Name can't be blank");
    });

    it('Input 4 characters, 1 space, 3 figures, 2 special symbols and press "Submit" -> \n' +
        '"Medical group was successfully updated." message should appear',() => {
        at(mgPage)
            .inputToField(mgNameField, 'abcd 123%&')
            .pressSubmitButton();
        at(basePage)
            .verifySystemMessage('Medical group was successfully updated.');
    });

    // it('Input 2 special symbols and press "Submit" -> "MG name should contain letter" message should appear',() => {
    //     at(mgPage)
    //         .inputToNameField('%&')
    //         .pressSubmitButton()
    //         .verifyErrorMessage('Medical group name should contain letter');
    // });
});
