import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import { companyAdminEmail, companyAdminPassword } from '../../../model/Credentials';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import medicalGroupsPage from '../../../pages/MedicalGroupsPage';
import basePage from '../../../pages/BasePage';
import mgPage from '../../../pages/EditMedicalGroupPage';
import { mgNPIField } from '../../../elements/EditMedicalGroupPageElements';
import { random10Characters, testNPINumber } from '../../../model/Constants';
import { medicalGroupsLink } from '../../../elements/HeaderElements';

describe('Validation rules test cases for medical group NPI number field',() => {
    beforeEach('Login as Company Admin user and go to "Medical Groups" Page -> Last Record -> Edit', () => {
        openLoginPage();
        at(loginPage)
            .login(companyAdminEmail, companyAdminPassword);
        at(clinicalTrialsPage)
            .goToHeaderCategory(medicalGroupsLink);
        at(medicalGroupsPage).performActionWithLastMedicalGroup('Edit');
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Input correct NPI number, all other fields fill with a proper data and press "Submit" ->\n' +
        '"Medical group was successfully updated" message should appear.\n' +
        'Check that NPI of a MG which was edited has been changed properly after submitting in MG list  ', () => {
        at(mgPage)
            .inputToField(mgNPIField, testNPINumber)
            .pressSubmitButton();
        at(medicalGroupsPage)
            .verifySystemMessage('Medical group was successfully updated.')
            .performActionWithLastMedicalGroup('Edit');
        const savedNPINumber = at(mgPage).getMGNPINumber();
        at(mgPage).verifyAttributeEqual(testNPINumber, savedNPINumber);
    });

    it('Leave the field empty and all other fields fill with a proper data and press "Submit" -> \n' +
        '"Medical group was successfully updated" message should appear ', () => {
        at(mgPage)
            .inputToField(mgNPIField, '')
            .pressSubmitButton();
        at(medicalGroupsPage)
            .verifySystemMessage('Medical group was successfully updated.');
    });

    it('Input 10 letters and press "Submit" -> "Npi can contain only numbers" message should appear',() => {
        at(mgPage)
            .inputToField(mgNPIField,random10Characters)
            .pressSubmitButton()
            .verifyErrorMessage('Npi can contain only numbers');
    });
});
