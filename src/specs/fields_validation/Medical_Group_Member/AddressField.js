import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import { mgAdminEmail, mgAdminPassword } from '../../../model/Credentials';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import basePage from '../../../pages/BasePage';
import companyPage from '../../../pages/CompanyPage';
import { random76Characters, testAddress } from '../../../model/Constants';
import membersPage from '../../../pages/MembersPage';
import mgMemberPage from '../../../pages/MGMemberPage';
import { MgMemberAddressField } from '../../../elements/MGMemberPageElements';
import { membersLink } from '../../../elements/HeaderElements';

describe('Validation rules test cases for CRO Member Address field',() => {
    beforeEach('Login as Medical Group Admin user and go to "Members" Page -> Last Record -> Edit', () => {
        openLoginPage();
        at(loginPage)
            .login(mgAdminEmail, mgAdminPassword);
        at(clinicalTrialsPage)
            .goToHeaderCategory(membersLink);
        at(membersPage).performActionWithLastMember('Edit');
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Fill in "Address" field with a correct data and press "Submit" -> \n' +
        '"User was successfully updated" message should appear.\n' +
        'Press "Edit" user button and check the correct saving', () => {
        at(mgMemberPage)
            .inputToField(MgMemberAddressField, testAddress)
            .pressSubmitButton();
        at(basePage)
            .verifySystemMessage('Member was successfully updated.');
        at(membersPage)
            .performActionWithLastMember('Edit');
        const savedMGMemberAddress = at(mgMemberPage).getMGMemberAddress();
        at(companyPage).verifyAttributeEqual(testAddress, savedMGMemberAddress);
    });

    it('Leave "Address" field empty and press "Submit" ->\n' +
        '"Company was successfully updated" message should appear', () => {
        at(mgMemberPage)
            .inputToField(MgMemberAddressField, '')
            .pressSubmitButton();
        at(basePage)
            .verifySystemMessage('Member was successfully updated.');
    });

    it('Input 76 characters and press "Submit" -> \n' +
        '"Address is too long (maximum is 75 characters)" message should appear',() => {
        at(mgMemberPage)
            .inputToField(MgMemberAddressField, random76Characters)
            .pressSubmitButton()
            .verifyErrorMessage('Address is too long (maximum is 75 characters)');
    });
});
