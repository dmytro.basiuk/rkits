import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import { mgAdminEmail, mgAdminPassword } from '../../../model/Credentials';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import membersPage from '../../../pages/MembersPage';
import basePage from '../../../pages/BasePage';
import { randomEmail, randomFirstName, randomLastName } from '../../../model/Constants';
import mgMemberPage from '../../../pages/MGMemberPage';
import {
    MgMemberEmailField,
    MgMemberFirstNameField,
    MgMemberLastNameField
} from '../../../elements/MGMemberPageElements';
import { membersLink } from '../../../elements/HeaderElements';

describe('Validation rules test cases for MG Member Email field',() => {
    beforeEach('Login as Medical Group Admin user and go to "Members" Page -> Last Record -> Edit', () => {
        openLoginPage();
        at(loginPage)
            .login(mgAdminEmail, mgAdminPassword);
        at(clinicalTrialsPage)
            .goToHeaderCategory(membersLink);
        at(membersPage).performActionWithLastMember('Edit');
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Fill in "Email" field and all other fields with a correct data and press "Submit" ->\n' +
        '"User was successfully updated" message should appear.\n' +
        'Check that email of the last on a page member has been changed to just submitted one', () => {
        at(mgMemberPage)
            .inputToField(MgMemberFirstNameField, randomFirstName)
            .inputToField(MgMemberLastNameField, randomLastName)
            .inputToField(MgMemberEmailField, randomEmail)
            .pressSubmitButton();
        const savedEmail = at(membersPage).getMGMemberEmail();
        at(membersPage)
            .verifySystemMessage('Member was successfully updated.')
            .verifyAttributeContains(savedEmail, randomEmail);
    });

    it('Leave the field empty, all other fields fill with a proper data and press "Submit" ->\n' +
        '"Email field cannot be blank" message should appear',() => {
        at(mgMemberPage)
            .inputToField(MgMemberFirstNameField,randomFirstName)
            .inputToField(MgMemberLastNameField, randomLastName)
            .inputToField(MgMemberEmailField, '')
            .pressSubmitButton()
            .verifyErrorMessage("Email can't be blank");
    });

    it('Input email address which has been already taken and press "Submit" ->\n' +
        '"Email has already been taken" message should appear',() => {
        at(mgMemberPage)
            .inputToField(MgMemberEmailField, mgAdminEmail)
            .pressSubmitButton()
            .verifyErrorMessage('Email has already been taken');
    });

    it('Input email without @ sign and press "Submit" -> "Email is invalid" message should appear',() => {
        at(mgMemberPage)
            .inputToField(MgMemberEmailField, 'testgmail.com')
            .pressSubmitButton()
            .verifyErrorMessage('Email is invalid');
    });
});
