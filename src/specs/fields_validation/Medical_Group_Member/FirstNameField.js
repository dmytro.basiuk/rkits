import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import { mgAdminEmail, mgAdminPassword } from '../../../model/Credentials';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import membersPage from '../../../pages/MembersPage';
import basePage from '../../../pages/BasePage';
import mgMemberPage from '../../../pages/MGMemberPage';
import {
    random1Character,
    random2Characters,
    random55Characters,
    random56Characters,
    randomFirstName, randomLastName
} from '../../../model/Constants';
import { MgMemberFirstNameField, MgMemberLastNameField } from '../../../elements/MGMemberPageElements';
import { membersLink } from '../../../elements/HeaderElements';

describe('Validation rules test cases for MG Member First Name field',() => {
    beforeEach('Login as Medical Group Admin user and go to "Members" Page -> Last Record -> Edit', () => {
        openLoginPage();
        at(loginPage)
            .login(mgAdminEmail, mgAdminPassword);
        at(clinicalTrialsPage)
            .goToHeaderCategory(membersLink);
        at(membersPage).performActionWithLastMember('Edit');
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Leave the field empty, all other fields fill with a proper data and press "Submit" ->\n' +
        '"First Name field cannot be blank" message should appear',() => {
        at(mgMemberPage)
            .inputToField(MgMemberFirstNameField,'')
            .inputToField(MgMemberLastNameField, randomLastName)
            .pressSubmitButton()
            .verifyErrorMessage("First name can't be blank");
    });

    it('Input 1 character and press "Submit" ->\n' +
        ' "First name is too short (minimum is 2 characters)" message should appear',() => {
        at(mgMemberPage)
            .inputToField(MgMemberFirstNameField, random1Character)
            .inputToField(MgMemberLastNameField, randomLastName)
            .pressSubmitButton()
            .verifyErrorMessage('First name is too short (minimum is 2 characters)');
    });

    it('Input 56 characters and press "Submit" -> \n' +
        '"First name is too long (maximum is 55 characters)" message should appear',() => {
        at(mgMemberPage)
            .inputToField(MgMemberFirstNameField, random56Characters)
            .inputToField(MgMemberLastNameField, randomLastName)
            .pressSubmitButton()
            .verifyErrorMessage('First name is too long (maximum is 55 characters)');
    });

    it('Input 2 characters and press "Submit" -> "User was successfully updated" message should appear.\n' +
        'Check that the name of the last on a page member has been changed to just submitted one',() => {
        at(mgMemberPage)
            .inputToField(MgMemberFirstNameField, random2Characters)
            .inputToField(MgMemberLastNameField, randomLastName)
            .pressSubmitButton();
        const savedMemberName = at(membersPage).getMemberFirstName();
        at(membersPage)
            .verifySystemMessage('Member was successfully updated.')
            .verifyAttributeContains(savedMemberName, random2Characters);
    });

    it('Input 55 characters and press "Submit" -> "User was successfully updated" message should appear',() => {
        at(mgMemberPage)
            .inputToField(MgMemberFirstNameField, random55Characters)
            .inputToField(MgMemberLastNameField, randomLastName)
            .pressSubmitButton();
        at(membersPage)
            .verifySystemMessage('Member was successfully updated.');
    });

    it('Input 3 spaces and press "Submit" -> \n' +
        '"First name field cannot be blank" message should appear',() => {
        at(mgMemberPage)
            .inputToField(MgMemberFirstNameField, '   ')
            .inputToField(MgMemberLastNameField, randomLastName)
            .pressSubmitButton()
            .verifyErrorMessage("First name can't be blank");
    });
});
