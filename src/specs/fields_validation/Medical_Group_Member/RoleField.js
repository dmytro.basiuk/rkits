import { capitalizeFirstLetter, openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import { mgAdminEmail, mgAdminPassword } from '../../../model/Credentials';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import membersPage from '../../../pages/MembersPage';
import basePage from '../../../pages/BasePage';
import mgMemberPage from '../../../pages/MGMemberPage';
import { membersLink } from '../../../elements/HeaderElements';

describe('Validation rules test cases for CRO Member Role field',() => {
    beforeEach('Login as Medical Group Admin user and go to "Members" Page -> Last Record -> Edit', () => {
        openLoginPage();
        at(loginPage)
            .login(mgAdminEmail, mgAdminPassword);
        at(clinicalTrialsPage)
            .goToHeaderCategory(membersLink);
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it("Get Last Member's role, go to -> Last Record -> Edit -> change the role, press 'Submit' button, \n" +
        'check that role is changed, then switch the role back', () => {
        const initialMemberRole = at(membersPage).getMgMemberRole();
        at(membersPage).performActionWithLastMember('Edit');
        at(mgMemberPage).changeMgMemberRole();
        at(membersPage).verifySystemMessage('Member was successfully updated.');
        const changedMemberRole = at(membersPage).getMgMemberRole();
        at(membersPage).checkMemberAttributeIsDifferent(initialMemberRole, changedMemberRole);
        //Switching Role back to initial status
        at(membersPage).performActionWithLastMember('Edit');
        const capitalizedInitialRole = capitalizeFirstLetter(initialMemberRole);
        at(mgMemberPage).changeToInitialMgMemberRole(capitalizedInitialRole);
        at(mgMemberPage).verifySystemMessage('Member was successfully updated.');
    },);
});
