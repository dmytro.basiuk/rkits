import { capitalizeFirstLetter, openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import { mgAdminEmail, mgAdminPassword } from '../../../model/Credentials';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import membersPage from '../../../pages/MembersPage';
import basePage from '../../../pages/BasePage';
import mgMemberPage from '../../../pages/MGMemberPage';
import { membersLink } from '../../../elements/HeaderElements';

describe('Validation rules test cases for MG Member Staff type field',() => {
    beforeEach('Login as Medical Group Admin user and go to "Members" Page -> Last Record -> Edit', () => {
        openLoginPage();
        at(loginPage)
            .login(mgAdminEmail, mgAdminPassword);
        at(clinicalTrialsPage)
            .goToHeaderCategory(membersLink);
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it("Get Last Member's 'Staff type', go to -> Last Record -> Edit -> change the Type, press 'Submit' button, \n" +
        'check that type is changed, then switch the type back', () => {
        const initialMemberStaffType = at(membersPage).getMgMemberStaffType();
        at(membersPage).performActionWithLastMember('Edit');
        at(mgMemberPage).changeMGMemberStaffType();
        at(membersPage).verifySystemMessage('Member was successfully updated.');
        const changedMemberStaffType = at(membersPage).getMgMemberStaffType();
        at(membersPage).checkMemberAttributeIsDifferent(initialMemberStaffType, changedMemberStaffType);
        //Switching Staff type back to initial status
        at(membersPage).performActionWithLastMember('Edit');
        const capitalizedInitialType = capitalizeFirstLetter(initialMemberStaffType);
        at(mgMemberPage).changeToInitialStaffType(capitalizedInitialType);
        at(membersPage).verifySystemMessage('Member was successfully updated.');
    },);
});
