import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import { mgAdminEmail, mgAdminPassword } from '../../../model/Credentials';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import basePage from '../../../pages/BasePage';
import companyPage from '../../../pages/CompanyPage';
import { random76Characters, testAddress } from '../../../model/Constants';
import { PatientAddressField } from '../../../elements/PatientPageElements';
import patientsPage from '../../../pages/PatientsPage';
import patientPage from '../../../pages/PatientPage';
import { patientsLink } from '../../../elements/HeaderElements';

describe('Validation rules test cases for Patient Address field',() => {
    beforeEach('Login as Medical Group Admin user and go to "Patients" Page -> Last Record -> Edit', () => {
        openLoginPage();
        at(loginPage)
            .login(mgAdminEmail, mgAdminPassword);
        at(clinicalTrialsPage)
            .goToHeaderCategory(patientsLink);
        at(patientsPage).performActionWithLastPatient('Edit');
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Fill in "Address" field with a correct data and press "Submit" -> \n' +
        '"Patient was successfully updated" message should appear.\n' +
        'Press "Edit" user button and check the correct saving', () => {
        at(patientPage)
            .inputToField(PatientAddressField, testAddress)
            .pressSubmitButton();
        at(basePage)
            .verifySystemMessage('Patient was successfully updated.');
        at(patientsPage)
            .performActionWithLastPatient('Edit');
        const savedPatientAddress = at(patientPage).getPatientAddress();
        at(companyPage).verifyAttributeEqual(testAddress, savedPatientAddress);
    });

    it('Leave "Address" field empty and press "Submit" ->\n' +
        '"Company was successfully updated" message should appear', () => {
        at(patientPage)
            .inputToField(PatientAddressField, '')
            .pressSubmitButton();
        at(basePage)
            .verifySystemMessage('Patient was successfully updated.');
    });

    it('Input 76 characters and press "Submit" -> \n' +
        '"Address is too long (maximum is 75 characters)" message should appear',() => {
        at(patientPage)
            .inputToField(PatientAddressField, random76Characters)
            .pressSubmitButton()
            .verifyErrorMessage('Address is too long (maximum is 75 characters)');
    });
});
