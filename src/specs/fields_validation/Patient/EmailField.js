import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import { mgAdminEmail, mgAdminPassword } from '../../../model/Credentials';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import basePage from '../../../pages/BasePage';
import { randomEmail } from '../../../model/Constants';
import patientsPage from '../../../pages/PatientsPage';
import patientPage from '../../../pages/PatientPage';
import { PatientEmailField } from '../../../elements/PatientPageElements';
import { patientsLink } from '../../../elements/HeaderElements';

describe('Validation rules test cases for Patient Email field',() => {
    beforeEach('Login as Medical Group Admin user and go to "Patients" Page -> Last Record -> Edit', () => {
        openLoginPage();
        at(loginPage)
            .login(mgAdminEmail, mgAdminPassword);
        at(clinicalTrialsPage)
            .goToHeaderCategory(patientsLink);
        at(patientsPage).performActionWithLastPatient('Edit');
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Fill in "Email" field and all other fields with a correct data and press "Submit" ->\n' +
        '"Patient was successfully updated" message should appear.\n' +
        'Check that email of the last on a page member has been changed to just submitted one', () => {
        at(patientPage)
            .inputToField(PatientEmailField, randomEmail)
            .pressSubmitButton();
        const savedEmail = at(patientsPage).getPatientEmail();
        at(patientsPage)
            .verifySystemMessage('Patient was successfully updated.')
            .verifyAttributeContains(savedEmail, randomEmail);
    });

    it('Leave the field empty, all other fields fill with a proper data and press "Submit" ->\n' +
        '"Email field cannot be blank" message should appear',() => {
        at(patientPage)
            .inputToField(PatientEmailField, '')
            .pressSubmitButton()
            .verifyErrorMessage("Email can't be blank");
    });

    it('Input email address which has been already taken and press "Submit" ->\n' +
        '"Email has already been taken" message should appear',() => {
        at(patientPage)
            .inputToField(PatientEmailField, mgAdminEmail)
            .pressSubmitButton()
            .verifyErrorMessage('Email has already been taken');
    });

    it('Input email without @ sign and press "Submit" -> "Email is invalid" message should appear',() => {
        at(patientPage)
            .inputToField(PatientEmailField, 'testgmail.com')
            .pressSubmitButton()
            .verifyErrorMessage('Email is invalid');
    });
});
