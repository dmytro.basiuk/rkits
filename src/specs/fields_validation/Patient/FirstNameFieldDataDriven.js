import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import { mgAdminEmail, mgAdminPassword } from '../../../model/Credentials';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import patientsPage from '../../../pages/PatientsPage';
import basePage from '../../../pages/BasePage';
import patientPage from '../../../pages/PatientPage';
import { random1Character, random2Characters, random55Characters, random56Characters } from '../../../model/Constants';
import { PatientFirstNameField } from '../../../elements/PatientPageElements';
import { patientsLink } from '../../../elements/HeaderElements';

//Leave the field empty, all other fields fill with a proper data and press "Submit" -> "First Name/Last Name field can't be blank" message should appear
// Input 1 character and press "Submit" -> "First name/Last name is too short (minimum is 2 characters)" message should appear
// Input 56 characters and press "Submit" -> "First name/Last name is too long (maximum is 55 characters)" message should appear
// Input 2 characters and press "Submit" -> "Patient was successfully updated" message should appear. Check that the name of the last on a page member has been changed to just submitted one
// Input 55 characters and press "Submit" -> "Patient was successfully updated" message should appear
// Input 3 spaces and press "Submit" -> "First Name/Last Name field cannot be blank" message should appear

const inputs = [
    {
        value: '',
        expectedError: "First name can't be blank"
    },
    {
        value: random1Character,
        expectedError: 'First name is too short (minimum is 2 characters)'
    },
    {
        value: random56Characters,
        expectedError: 'First name is too long (maximum is 55 characters)'
    },
    {
        value: random2Characters
    },
    {
        value: random55Characters
    },
    {
        value: '   ',
        expectedError: "First name can't be blank"
    }
];

describe('Validation rules test cases for Patient First Name field',() => {

    inputs.forEach(function (input) {
        let testName = input.value;

        if (input.expectedError) {
            testName += ` should show error: ${input.expectedError}`;
        } else {
            testName += ' should pass validation';
        }

        it(testName, function() {
            openLoginPage();
            at(loginPage)
                .login(mgAdminEmail, mgAdminPassword);
            at(clinicalTrialsPage)
                .goToHeaderCategory(patientsLink);
            at(patientsPage).performActionWithLastPatient('Edit');
            at(patientPage)
                .inputToField(PatientFirstNameField, input.value)
                .pressSubmitButton();
            if (input.expectedError) {
                at(patientPage).verifyErrorMessage(input.expectedError);
            } else {
                at(patientsPage).verifySystemMessage('Patient was successfully updated.');
            }
            at(basePage).cleanBrowserSession();
        });
    });
});
