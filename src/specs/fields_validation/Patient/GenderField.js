import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import { mgAdminEmail, mgAdminPassword } from '../../../model/Credentials';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import basePage from '../../../pages/BasePage';
import patientsPage from '../../../pages/PatientsPage';
import patientPage from '../../../pages/PatientPage';
import { PatientFemaleGender, PatientMaleGender } from '../../../elements/PatientPageElements';
import { patientsLink } from '../../../elements/HeaderElements';

describe('Validation rules test cases for Patient Gender field',() => {
    beforeEach('Login as Medical Group Admin user and go to "Patients" Page -> Last Record -> Edit', () => {
        openLoginPage();
        at(loginPage)
            .login(mgAdminEmail, mgAdminPassword);
        at(clinicalTrialsPage)
            .goToHeaderCategory(patientsLink);
        at(patientsPage).performActionWithLastPatient('Edit');
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it("Select 'Male' and all other fields fill with a proper data and press 'Submit'\n" +
        ".Go to just changed Patient info and check that 'Gender' field has appropriate value selected.\n" +
        'Perform the same actions for Female gender testing', () => {
        at(patientPage)
            .changePatientGenderTo(PatientMaleGender)
            .pressSubmitButton();
        at(patientsPage)
            .verifySystemMessage('Patient was successfully updated.')
            .performActionWithLastPatient('Edit');
        const firstSelectedGenderMale = at(patientPage).getPatientSelectedGender();
        at(patientPage)
            .verifySelectedValue(firstSelectedGenderMale, 'Male')
            .changePatientGenderTo(PatientFemaleGender)
            .pressSubmitButton();
        at(patientsPage)
            .verifySystemMessage('Patient was successfully updated.')
            .performActionWithLastPatient('Edit');
        const secondSelectedGenderFemale = at(patientPage).getPatientSelectedGender();
        at(patientPage).verifySelectedValue(secondSelectedGenderFemale, 'Female');
    },);
});
