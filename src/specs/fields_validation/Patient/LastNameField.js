import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import { mgAdminEmail, mgAdminPassword } from '../../../model/Credentials';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import membersPage from '../../../pages/MembersPage';
import basePage from '../../../pages/BasePage';
import { random1Character, random2Characters, random55Characters, random56Characters } from '../../../model/Constants';
import patientsPage from '../../../pages/PatientsPage';
import patientPage from '../../../pages/PatientPage';
import { PatientLastNameField } from '../../../elements/PatientPageElements';
import { patientsLink } from '../../../elements/HeaderElements';

describe('Validation rules test cases for Patient Last Name field',() => {
    beforeEach('Login as Medical Group Admin user and go to "Patients" Page -> Last Record -> Edit', () => {
        openLoginPage();
        at(loginPage)
            .login(mgAdminEmail, mgAdminPassword);
        at(clinicalTrialsPage)
            .goToHeaderCategory(patientsLink);
        at(patientsPage).performActionWithLastPatient('Edit');
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Leave the field empty, all other fields fill with a proper data and press "Submit" ->\n' +
        '"Last Name cannot be blank" message should appear',() => {
        at(patientPage)
            .inputToField(PatientLastNameField,'')
            .pressSubmitButton()
            .verifyErrorMessage("Last name can't be blank");
    });

    it('Input 1 character and press "Submit" ->\n' +
        'Last name is too short (minimum is 2 characters)" message should appear',() => {
        at(patientPage)
            .inputToField(PatientLastNameField, random1Character)
            .pressSubmitButton()
            .verifyErrorMessage('Last name is too short (minimum is 2 characters)');
    });

    it('Input 56 characters and press "Submit" -> \n' +
        '"Last name is too long (maximum is 55 characters)" message should appear',() => {
        at(patientPage)
            .inputToField(PatientLastNameField, random56Characters)
            .pressSubmitButton()
            .verifyErrorMessage('Last name is too long (maximum is 55 characters)');
    });

    it('Input 2 characters and press "Submit" -> "Patient was successfully updated" message should appear.\n' +
        'Check that the name of the last on a page member has been changed to just submitted one',() => {
        at(patientPage)
            .inputToField(PatientLastNameField, random2Characters)
            .pressSubmitButton();
        const savedPatientName = at(patientsPage).getPatientName();
        at(patientsPage)
            .verifySystemMessage('Patient was successfully updated.')
            .verifyAttributeContains(savedPatientName, random2Characters);
    });

    it('Input 55 characters and press "Submit" -> "Patient was successfully updated" message should appear',() => {
        at(patientPage)
            .inputToField(PatientLastNameField, random55Characters)
            .pressSubmitButton();
        at(membersPage)
            .verifySystemMessage('Patient was successfully updated.');
    });

    it('Input 3 spaces and press "Submit" -> \n' +
        '"Last name field cannot be blank" message should appear',() => {
        at(patientPage)
            .inputToField(PatientLastNameField, '   ')
            .pressSubmitButton()
            .verifyErrorMessage("Last name can't be blank");
    });

});
