import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import { mgAdminEmail, mgAdminPassword } from '../../../model/Credentials';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import basePage from '../../../pages/BasePage';
import { PatientPhoneField } from '../../../elements/PatientPageElements';
import { random23Figures, randomPhoneNumber } from '../../../model/Constants';
import patientsPage from '../../../pages/PatientsPage';
import patientPage from '../../../pages/PatientPage';
import { patientsLink } from '../../../elements/HeaderElements';

describe('Validation rules test cases for Patient Phone field',() => {
    beforeEach('Login as Medical Group Admin user and go to "Patients" Page -> Last Record -> Edit', () => {
        openLoginPage();
        at(loginPage)
            .login(mgAdminEmail, mgAdminPassword);
        at(clinicalTrialsPage)
            .goToHeaderCategory(patientsLink);
        at(patientsPage).performActionWithLastPatient('Edit');
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Leave the field empty, all other fields fill with a proper data and press "Submit" ->\n' +
        '"Patient was successfully updated" message should appear', () => {
        at(patientPage)
            .inputToField(PatientPhoneField, '')
            .pressSubmitButton();
        at(patientsPage)
            .verifySystemMessage('Patient was successfully updated.');
    });

    it('Input 10 figures and press "Submit" -> \n' +
        '"Patient was successfully updated" message should appear" message should appear', () => {
        at(patientPage)
            .inputToField(PatientPhoneField, randomPhoneNumber)
            .pressSubmitButton();
        at(patientsPage)
            .verifySystemMessage('Patient was successfully updated.');
    });

    it('Input 23 figures and press "Submit" -> \n' +
        '"Phone is too long (maximum is 22 characters)" message should appear', () => {
        at(patientPage)
            .inputToField(PatientPhoneField, random23Figures)
            .pressSubmitButton()
            .verifySystemErrorMessage('Phone is too long (maximum is 22 characters)');
    });

    it('Input 3 letters and press "Submit" -> \n' +
        '"Phone can contain only numbers" message should appear', () => {
        at(patientPage)
            .inputToField(PatientPhoneField, 'abc')
            .pressSubmitButton()
            .verifySystemErrorMessage('Phone can contain only numbers');
    });
});
