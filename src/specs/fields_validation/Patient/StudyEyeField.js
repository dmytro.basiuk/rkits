import { capitalizeFirstLetter, openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import { mgAdminEmail, mgAdminPassword } from '../../../model/Credentials';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import basePage from '../../../pages/BasePage';
import patientsPage from '../../../pages/PatientsPage';
import patientPage from '../../../pages/PatientPage';
import { patientsLink } from '../../../elements/HeaderElements';

describe('Validation rules test cases for Patient Study Eye field',() => {
    beforeEach('Login as Medical Group Admin user and go to "Patients" Page -> Last Record -> Edit', () => {
        openLoginPage();
        at(loginPage)
            .login(mgAdminEmail, mgAdminPassword);
        at(clinicalTrialsPage)
            .goToHeaderCategory(patientsLink);
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it("Get Last Patient's 'Study Eye', go to -> Last Record -> Edit -> change the Study Eye, press 'Submit' button, \n" +
        'check that study eye is changed, then switch the study eye back', () => {
        const initialPatientStudyEye = at(patientsPage).getPatientStudyEye();
        at(patientsPage).performActionWithLastPatient('Edit');
        at(patientPage).changePatientStudyEye();
        at(patientsPage).verifySystemMessage('Patient was successfully updated.');
        const changedPatientsStudyEye = at(patientsPage).getPatientStudyEye();
        at(patientsPage).checkPatientAttributeIsDifferent(initialPatientStudyEye, changedPatientsStudyEye);
        //Switching Study Eye to initial status
        at(patientsPage).performActionWithLastPatient('Edit');
        const capitalizedStudyEye = capitalizeFirstLetter(initialPatientStudyEye);
        at(patientPage).changeToInitialStudyEye(capitalizedStudyEye);
        at(patientsPage).verifySystemMessage('Patient was successfully updated.');
    });
});
