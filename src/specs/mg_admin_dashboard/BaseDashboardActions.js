/* eslint-disable no-console */
import { openLoginPage, performModalWindowAction } from '../../functions/BaseFunctions';
import { at } from '../../utils/PageFactory';
import loginPage from '../../pages/LoginPage';
import { mgAdminMainEmail, mgAdminPassword } from '../../model/Credentials';
import dashboardPage from '../../pages/DashboardPage';
import patientsPage from '../../pages/PatientsPage';
import patientPage from '../../pages/PatientPage';
import patientMenuDashboardPage from '../../pages/patientMenu/PatientDashboardPage';
import patientMenuEventPage from '../../pages/patientMenu/PatientEventsPage';
import patientMenuClinicalTrialsPage from '../../pages/patientMenu/PatientClinicalTrialsPage';
import mgMemberPage from '../../pages/MGMemberPage';
import { mainHeaderIcon } from '../../elements/HeaderElements';
import {
    addMGMemberButton,
    addPatientButton,
    firstPatientName,
    firstPatientNotes,
    viewAllPatientsLink
} from '../../elements/DashboardPageElements';
import { backToDashboardButton, viewGrid } from '../../elements/PatientsPageElements';
import { backButton } from '../../elements/MGMemberPageElements';
import {
    patientMenuAssignCTButton,
    patientMenuUnAssignCTButton
} from '../../elements/patientMenu/PatientClinicalTrialsElements';
import basePage from '../../pages/BasePage';
import { PatientNotesField } from '../../elements/PatientPageElements';
import { randomSentence } from '../../model/Constants';
import { notesTextField } from '../../elements/patientMenu/PatientDashboardElements';

describe('Testing of base MG Admin Dashboard actions', () => {

    beforeEach('Login as Medical Group Admin user and checking that at Dashboard Page', () => {
        openLoginPage();
        at(loginPage)
            .login(mgAdminMainEmail, mgAdminPassword);
        dashboardPage.checkingDashboardTitle();
    });

    afterEach('Clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    xit('Check "View All" link and "Back to Dashboard" button', () => {
        dashboardPage.clickTheButton(viewAllPatientsLink);
        patientsPage.checkingPatientsTitle();
        patientsPage.clickTheButton(backToDashboardButton);
        dashboardPage.checkingDashboardTitle();
    });

    xit('Check "Add Patient" and "Add MG Member" buttons', () => {
        dashboardPage.clickTheButton(addPatientButton);
        patientPage.clickTheButton(backButton);
        patientsPage.clickTheButton(backToDashboardButton);
        dashboardPage.clickTheButton(addMGMemberButton);
        mgMemberPage.clickTheButton(backButton);
    });

    it('Check correct Event counters: Actionable (Pending, Processing, Completed)', () => {
        const pendingCounter = dashboardPage.getEventValue('Pending');
        const processingCounter = dashboardPage.getEventValue('Processing');
        const completedCounter = dashboardPage.getEventValue('Completed');
        console.log('pendingCounter, processingCounter, completedCounter', pendingCounter, processingCounter, completedCounter);

        dashboardPage.clickOnEventValue('Pending');
        const patientPending = patientMenuEventPage.getEventsNumber();
        patientMenuEventPage.clickTheButton(mainHeaderIcon);
        dashboardPage.clickOnEventValue('Processing');
        const patientProcessing = patientMenuEventPage.getEventsNumber();
        patientMenuEventPage.clickTheButton(mainHeaderIcon);
        dashboardPage.clickOnEventValue('Completed');
        const patientCompleted = patientMenuEventPage.getEventsNumber();
        console.log('patientPending, patientProcessing, patientCompleted', patientPending, patientProcessing, patientCompleted);

        at(dashboardPage).verifyAttributeEqual(pendingCounter, patientPending)
            .verifyAttributeEqual(processingCounter, patientProcessing)
            .verifyAttributeEqual(completedCounter, patientCompleted);
    });

    xit('Archive one Patient and check on Dashboard \n' +
        'then unarchive this patient and check again on dashboard', () => {
        const patientToArchive = dashboardPage.getPatientName();
        console.log('name of the patient which will be archived', patientToArchive);
        dashboardPage.performingPatientAction('archive');
        performModalWindowAction(browser.alertAccept());

        patientsPage.clickTheButton(mainHeaderIcon);
        const patientNotArchived = dashboardPage.getPatientName();
        at(dashboardPage).verifyAttributeNotEqual(patientToArchive, patientNotArchived);
        console.log('should be 2 different persons', patientToArchive, patientNotArchived);

        dashboardPage.clickTheButton(viewAllPatientsLink);
        dashboardPage.clickTheButton(viewGrid);
        patientsPage.searchingAndPerformingPatientAction(patientToArchive, 'UnArchive');
        performModalWindowAction(browser.alertAccept());
        patientsPage.clickTheButton(mainHeaderIcon);
        const patientUnarchived = dashboardPage.getPatientName();
        at(dashboardPage).verifyAttributeEqual(patientToArchive,patientUnarchived);
        console.log('should be the same person', patientToArchive, patientUnarchived);
    });

    xit('Assign CT check, then unassigning back and check. \n' +
        '(Precondition: there is at least one CT which is curentlynot assigned to current Patient)', () => {
        //assigning CT and checking counter
        const initialAssignedCTs = dashboardPage.getAssignedCTs();
        dashboardPage.assigningCTAction(patientMenuAssignCTButton);
        patientMenuClinicalTrialsPage.clickTheButton(mainHeaderIcon);
        const changedAssignedCTs = dashboardPage.getAssignedCTs();
        at(dashboardPage).verifyAttributeIsGreater(initialAssignedCTs, changedAssignedCTs);
        //unassigning CT and checking counter
        dashboardPage.assigningCTAction(patientMenuUnAssignCTButton);
        patientMenuClinicalTrialsPage.clickTheButton(mainHeaderIcon);
        const changedUnAssignedCTs = dashboardPage.getAssignedCTs();
        at(dashboardPage).verifyAttributeIsGreater(changedUnAssignedCTs, changedAssignedCTs);
    });

    it("Add 'Note' to Patient's profile and verify it on Patient's and main dashboards; \n" +
        "then remove Note and checking it disappearing on Patient's and main dashboards", ()=>{
        //Adding Note and verifications
        dashboardPage.clickTheButton(firstPatientName);
        patientMenuDashboardPage.doPatientDashboardActions('edit');
        at(patientPage).inputToField(PatientNotesField, randomSentence)
            .pressSubmitButton();
        const notesText = patientMenuDashboardPage.getTextOfElement(notesTextField);
        at(patientMenuDashboardPage).verifyAttributeEqual(randomSentence, notesText);
        patientMenuDashboardPage.clickTheButton(mainHeaderIcon);
        at(dashboardPage).elementIsExisting(firstPatientNotes);
        const dashboardNote = dashboardPage.getAttributeTextOfElement(firstPatientNotes, 'data-content');
        at(dashboardPage).verifyAttributeContains(dashboardNote, randomSentence);

        //Removing Note and verifications
        dashboardPage.clickTheButton(firstPatientName);
        patientMenuDashboardPage.doPatientDashboardActions('edit');
        at(patientPage).inputToField(PatientNotesField, '')
            .pressSubmitButton()
            .elementIsNotExisting(notesTextField);
        patientMenuDashboardPage.clickTheButton(mainHeaderIcon);
        at(dashboardPage).elementIsNotExisting(firstPatientNotes);
    });
});
