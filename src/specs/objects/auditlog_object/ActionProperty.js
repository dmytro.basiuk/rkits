import { openLoginPage, performModalWindowAction } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import basePage from '../../../pages/BasePage';
import auditLogPage from '../../../pages/AuditLogPage';
import accountPage from '../../../pages/AccountPage';
import dashboardPage from '../../../pages/DashboardPage';
import { mgAdminMainEmail, mgAdminPassword } from '../../../model/Credentials';
import { randomCTRuleTitle, randomLastName, randomSentence, randomSentence2 } from '../../../model/Constants';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import { auditLogLink, clinicalTrialsLink, patientsLink } from '../../../elements/HeaderElements';
import { auditLogTable } from '../../../elements/AuditLogPageElements';
import { userCurrentPassword, userLastNameField } from '../../../elements/AccountPage';
import ctRulePage from '../../../pages/clinicalTrialMenu/CTRulePage';
import { ruleNameField, submitRuleButton } from '../../../elements/clinicalTrialMenu/CTRulePageElements';
import {
    addRuleButton,
    rulesLibraryButton,
    ruleWasCreatedMessage
} from '../../../elements/clinicalTrialMenu/CTRulesPageElements';
import ctDashboardPage from '../../../pages/clinicalTrialMenu/CTDashboardPage';
import ctEventsPage from '../../../pages/clinicalTrialMenu/CTEventsPage';
import { showLastEventButton } from '../../../elements/clinicalTrialMenu/CTEventsPageElements';
import ctEventPage from '../../../pages/clinicalTrialMenu/CTEventPage';
import {
    eventCommentWasAddedMessage,
    eventWasUpdatedMessage,
    leaveCommentButton,
    modalCommentField,
    modalStatusCommentField,
    modalSubmitButton,
    notActivePriorityButton
} from '../../../elements/clinicalTrialMenu/CTEventPageElements';
import { firstCT, secondCT } from '../../../elements/ClinicalTrialsPageElements';
import { myUnprocessedEventsNumber } from '../../../elements/clinicalTrialMenu/CTDashboardPageElements';
import patientsPage from '../../../pages/PatientsPage';
import { viewGrid } from '../../../elements/PatientsPageElements';
import ctRulesPage from '../../../pages/clinicalTrialMenu/CTRulesPage';

describe('Checking the appearance of records in Audit log with different "Actions" ',() => {
    beforeEach('Login as MG Admin', () => {
        openLoginPage();
        at(loginPage).login(mgAdminMainEmail, mgAdminPassword);
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Check "Login" action', () => {
        at(dashboardPage).goToHeaderCategory(auditLogLink);
        const actionTitle = dashboardPage.getTextOfElement(auditLogTable.actionOfLastRecord);
        at(auditLogPage).verifyAttributeEqual(actionTitle, 'Login');
    });

    it('Check "Update profile" action', () => {
        at(dashboardPage).goToAccountPage();
        at(accountPage).inputToField(userLastNameField, randomLastName)
            .inputToField(userCurrentPassword, mgAdminPassword);
        accountPage.pressSubmitButton();
        at(accountPage).goToHeaderCategory(auditLogLink);
        const actionTitle = dashboardPage.getTextOfElement(auditLogTable.actionOfLastRecord);
        at(auditLogPage).verifyAttributeEqual(actionTitle, 'Update profile');
    });

    it('Check "Create" action', () => {
        at(clinicalTrialsPage).goToHeaderCategory(clinicalTrialsLink);
        clinicalTrialsPage.clickTheButton(firstCT);
        ctDashboardPage.goToCTMenuSection('rules');
        ctRulesPage.clickTheButton(rulesLibraryButton);
        ctRulesPage.clickTheButton(addRuleButton);
        at(ctRulePage).inputToField(ruleNameField, randomCTRuleTitle);
        ctRulePage.chooseRecipient();
        ctRulePage.clickTheButton(submitRuleButton);
        at(ctRulePage).verifySystemMessage(ruleWasCreatedMessage)
            .goToHeaderCategory(auditLogLink);
        const actionTitle = dashboardPage.getTextOfElement(auditLogTable.actionOfLastRecord);
        at(auditLogPage).verifyAttributeEqual(actionTitle, 'Create');
    });

    it('Check "Create comment" action', () => {
        at(clinicalTrialsPage).goToHeaderCategory(clinicalTrialsLink);
        clinicalTrialsPage.clickTheButton(secondCT);
        ctDashboardPage.goToCTMenuSection('events');
        ctEventsPage.clickTheButton(showLastEventButton);
        ctEventPage.clickTheButton(leaveCommentButton);
        at(ctEventPage).inputToField(modalCommentField, randomSentence2);
        ctEventPage.clickTheButton(modalSubmitButton);
        at(ctEventPage).verifySystemMessage(eventCommentWasAddedMessage)
            .goToHeaderCategory(auditLogLink);
        const actionLastTitle = dashboardPage.getTextOfElement(auditLogTable.actionOfLastRecord);
        at(auditLogPage).verifyAttributeEqual(actionLastTitle, 'View');
        const actionPreLastTitle = dashboardPage.getTextOfElement(auditLogTable.actionOfPreLastRecord);
        at(auditLogPage).verifyAttributeEqual(actionPreLastTitle, 'Create comment');
    });

    it('Check "Update" action', () => {
        at(clinicalTrialsPage).goToHeaderCategory(clinicalTrialsLink);
        clinicalTrialsPage.clickTheButton(secondCT);
        ctDashboardPage.goToCTMenuSection('events');
        ctEventsPage.clickTheButton(showLastEventButton);
        ctEventPage.clickTheButton(notActivePriorityButton);
        at(ctEventPage).goToHeaderCategory(auditLogLink);
        const actionLastTitle = dashboardPage.getTextOfElement(auditLogTable.actionOfLastRecord);
        at(auditLogPage).verifyAttributeEqual(actionLastTitle, 'Update');
    });

    it('Check "Update status" action', () => {
        at(clinicalTrialsPage).goToHeaderCategory(clinicalTrialsLink);
        clinicalTrialsPage.clickTheButton(secondCT);
        ctDashboardPage.clickTheButton(myUnprocessedEventsNumber);
        ctEventsPage.clickTheButton(showLastEventButton);
        ctEventPage.changingEventStatus('Processing');
        at(ctEventPage).inputToField(modalStatusCommentField, randomSentence);
        ctEventPage.clickTheButton(modalSubmitButton);
        at(ctEventPage).verifySystemMessage(eventWasUpdatedMessage)
            .goToHeaderCategory(auditLogLink);
        const actionLastTitle = dashboardPage.getTextOfElement(auditLogTable.actionOfLastRecord);
        at(auditLogPage).verifyAttributeEqual(actionLastTitle, 'View');
        const actionPreLastTitle = dashboardPage.getTextOfElement(auditLogTable.actionOfPreLastRecord);
        at(auditLogPage).verifyAttributeEqual(actionPreLastTitle, 'Update status');
    });

    it('Check "Archive" and "UnArchive" actions', () => {
        const patientToArchive = dashboardPage.getPatientName();
        dashboardPage.performingPatientAction('archive');
        performModalWindowAction(browser.alertAccept());
        at(dashboardPage).goToHeaderCategory(auditLogLink);
        const actionLastTitle1 = dashboardPage.getTextOfElement(auditLogTable.actionOfLastRecord);
        at(auditLogPage).verifyAttributeEqual(actionLastTitle1, 'Archive');
        at(auditLogPage).goToHeaderCategory(patientsLink);
        patientsPage.clickTheButton(viewGrid);
        patientsPage.searchingAndPerformingPatientAction(patientToArchive, 'UnArchive');
        performModalWindowAction(browser.alertAccept());
        at(dashboardPage).goToHeaderCategory(auditLogLink);
        const actionLastTitle2 = dashboardPage.getTextOfElement(auditLogTable.actionOfLastRecord);
        at(auditLogPage).verifyAttributeEqual(actionLastTitle2, 'Unarchive');
    });
});
