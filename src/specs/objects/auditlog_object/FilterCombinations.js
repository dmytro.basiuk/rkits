import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import basePage from '../../../pages/BasePage';
import auditLogPage from '../../../pages/AuditLogPage';
import dashboardPage from '../../../pages/DashboardPage';
import { mgAdminMainEmail, mgAdminPassword } from '../../../model/Credentials';
import { auditLogLink, headerAccountUserName } from '../../../elements/HeaderElements';
import { auditLogFilters, auditLogTable } from '../../../elements/AuditLogPageElements';
import { filterButton } from '../../../elements/BasePageElements';
import accountPage from '../../../pages/AccountPage';
import { userCurrentPassword, userLastNameField } from '../../../elements/AccountPage';
import { randomLastName } from '../../../model/Constants';

describe('Checking filter combinations on Audit Log page ',() => {
    beforeEach('Login as MG Admin', () => {
        openLoginPage();
        at(loginPage).login(mgAdminMainEmail, mgAdminPassword);
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Check "Who" filter', () => {
        at(dashboardPage).goToHeaderCategory(auditLogLink);
        const userName = dashboardPage.getTextOfElement(headerAccountUserName);
        auditLogPage.clickTheButton(filterButton);
        auditLogPage.filterRecordsInAuditLogByWho(userName);
        const filteredWhoValues = auditLogPage.getTextOfElements(auditLogTable.whoValues);
        auditLogPage.verifyAllValuesEqual(filteredWhoValues,userName);
    });

    it('Check "Who"+"Action->Login" filter', () => {
        at(dashboardPage).goToHeaderCategory(auditLogLink);
        const userName = dashboardPage.getTextOfElement(headerAccountUserName);
        auditLogPage.clickTheButton(filterButton);
        auditLogPage.filterRecordsInAuditLogByWho(userName);
        auditLogPage.filterRecordsInAuditLogByAction('login');
        const filteredRecords = auditLogPage.getTextOfElements(auditLogTable.recordDescription);
        auditLogPage.verifyAllValuesContains(filteredRecords, userName+' logged in');
    });

    it('Check "Who"+"Action->Update Profile"+"Entity" filters', () => {
        at(dashboardPage).goToAccountPage();
        at(accountPage).inputToField(userLastNameField, randomLastName)
            .inputToField(userCurrentPassword, mgAdminPassword);
        accountPage.pressSubmitButton();
        at(accountPage).goToHeaderCategory(auditLogLink);
        const userName = dashboardPage.getTextOfElement(headerAccountUserName);
        auditLogPage.clickTheButton(filterButton);
        auditLogPage.filterRecordsInAuditLogByWho(userName);
        auditLogPage.filterRecordsInAuditLogByAction('login');

        //Providing filter by "Entity->MG Member" together with "Who" and "Action" ->
        // resulting list of available records
        auditLogPage.filterRecordsInAuditLogByEntity('MedicalGroupMember');
        const filteredRecords = auditLogPage.getTextOfElements(auditLogTable.recordDescription);
        auditLogPage.verifyAllValuesContains(filteredRecords, userName+' logged in');

        //Providing filter by "Entity->MG" together with "Who" and "Action" ->
        // resulting "No records found" message
        auditLogPage.filterRecordsInAuditLogByEntity('MedicalGroup');
        at(auditLogPage).elementIsExisting(auditLogFilters.noResultsMessage);
    });

});
