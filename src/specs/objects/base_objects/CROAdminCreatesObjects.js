import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import basePage from '../../../pages/BasePage';
import clinicalTrialPage from '../../../pages/ClinicalTrialPage';
import mgPage from '../../../pages/EditMedicalGroupPage';
import mgsPage from '../../../pages/MedicalGroupsPage';
import { companyAdminEmail, companyAdminPassword } from '../../../model/Credentials';
import {
    randomEmail2,
    randomFirstName2,
    randomLastName2,
    randomPassword8Figures,
    randomClinicalTrialName,
    randomMedicalGroupName
} from '../../../model/Constants';
import { ctNameField, ctWasCreatedMessage } from '../../../elements/ClinicalTrialPageElements';
import {
    CROmemberEmailField,
    CROmemberFirstNameField,
    CROmemberLastNameField, CROmemberPasswordField,
    CROMemberWasCreatedMessage
} from '../../../elements/CROMemberPageElements';
import CROMemberPage from '../../../pages/CROMemberPage';
import { mgNameField } from '../../../elements/EditMedicalGroupPageElements';
import { mgWasCreatedMessage } from '../../../elements/MedicalGroupsPageElements';

describe('Creation of objects by CRO Admin',() => {
    beforeEach('Login as CRO Admin', () => {
        openLoginPage();
        at(loginPage).login(companyAdminEmail, companyAdminPassword);
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Add Clinical Trial with a valid data',() =>{
        at(basePage).selectToAdd('Add Clinical Trial');
        at(clinicalTrialPage).inputToField(ctNameField, randomClinicalTrialName)
            .pressSubmitButton()
            .verifySystemMessage(ctWasCreatedMessage);
    });

    it('Add CRO Member with a valid data', () => {
        at(basePage).selectToAdd('Add CRO Member');
        at(CROMemberPage).inputToField(CROmemberEmailField, randomEmail2)
            .inputToField(CROmemberFirstNameField, randomFirstName2)
            .inputToField(CROmemberLastNameField, randomLastName2)
            .inputToField(CROmemberPasswordField, randomPassword8Figures)
            .pressSubmitButton();
        at(CROMemberPage).verifySystemMessage(CROMemberWasCreatedMessage);
    });

    it('Add Medical Group with a valid data', () => {
        at(basePage).selectToAdd('Add Medical Group');
        at(mgPage).inputToField(mgNameField, randomMedicalGroupName)
            .pressSubmitButton();
        at(mgsPage).verifySystemMessage(mgWasCreatedMessage);
    });

});
