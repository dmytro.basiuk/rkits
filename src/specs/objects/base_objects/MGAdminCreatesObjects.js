import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import basePage from '../../../pages/BasePage';
import patientPage from '../../../pages/PatientPage';
import patientsPage from '../../../pages/PatientsPage';
import mgMemberPage from '../../../pages/MGMemberPage';
import mgMembersPage from '../../../pages/MGMembersPage';
import { mgAdminEmail, mgAdminMainEmail, mgAdminPassword } from '../../../model/Credentials';
import {
    PatientEmailField,
    PatientFirstNameField,
    PatientLastNameField,
    PatientPhoneField
} from '../../../elements/PatientPageElements';
import {
    randomEmail, randomEmail2, randomFirstFemaleName, randomFirstMaleName,
    randomFirstName, randomFirstName2,
    randomLastName, randomLastName2,
    randomPassword8Figures,
    randomPhoneNumber, randomPicture, randomPicture2
} from '../../../model/Constants';
import { patientWasCreatedMessage } from '../../../elements/PatientsPageElements';
import {
    MgMemberEmailField,
    MgMemberFirstNameField,
    MgMemberLastNameField,
    MgMemberPasswordField
} from '../../../elements/MGMemberPageElements';
import { mgMemberCreatedMessage } from '../../../elements/MembersPageElements';

describe('Creation of objects by MG Admin',() => {
    beforeEach('Login as MG Admin', () => {
        openLoginPage();
        at(loginPage).login(mgAdminMainEmail, mgAdminPassword);
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    // for (let i = 1; i <= 100; i++) {
    it('Add Patient with a valid data', () => {
        at(basePage).selectToAdd('Add Patient');
        at(patientPage).inputToField(PatientEmailField, randomEmail)
            .inputToField(PatientFirstNameField, randomFirstFemaleName)
            .inputToField(PatientLastNameField, randomLastName)
            .inputToField(PatientPhoneField, randomPhoneNumber);
        patientPage.attachingFile(`files/femalePhotos/${randomPicture}.jpeg`);
        at(patientPage).pressSubmitButton();
        at(patientsPage).verifySystemMessage(patientWasCreatedMessage);
    });
    // }

    it('Add MG Member with a valid data', () => {
        at(basePage).selectToAdd('Add MG Member');
        at(mgMemberPage).inputToField(MgMemberEmailField, randomEmail2)
            .inputToField(MgMemberFirstNameField, randomFirstMaleName)
            .inputToField(MgMemberLastNameField, randomLastName)
            .inputToField(MgMemberPasswordField, randomPassword8Figures);
        mgMemberPage.attachingFile(`files/malePhotos/${randomPicture2}.jpeg`);
        at(mgMemberPage).pressSubmitButton();
        at(mgMembersPage).verifySystemMessage(mgMemberCreatedMessage);
    });

});
