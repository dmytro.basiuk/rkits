import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import basePage from '../../../pages/BasePage';
import ctDashboardPage from '../../../pages/clinicalTrialMenu/CTDashboardPage';
import ctEventsPage from '../../../pages/clinicalTrialMenu/CTEventsPage';
import ctEventPage from '../../../pages/clinicalTrialMenu/CTEventPage';
import { mgAdminMainEmail, mgAdminPassword, mgMemberEmail, mgMemberPassword } from '../../../model/Credentials';
import { randomSentence } from '../../../model/Constants';
import { firstCT, secondCT } from '../../../elements/ClinicalTrialsPageElements';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import { clinicalTrialsLink } from '../../../elements/HeaderElements';
import { myUnprocessedEventsNumber } from '../../../elements/clinicalTrialMenu/CTDashboardPageElements';
import { showLastEventButton } from '../../../elements/clinicalTrialMenu/CTEventsPageElements';
import {
    actionableDisabledButton,
    actionableEnabledButton,
    eventReminderPendingCurrentOff,
    eventReminderPendingHours,
    eventReminderProcessingCurrentOff,
    eventReminderProcessingHours,
    eventWasUpdatedMessage,
    informativeDisabledButton,
    modalStatusCommentField,
    modalSubmitButton,
    remindersBlock
} from '../../../elements/clinicalTrialMenu/CTEventPageElements';

describe('Test cases for changing type of "Event" object from\n' +
    ' "Informative" to "Actionable" and vice versa',() => {
    beforeEach('Login as MG Admin, go to "MG CT menu" page', () => {
        openLoginPage();
        at(loginPage).login(mgAdminMainEmail, mgAdminPassword);
        at(clinicalTrialsPage).goToHeaderCategory(clinicalTrialsLink);
        clinicalTrialsPage.clickTheButton(secondCT);
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Change Event type from "Actionable" to "Informative" \n' +
        'and check "My Unprocessed Events" number decreasing',() =>{
        const myUnprocessedEventsInitial = ctDashboardPage.getMyUnprocessedEventsNumber();
        ctDashboardPage.clickTheButton(myUnprocessedEventsNumber);
        ctEventsPage.clickTheButton(showLastEventButton);
        ctEventPage.clickTheButton(informativeDisabledButton);
        at(ctEventPage).inputToField(modalStatusCommentField, randomSentence);
        ctEventPage.clickTheButton(modalSubmitButton);
        at(ctEventPage).verifySystemMessage(eventWasUpdatedMessage);
        ctEventPage.goToCTMenuSection('clinical_trials');
        const myUnprocessedEventsChanged = ctDashboardPage.getMyUnprocessedEventsNumber();
        at(ctDashboardPage).verifyAttributeIsGreater(myUnprocessedEventsChanged, myUnprocessedEventsInitial);
    });

    it('Checking that MG Member cannot change Event type', () =>{
        ctDashboardPage.clickTheButton(myUnprocessedEventsNumber);
        ctEventsPage.clickTheButton(showLastEventButton);
        at(ctEventPage).elementIsDisplaying(actionableEnabledButton)
            .loggingOut();
        at(loginPage).login(mgMemberEmail, mgMemberPassword);
        clinicalTrialsPage.clickTheButton(secondCT);
        ctDashboardPage.clickTheButton(myUnprocessedEventsNumber);
        ctEventsPage.clickTheButton(showLastEventButton);
        at(ctEventPage).elementIsNotExisting(actionableEnabledButton);
    });

    it('Checking disappearing of "Reminders" block when changing Event type \n' +
        ' from "Actionable" to "Informative"', () =>{
        ctDashboardPage.clickTheButton(myUnprocessedEventsNumber);
        ctEventsPage.clickTheButton(showLastEventButton);
        at(ctEventPage).elementIsDisplaying(remindersBlock);
        ctEventPage.clickTheButton(informativeDisabledButton);
        at(ctEventPage).inputToField(modalStatusCommentField, randomSentence);
        ctEventPage.clickTheButton(modalSubmitButton);
        at(ctEventPage).verifySystemMessage(eventWasUpdatedMessage)
            .elementIsNotExisting(remindersBlock);
    });

    it('Checking that "Reminders" properly saved and turned off after \n' +
        'changing Event type: "Actionable->Informative->Actionable"', () =>{
        ctDashboardPage.clickTheButton(myUnprocessedEventsNumber);
        ctEventsPage.clickTheButton(showLastEventButton);

        //Getting hours of Reminders and turning it "on" if "off"
        const pendingHoursInit = ctEventPage.getReminderHours(eventReminderPendingHours);
        const processingHoursInit = ctEventPage.getReminderHours(eventReminderProcessingHours);
        at(ctEventPage).turningOnReminders();

        //Changing a type of Event to "Informative"
        ctEventPage.clickTheButton(informativeDisabledButton);
        at(ctEventPage).inputToField(modalStatusCommentField, randomSentence);
        ctEventPage.clickTheButton(modalSubmitButton);
        at(ctEventPage).verifySystemMessage(eventWasUpdatedMessage);

        //Changing a type of Event back to "Actionable"
        ctEventPage.clickTheButton(actionableDisabledButton);
        at(ctEventPage).inputToField(modalStatusCommentField, randomSentence);
        ctEventPage.clickTheButton(modalSubmitButton);
        at(ctEventPage).verifySystemMessage(eventWasUpdatedMessage);

        //Verifying that hours of Reminders left the same
        const pendingHoursSaved = ctEventPage.getReminderHours(eventReminderPendingHours);
        const processingHoursSaved = ctEventPage.getReminderHours(eventReminderProcessingHours);
        at(ctEventPage).verifyAttributeEqual(pendingHoursInit, pendingHoursSaved)
            .verifyAttributeEqual(processingHoursInit, processingHoursSaved)

        //Verifying that Reminders turned "off" after changing Event type "Actionable->Informative->Actionable"
            .elementIsDisplaying(eventReminderPendingCurrentOff)
            .elementIsDisplaying(eventReminderProcessingCurrentOff);
    });

});
