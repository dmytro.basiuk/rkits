import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import basePage from '../../../pages/BasePage';
import ctDashboardPage from '../../../pages/clinicalTrialMenu/CTDashboardPage';
import ctEventsPage from '../../../pages/clinicalTrialMenu/CTEventsPage';
import { mgAdminMainEmail, mgAdminPassword } from '../../../model/Credentials';
import { firstCT, secondCT } from '../../../elements/ClinicalTrialsPageElements';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import { clinicalTrialsLink } from '../../../elements/HeaderElements';
import { actionableEventIcon, filterButton, resetFilterButton } from '../../../elements/BasePageElements';
import { showLastEventButton } from '../../../elements/clinicalTrialMenu/CTEventsPageElements';
import ctEventPage from '../../../pages/clinicalTrialMenu/CTEventPage';
import {
    eventWasUpdatedMessage,
    modalStatusCommentField,
    modalSubmitButton
} from '../../../elements/clinicalTrialMenu/CTEventPageElements';
import { randomSentence } from '../../../model/Constants';
import { myUnprocessedEventsNumber } from '../../../elements/clinicalTrialMenu/CTDashboardPageElements';

describe('Checking functionality of filters on "CT->Events" page',() => {
    beforeEach('Login as MG Admin, go to "MG CT menu -> Events" page', () => {
        openLoginPage();
        at(loginPage).login(mgAdminMainEmail, mgAdminPassword);
        at(clinicalTrialsPage).goToHeaderCategory(clinicalTrialsLink);
        clinicalTrialsPage.clickTheButton(secondCT);
        ctDashboardPage.clickTheButton(myUnprocessedEventsNumber);
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Checking Events filter by "Priority" + "Event Type"', () => {
        //Changing last Event Priority to "High"
        ctEventsPage.putLastEventPriorityHigh();

        //Checking correct filtering by "Priority->Normal"
        //ctEventsPage.clickTheButton(filterButton);
        ctEventsPage.filterBy('priority', 'normal');
        ctEventsPage.verifyingAllPrioritiesAre('Normal');

        //Checking correct filtering by "Priority->Normal" + "Event Type->Actionable/Informative"
        ctEventsPage.filterBy('event_type', 'actionable');
        at(ctEventsPage).elementsAreDisplaying(actionableEventIcon);
        ctEventsPage.filterBy('event_type', 'informative');
        at(ctEventsPage).elementsAreNotExisting(actionableEventIcon);
    });

    it('Checking Events filter by "Status"', () => {
        //Changing last Event status to "Processing"
        ctEventsPage.clickTheButton(showLastEventButton);
        ctEventPage.changingEventStatus('Processing');
        at(ctEventPage).inputToField(modalStatusCommentField, randomSentence);
        ctEventPage.clickTheButton(modalSubmitButton);
        at(ctEventPage).verifySystemMessage(eventWasUpdatedMessage);
        ctDashboardPage.goToCTMenuSection('events');

        //Checking correct filtering by "Status->Processing""
        ctEventsPage.clickTheButton(filterButton);
        ctEventsPage.filterBy('status', 'processing');
        ctEventsPage.verifyingAllStatusesAre('Processing');
        ctEventsPage.clickTheButton(resetFilterButton);

        //Filtering by "Actionable" and
        // Changing last Event status to "Completed"
        ctEventsPage.clickTheButton(filterButton);
        ctEventsPage.filterBy('event_type', 'actionable');
        ctEventsPage.clickTheButton(showLastEventButton);
        ctEventPage.changingEventStatus('Completed');
        at(ctEventPage).inputToField(modalStatusCommentField, randomSentence);
        ctEventPage.clickTheButton(modalSubmitButton);
        at(ctEventPage).verifySystemMessage(eventWasUpdatedMessage);
        ctDashboardPage.goToCTMenuSection('events');

        //Checking correct filtering by "Status->Completed""
        ctEventsPage.clickTheButton(filterButton);
        ctEventsPage.filterBy('status', 'completed');
        ctEventsPage.verifyingAllStatusesAre('Completed');

    });
});
