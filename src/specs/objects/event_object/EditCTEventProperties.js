/* eslint-disable mocha/no-exclusive-tests */
import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import basePage from '../../../pages/BasePage';
import ctDashboardPage from '../../../pages/clinicalTrialMenu/CTDashboardPage';
import ctEventsPage from '../../../pages/clinicalTrialMenu/CTEventsPage';
import ctEventPage from '../../../pages/clinicalTrialMenu/CTEventPage';
import ctRecordPage from '../../../pages/clinicalTrialMenu/CTRecordPage';
import { mgAdminMainEmail, mgAdminPassword, mgMemberEmail, mgMemberPassword } from '../../../model/Credentials';
import { randomSentence, randomSentence2 } from '../../../model/Constants';
import { secondCT } from '../../../elements/ClinicalTrialsPageElements';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import { clinicalTrialsLink, notificationBell } from '../../../elements/HeaderElements';
import { myUnprocessedEventsNumber } from '../../../elements/clinicalTrialMenu/CTDashboardPageElements';
import { showLastEventButton } from '../../../elements/clinicalTrialMenu/CTEventsPageElements';
import {
    ctRecordButton,
    disabledCompleteButton,
    eventCommentWasAddedMessage,
    eventRuleLink,
    eventWasUpdatedMessage,
    leaveCommentButton,
    modalCommentField,
    modalStatusCommentField,
    modalSubmitButton,
    notActivePriorityButton,
    showLastStatusCommentButton
} from '../../../elements/clinicalTrialMenu/CTEventPageElements';
import { ctEventsBlock } from '../../../elements/clinicalTrialMenu/CRRecordPageElements';

describe('Test cases for editing and checking properties of "Event" object"',() => {
    beforeEach('Login as MG Admin, go to "MG CT menu" page', () => {
        openLoginPage();
        at(loginPage).login(mgAdminMainEmail, mgAdminPassword);
        at(clinicalTrialsPage).goToHeaderCategory(clinicalTrialsLink);
        clinicalTrialsPage.clickTheButton(secondCT);
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Change Event status from "Pending" and check "My Unprocessed Events',() =>{
        const myUnprocessedEventsInitial = ctDashboardPage.getMyUnprocessedEventsNumber();
        ctDashboardPage.clickTheButton(myUnprocessedEventsNumber);
        ctEventsPage.clickTheButton(showLastEventButton);
        ctEventPage.changingEventStatus('Processing');
        at(ctEventPage).inputToField(modalStatusCommentField, randomSentence);
        ctEventPage.clickTheButton(modalSubmitButton);
        at(ctEventPage).verifySystemMessage(eventWasUpdatedMessage);
        ctEventPage.goToCTMenuSection('clinical_trials');
        const myUnprocessedEventsChanged = ctDashboardPage.getMyUnprocessedEventsNumber();
        at(ctDashboardPage).verifyAttributeIsGreater(myUnprocessedEventsChanged, myUnprocessedEventsInitial);
    });

    it('Check "Priority" proper changing/saving', () => {
        ctDashboardPage.goToCTMenuSection('events');
        ctEventsPage.clickTheButton(showLastEventButton);
        const notActivePriority = ctEventPage.gettingNotActivePriority();
        ctEventPage.clickTheButton(notActivePriorityButton);
        ctEventPage.goToCTMenuSection('events');
        const savedPriority = ctEventsPage.getLastEventPriority();
        at(ctEventsPage).verifyAttributeEqual(notActivePriority,savedPriority);
    });

    it('Check "Assignee" proper changing/saving', () => {
        const myUnprocessedEventsFirst = ctDashboardPage.getMyUnprocessedEventsNumber();
        ctDashboardPage.clickTheButton(myUnprocessedEventsNumber);
        ctEventsPage.clickTheButton(showLastEventButton);
        ctEventPage.changingEventAssignee();
        at(ctEventPage).verifySystemMessage(eventWasUpdatedMessage);
        ctEventPage.goToCTMenuSection('clinical_trials');
        const myUnprocessedEventsSecond = ctDashboardPage.getMyUnprocessedEventsNumber();
        at(ctDashboardPage).verifyAttributeIsGreater(myUnprocessedEventsSecond, myUnprocessedEventsFirst);
    });

    it('Change status and check comment record in Audit log', () => {
        ctDashboardPage.clickTheButton(myUnprocessedEventsNumber);
        ctEventsPage.clickTheButton(showLastEventButton);
        ctEventPage.changingEventStatus('Processing');
        at(ctEventPage).inputToField(modalStatusCommentField, randomSentence);
        ctEventPage.clickTheButton(modalSubmitButton);
        at(ctEventPage).verifySystemMessage(eventWasUpdatedMessage);
        ctEventPage.clickingLogTab('Audit log');
        ctEventPage.clickTheButton(showLastStatusCommentButton);
        const lastStatusComment = ctEventPage.gettingLastStatusComment();
        at(ctEventPage).verifyAttributeEqual(randomSentence, lastStatusComment);
    });

    it('Leave a comment and check it under "Change log" tab', () => {
        ctDashboardPage.goToCTMenuSection('events');
        ctEventsPage.clickTheButton(showLastEventButton);
        ctEventPage.clickTheButton(leaveCommentButton);
        at(ctEventPage).inputToField(modalCommentField, randomSentence2);
        ctEventPage.clickTheButton(modalSubmitButton);
        at(ctEventPage).verifySystemMessage(eventCommentWasAddedMessage);
        ctEventPage.clickingLogTab('Change log');
        const lastCommentText = ctEventPage.gettingLastComment();
        at(ctEventPage).verifyAttributeEqual(randomSentence2, lastCommentText);
    });

    it('Check Event which was triggered on CT Page', () => {
        ctDashboardPage.goToCTMenuSection('events');
        ctEventsPage.clickTheButton(showLastEventButton);
        const ruleOfEvent = ctEventPage.getTextOfElement(eventRuleLink);
        ctEventPage.clickTheButton(ctRecordButton);
        const ctRecordEventsBlock = ctRecordPage.gettingPageSource(ctEventsBlock);
        at(ctRecordPage).verifyAttributeContains(ctRecordEventsBlock,ruleOfEvent);
    });

    it('Change status from "Pending" to any other and check notification number decreasing \n' +
        'Change status back to "Pending" and check that notification number returned to initial', () => {
        ctDashboardPage.clickTheButton(myUnprocessedEventsNumber);
        ctEventsPage.clickTheButton(showLastEventButton);
        const notificationsNumberInitial = ctEventPage.getNumberOfElementText(notificationBell);
        ctEventPage.changingEventStatus('Processing');
        at(ctEventPage).inputToField(modalStatusCommentField, randomSentence);
        ctEventPage.clickTheButton(modalSubmitButton);
        at(ctEventPage).verifySystemMessage(eventWasUpdatedMessage);
        const notificationsNumberChanged = ctEventPage.getNumberOfElementText(notificationBell);
        at(ctEventPage).verifyAttributeIsGreater(notificationsNumberChanged, notificationsNumberInitial);
        ctEventPage.changingEventStatus('Pending');
        at(ctEventPage).inputToField(modalStatusCommentField, randomSentence);
        ctEventPage.clickTheButton(modalSubmitButton);
        at(ctEventPage).verifySystemMessage(eventWasUpdatedMessage);
        browser.pause(1000);
        const notificationsNumberChanged2 = ctEventPage.getNumberOfElementText(notificationBell);
        at(ctEventPage).verifyAttributeEqual(notificationsNumberInitial, notificationsNumberChanged2);
        at(ctEventPage).verifyAttributeIsGreater(notificationsNumberChanged, notificationsNumberChanged2);
    });

    it('MG User cannot mark "Completed" for Event which is not assigned to him',() => {
        at(ctDashboardPage).loggingOut();
        //Logging in as MG Member
        at(loginPage).login(mgMemberEmail, mgMemberPassword);
        clinicalTrialsPage.clickTheButton(secondCT);
        ctDashboardPage.clickTheButton(myUnprocessedEventsNumber);
        ctEventsPage.clickTheButton(showLastEventButton);
        //Checking the ability to change (assigned to this MG Member) Event status to "Completed"
        at(ctEventPage).elementIsNotExisting(disabledCompleteButton);
        ctEventPage.changingEventStatus('Completed');
        at(ctEventPage).inputToField(modalStatusCommentField, randomSentence);
        ctEventPage.clickTheButton(modalSubmitButton);
        at(ctEventPage).verifySystemMessage(eventWasUpdatedMessage);
        //Assign Event to someone else and checking that "Completed" button becomes disabled
        ctEventPage.changingEventAssignee();
        at(ctEventPage).elementIsExisting(disabledCompleteButton);
    });

});
