import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import basePage from '../../../pages/BasePage';
import ctDashboardPage from '../../../pages/clinicalTrialMenu/CTDashboardPage';
import ctEventsPage from '../../../pages/clinicalTrialMenu/CTEventsPage';
import ctEventPage from '../../../pages/clinicalTrialMenu/CTEventPage';
import { mgAdminMainEmail, mgAdminPassword } from '../../../model/Credentials';
import { randomSentence } from '../../../model/Constants';
import { secondCT } from '../../../elements/ClinicalTrialsPageElements';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import { clinicalTrialsLink } from '../../../elements/HeaderElements';
import { showLastEventButton } from '../../../elements/clinicalTrialMenu/CTEventsPageElements';
import {
    eventReminderPendingCurrentOff,
    eventReminderProcessingCurrentOff,
    eventWasUpdatedMessage,
    markAsInvalidButton,
    modalStatusCommentField,
    modalSubmitButton,
    restoreEventButton
} from '../../../elements/clinicalTrialMenu/CTEventPageElements';
import { myUnprocessedEventsNumber } from '../../../elements/clinicalTrialMenu/CTDashboardPageElements';
import { filterButton } from '../../../elements/BasePageElements';

xdescribe('Checking all "Valid/Invalid" Event functionality + ' +
    '2 UI tests for blur screen while "Invalid" status turned on and turned off',() => {

    // TODO: rerun this test after fixing https://jira.gera-it.com/browse/RKS-517

    //this.retries(2);

    beforeEach('Login as MG Admin, go to "MG CT menu" page', () => {
        openLoginPage();
        at(loginPage).login(mgAdminMainEmail, mgAdminPassword);
        at(clinicalTrialsPage).goToHeaderCategory(clinicalTrialsLink);
        clinicalTrialsPage.clickTheButton(secondCT);
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Change Event status to "Invalid", return to valid and check all main functionality requirements', () => {
        ctDashboardPage.clickTheButton(myUnprocessedEventsNumber);
        ctEventsPage.clickTheButton(showLastEventButton);

        //Changing status of Event to "Invalid"
        ctEventPage.clickTheButton(markAsInvalidButton);
        at(ctEventPage).inputToField(modalStatusCommentField, randomSentence);
        ctEventPage.clickTheButton(modalSubmitButton);
        at(ctEventPage).verifySystemMessage(eventWasUpdatedMessage);

        //Checking that Event page becomes 'blur' after marking Event as 'Invalid'
        browser.checkWindow('Event page should be blur');

        //Press "Events" for CT menu, turn on "Show Invalid?" filter
        // -> last Event in a list should have status "Invalid"
        ctDashboardPage.goToCTMenuSection('events');
        ctEventsPage.clickTheButton(filterButton);
        ctEventsPage.filterBy('event_type', 'actionable');
        ctEventsPage.filterBy('status', 'pending');
        ctEventsPage.switchingInvalidEventsFilters();
        const lastEventStatus = ctEventsPage.getLastEventPriority();
        at(ctEventsPage).verifyAttributeEqual(lastEventStatus, 'Invalid');

        //Restore the Event and check that Reminders turned off
        ctEventsPage.clickTheButton(showLastEventButton);
        ctEventPage.clickTheButton(restoreEventButton);
        at(ctEventPage).inputToField(modalStatusCommentField, randomSentence);
        ctEventPage.clickTheButton(modalSubmitButton);
        at(ctEventPage).verifySystemMessage(eventWasUpdatedMessage)
            .elementIsDisplaying(eventReminderPendingCurrentOff)
            .elementIsDisplaying(eventReminderProcessingCurrentOff);

        //Checking that Event page is not 'blur' after restoring Event
        browser.checkWindow("Event page shouldn't be blur");

        //Press "Events" for CT menu, turn on "Show Invalid?" filter
        // -> last Event in a list should have status "Pending"
        ctDashboardPage.goToCTMenuSection('events');
        ctEventsPage.clickTheButton(filterButton);
        ctEventsPage.filterBy('event_type', 'actionable');
        ctEventsPage.filterBy('status', 'pending');
        ctEventsPage.switchingInvalidEventsFilters();
        const lastEventStatus2 = ctEventsPage.getLastEventStatus();
        at(ctEventsPage).verifyAttributeContains(lastEventStatus2, 'Pending');
    });
});
