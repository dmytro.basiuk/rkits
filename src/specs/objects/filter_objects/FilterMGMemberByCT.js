import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import basePage from '../../../pages/BasePage';
import mgMembersPage from '../../../pages/MGMembersPage';
import mgMemberCTPage from '../../../pages/mgMemberMenu/MGMemberClinicalTrialsPage';
import { mgAdminMainEmail, mgAdminPassword } from '../../../model/Credentials';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import { membersLink } from '../../../elements/HeaderElements';
import {
    resetMGFilterButton,
    upperMemberName,
    upperMemberNumberOfCTAssignedLink
} from '../../../elements/MembersPageElements';
import {
    changeAssignedObjectButton,
    upperCTLink,
    upperCTUnAssignButton
} from '../../../elements/mgMemberMenu/MGMemberClinicalTrialsElements';
import { filterButton } from '../../../elements/BasePageElements';

describe('Checking proper filtering MG Member by CT',() => {
    beforeEach('Login as MG Admin, go to "MG Members" page', () => {
        openLoginPage();
        at(loginPage).login(mgAdminMainEmail, mgAdminPassword);
        at(clinicalTrialsPage).goToHeaderCategory(membersLink);
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('UnAssign MG Member from CT -> check filter by CT; \n' +
        'assign back MG Member to CT -> check filter by CT', () => {
        //Get the number of CT assigned to last (upper) MG Member and click on this number
        const mgMemberName = mgMembersPage.getTextOfElement(upperMemberName);
        //const ctAssignedInitial = mgMembersPage.getTextOfElement(upperMemberNumberOfCTAssignedLink);
        mgMembersPage.clickTheButton(upperMemberNumberOfCTAssignedLink);

        //UnAssign mentioned above MG Member from one of CT and get the title of unAssigned CT
        const ctToUnAssign = mgMemberCTPage.getTextOfElement(upperCTLink);
        mgMemberCTPage.clickTheButton(upperCTUnAssignButton);

        //Return to  to "Members" page and open filter
        at(mgMemberCTPage).goToHeaderCategory(membersLink);
        mgMembersPage.clickTheButton(filterButton);

        //Filter by title of CT which was unAssigned from mentioned above MG Member
        mgMembersPage.filterMGMemberByCT(ctToUnAssign);

        //Mentioned above MG Member shouldn't be within filtered by the CT MG Members
        mgMembersPage.verifyNameNotInAList(mgMemberName);

        // *Postcondition:*
        // Reset filter and assign mentioned above CT to mentioned above MG Member.
        // Apply filter and check that MG Member is within a MG Members list
        mgMembersPage.clickTheButton(resetMGFilterButton);
        mgMembersPage.clickTheButton(upperMemberNumberOfCTAssignedLink);
        mgMemberCTPage.clickTheButton(changeAssignedObjectButton);
        mgMemberCTPage.assignActionCTtoMGMember(ctToUnAssign, 'Assign');
        at(mgMemberCTPage).goToHeaderCategory(membersLink);
        mgMembersPage.clickTheButton(filterButton);
        mgMembersPage.filterMGMemberByCT(ctToUnAssign);
        mgMembersPage.verifyNameInAList(mgMemberName);
    });
});
