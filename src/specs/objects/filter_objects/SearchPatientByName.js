import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import basePage from '../../../pages/BasePage';
import patientsPage from '../../../pages/PatientsPage';
import { mgAdminMainEmail, mgAdminPassword } from '../../../model/Credentials';
import { patientsLink } from '../../../elements/HeaderElements';
import { filterButton } from '../../../elements/BasePageElements';
import dashboardPage from '../../../pages/DashboardPage';
import { randomFirstName, randomLastName } from '../../../model/Constants';
import { noSearchResultsMessage, patientSearchResetButton } from '../../../elements/PatientsPageElements';

describe('Filtering on "Patients" page',() => {
    beforeEach('Login as MG Admin', () => {
        openLoginPage();
        at(loginPage).login(mgAdminMainEmail, mgAdminPassword);
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Search Patient by existing Name -> check result; \n' +
        'search Patient by wrong name -> check "No results" message; reset search', () => {
        const patientToSearch = dashboardPage.getPatientName();
        at(dashboardPage).goToHeaderCategory(patientsLink);
        patientsPage.clickTheButton(filterButton);

        patientsPage.searchingPatientByName(patientToSearch);
        const foundPatient = at(patientsPage).getPatientName();
        at(patientsPage).verifyAttributeEqual(patientToSearch, foundPatient);

        patientsPage.searchingPatientByName(patientToSearch + randomFirstName + randomLastName);
        at(patientsPage).elementIsExisting(noSearchResultsMessage);
        patientsPage.clickTheButton(patientSearchResetButton);
        at(patientsPage).elementIsNotExisting(noSearchResultsMessage);
    });
});
