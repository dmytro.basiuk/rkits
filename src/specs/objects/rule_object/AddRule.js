import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import basePage from '../../../pages/BasePage';
import ctDashboardPage from '../../../pages/clinicalTrialMenu/CTDashboardPage';
import ctRulesPage from '../../../pages/clinicalTrialMenu/CTRulesPage';
import ctRulePage from '../../../pages/clinicalTrialMenu/CTRulePage';
import { mgAdminEmail, mgAdminPassword } from '../../../model/Credentials';
import {
    randomCTRuleTitle,
    randomCTRuleTitle2,
    randomCTRuleTitle3,
    randomCTRuleTitle4
} from '../../../model/Constants';
import { firstCT } from '../../../elements/ClinicalTrialsPageElements';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import { clinicalTrialsLink } from '../../../elements/HeaderElements';
import {
    addRuleButton,
    lastRuleActionable,
    ruleAssigneeCantBeBlankMessage,
    ruleNameIsAlreadyTakenMessage,
    ruleRecipientsCantBeBlankMessage,
    rulesLibraryButton,
    ruleWasCreatedMessage
} from '../../../elements/clinicalTrialMenu/CTRulesPageElements';
import {
    ruleNameField,
    ruleRemindersBlockHidden, ruleVariableRecipientName,
    submitRuleButton
} from '../../../elements/clinicalTrialMenu/CTRulePageElements';

describe('Adding Rule to CT by MG Admin',() => {
    beforeEach('Login as MG Admin, go to "CT->Rules Library" page and press "Add Rule" button', () => {
        openLoginPage();
        at(loginPage).login(mgAdminEmail, mgAdminPassword);
        at(clinicalTrialsPage).goToHeaderCategory(clinicalTrialsLink);
        clinicalTrialsPage.clickTheButton(firstCT);
        ctDashboardPage.goToCTMenuSection('rules');
        ctRulesPage.clickTheButton(rulesLibraryButton);
        ctRulesPage.clickTheButton(addRuleButton);
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Adding Informative Rule to Clinical Trial - positive case',() =>{
        at(ctRulePage).inputToField(ruleNameField, randomCTRuleTitle);
        ctRulePage.chooseRecipient();
        ctRulePage.clickTheButton(submitRuleButton);
        at(ctRulePage).verifySystemMessage(ruleWasCreatedMessage);
    });

    it('Adding Actionable Rule to Clinical Trial - positive case',() =>{
        at(ctRulePage).inputToField(ruleNameField, randomCTRuleTitle2);
        ctRulePage.selectEventType('actionable');
        ctRulePage.chooseRecipient();
        ctRulePage.chooseRuleAssignee();
        ctRulePage.clickTheButton(submitRuleButton);
        at(ctRulePage).verifySystemMessage(ruleWasCreatedMessage)
            .elementIsDisplaying(lastRuleActionable);
    });

    it('Input not unique Rule title, press "Submit" and check \n' +
        '"Name has already been taken" message appearance', () => {
        //adding rule
        at(ctRulePage).inputToField(ruleNameField, randomCTRuleTitle3);
        ctRulePage.chooseRecipient();
        ctRulePage.clickTheButton(submitRuleButton);
        at(ctRulePage).verifySystemMessage(ruleWasCreatedMessage);

        //attempting to add a rule with the same title and checking correct error message appearance
        ctRulesPage.clickTheButton(rulesLibraryButton);
        ctRulesPage.clickTheButton(addRuleButton);
        at(ctRulePage).inputToField(ruleNameField, randomCTRuleTitle3);
        ctRulePage.chooseRecipient();
        ctRulePage.clickTheButton(submitRuleButton);
        at(ctRulePage).verifySystemErrorMessage(ruleNameIsAlreadyTakenMessage);
    });

    it("Don't input Assignee to Actionable Rule, press 'Submit' and check \n" +
        "'Assignee can't be blank' message appearance", () => {
        at(ctRulePage).inputToField(ruleNameField, randomCTRuleTitle);
        ctRulePage.selectEventType('actionable');
        ctRulePage.chooseRecipient();
        ctRulePage.clickTheButton(submitRuleButton);
        at(ctRulePage).verifySystemErrorMessage(ruleAssigneeCantBeBlankMessage);
    });

    it("Don't input Recipients, press 'Submit' and check 'Recipients can't be blank' message appearance", () => {
        at(ctRulePage).inputToField(ruleNameField, randomCTRuleTitle);
        ctRulePage.clickTheButton(submitRuleButton);
        at(ctRulePage).verifySystemErrorMessage(ruleRecipientsCantBeBlankMessage);
    });

    it('Check "Reminders" block appearing when switching to "Actionable" Event type', () => {
        at(ctRulePage).elementIsExisting(ruleRemindersBlockHidden);
        ctRulePage.selectEventType('actionable');
        at(ctRulePage).elementIsNotExisting(ruleRemindersBlockHidden);
    });

    it('Using email variables when creating a Rule', () => {
        ctRulePage.copyPasteEmailVariable(ruleVariableRecipientName);
        const emailSubjectText = ctRulePage.getEmailSubject();
        at(ctRulePage).verifyAttributeEqual(emailSubjectText,ruleVariableRecipientName);
    });

    it('Configuring Reminders of a Rule and checking proper saving', () => {
        at(ctRulePage).inputToField(ruleNameField, randomCTRuleTitle4);
        ctRulePage.selectEventType('actionable');
        ctRulePage.chooseRecipient();
        ctRulePage.chooseRuleAssignee();

        //setting Reminder 'Processing' to 48 hours
        const selectedHours = '48';
        ctRulePage.reminderSelectHours('text-warning', selectedHours);
        //turning off Reminder 'Pending'
        ctRulePage.turnOnOffReminder('text-danger');
        ctRulePage.clickTheButton(submitRuleButton);
        at(ctRulePage).verifySystemMessage(ruleWasCreatedMessage);

        //Checking proper saving
        ctRulesPage.actionLastRule('edit');
        const savedHours = ctRulePage.getReminderSelectedHours('text-warning');
        at(ctRulePage).verifyAttributeContains(savedHours, selectedHours);
        ctRulePage.checkingReminderIsTurnedOff('text-danger');
    });
});
