/* eslint-disable no-console */
import { openLoginPage } from '../../../functions/BaseFunctions';
import { at } from '../../../utils/PageFactory';
import loginPage from '../../../pages/LoginPage';
import basePage from '../../../pages/BasePage';
import ctDashboardPage from '../../../pages/clinicalTrialMenu/CTDashboardPage';
import ctRulesPage from '../../../pages/clinicalTrialMenu/CTRulesPage';
import ctRulePage from '../../../pages/clinicalTrialMenu/CTRulePage';
import { mgAdminEmail, mgAdminPassword } from '../../../model/Credentials';
import { random501Characters, randomSentence } from '../../../model/Constants';
import { firstCT } from '../../../elements/ClinicalTrialsPageElements';
import clinicalTrialsPage from '../../../pages/ClinicalTrialsPage';
import { clinicalTrialsLink } from '../../../elements/HeaderElements';
import {
    ruleCommentEmptyError,
    ruleCommentField,
    ruleCommentSubmitButton,
    ruleCommentTooLongError,
    ruleSuccessActionMessage,
    ruleWasUpdatedMessage
} from '../../../elements/clinicalTrialMenu/CTRulesPageElements';
import { submitRuleButton, submitRuleChangesButton } from '../../../elements/clinicalTrialMenu/CTRulePageElements';

describe('Configuring existing in CT Rule by MG Admin',() => {
    beforeEach('Login as MG Admin, go to "CT->Rules Library" page and press "Add Rule" button', () => {
        openLoginPage();
        at(loginPage).login(mgAdminEmail, mgAdminPassword);
        at(clinicalTrialsPage).goToHeaderCategory(clinicalTrialsLink);
        clinicalTrialsPage.clickTheButton(firstCT);
        ctDashboardPage.goToCTMenuSection('rules');
    });

    afterEach('clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Activation/Deactivation of existing Rule',() => {
        ctRulesPage.actionLastRule('archive');
        at(ctRulesPage).inputToField(ruleCommentField, randomSentence);
        ctRulesPage.clickTheButton(ruleCommentSubmitButton);
        at(ctRulePage).verifySystemMessageContains(ruleSuccessActionMessage);
        ctRulesPage.actionLastRule('archive');
        at(ctRulesPage).inputToField(ruleCommentField, randomSentence);
        ctRulesPage.clickTheButton(ruleCommentSubmitButton);
        at(ctRulePage).verifySystemMessageContains(ruleSuccessActionMessage);
    });

    it("Checking 'Comment can't be blank' message when activating/deactivating Rule",() => {
        ctRulesPage.actionLastRule('archive');
        ctRulesPage.clickTheButton(ruleCommentSubmitButton);
        at(ctRulesPage).verifySystemErrorMessage(ruleCommentEmptyError);
    });

    it("Checking 'Comment is too long' message when activating/deactivating Rule",() => {
        ctRulesPage.actionLastRule('archive');
        at(ctRulesPage).inputToField(ruleCommentField, random501Characters);
        ctRulesPage.clickTheButton(ruleCommentSubmitButton);
        at(ctRulesPage).verifySystemErrorMessage(ruleCommentTooLongError);
    });

    it("Configuring existing Rule; checking 'Comment can't be blank' and \n" +
        "'Comment is too long' messages", () => {
        ctRulesPage.actionLastRule('edit');
        ctRulePage.clickTheButton(submitRuleButton);
        ctRulePage.clickTheButton(submitRuleChangesButton);
        at(ctRulePage).verifySystemErrorMessage(ruleCommentEmptyError)
            .inputToField(ruleCommentField, random501Characters);
        ctRulePage.clickTheButton(ruleCommentSubmitButton);
        at(ctRulePage).verifySystemErrorMessage(ruleCommentTooLongError)
            .inputToField(ruleCommentField, randomSentence);
        ctRulePage.clickTheButton(submitRuleChangesButton);
        at(ctRulesPage).verifySystemMessage(ruleWasUpdatedMessage);
    });
});
