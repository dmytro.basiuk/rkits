/* eslint-disable no-console */
import { openLoginPage, refreshingCurrentPage } from '../../functions/BaseFunctions';
import { at } from '../../utils/PageFactory';
import loginPage from '../../pages/LoginPage';
import { mgAdminMainEmail, mgAdminPassword } from '../../model/Credentials';
import dashboardPage from '../../pages/DashboardPage';
import patientDashboardPage from '../../pages/patientMenu/PatientDashboardPage';
import patientRMPage from '../../pages/patientMenu/PatientRMPage';
import mgMemberMenuAssignedPatientsPage from '../../pages/mgMemberMenu/MGMemberAssignedPatientsPage';
import mgMemberMenuDashboardPage from '../../pages/mgMemberMenu/MGMemberMenuBasePage';
import { firstPatientName } from '../../elements/DashboardPageElements';
import basePage from '../../pages/BasePage';
import { changeButton } from '../../elements/patientMenu/PatientRMPageElements';
import { membersLink } from '../../elements/HeaderElements';
import membersPage from '../../pages/MembersPage';
import { upperMemberName } from '../../elements/MembersPageElements';

describe('Testing "Assigning/UnAssigning Patient to MG Member" functionality', () => {
    beforeEach('Login as MG Admin', () => {
        openLoginPage();
        at(loginPage).login(mgAdminMainEmail, mgAdminPassword);
    });
    afterEach('Clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Assign/unAssign Patient from different profiles', () => {
        //Going to some Patient's "Responsible Members" section
        const patientName = dashboardPage.getFirstPatientName();
        dashboardPage.clickTheButton(firstPatientName);
        patientDashboardPage.goToPatientMenuSection('responsible_members');

        //Assigning some Responsible Member to Patient
        patientDashboardPage.clickTheButton(changeButton);
        const assignedMember = patientRMPage.assigningMGMember();
        const assignedMemberName = patientRMPage.getTextOfElement(assignedMember);

        //Going to the mentioned above Responsible Member page-> "Assigned Patients" section
        patientDashboardPage.clickTheButton('//a[text()="'+assignedMemberName+'"]');
        mgMemberMenuDashboardPage.goToMGMemberMenuSection('Assigned Patients');

        //UnAssigning the mentioned above Patient
        mgMemberMenuAssignedPatientsPage.clickTheButton(changeButton);
        mgMemberMenuAssignedPatientsPage.unAssigningPatient(patientName);

        //Going back to mentioned above Patient "Responsible Members" section
        mgMemberMenuAssignedPatientsPage.returningToMentionedPatientDashboard(patientName);
        patientDashboardPage.goToPatientMenuSection('responsible_members');
        patientDashboardPage.clickTheButton(changeButton);

        //Checking that mentioned above Responsible Manager in not assigned
        patientRMPage.verifyingUnassignedMember(assignedMemberName);
    });

    it('Check "Primary" and "Backup" labels assigning', () => {
        // Going to some Patient's "Responsible Members" section
        const patientName = dashboardPage.getFirstPatientName();
        dashboardPage.clickTheButton(firstPatientName);
        patientDashboardPage.goToPatientMenuSection('responsible_members');

        // Assigning 2 RMs for the Patient and setting one "RM" as "Primary"
        patientDashboardPage.clickTheButton(changeButton);
        const assignedMemberOne = patientRMPage.assigningMGMember();
        const assignedMemberNameOne = patientRMPage.getTextOfElement(assignedMemberOne);
        const assignedMemberTwo = patientRMPage.assigningMGMember();
        const assignedMemberNameTwo = patientRMPage.getTextOfElement(assignedMemberTwo);
        patientRMPage.settingPrimaryMGMember(assignedMemberNameOne);

        // Setting second "RM" as "Primary" -> Check that first one RM is not "Primary" anymore
        patientRMPage.settingPrimaryMGMember(assignedMemberNameTwo);
        patientRMPage.verifyingMemberDoesNotHaveLabel(assignedMemberNameOne, 'primary');

        // Doing the same actions for "Backup" label -> Check that second "RM" has "Backup" label
        patientRMPage.settingBackupMGMember(assignedMemberNameOne);
        patientRMPage.settingBackupMGMember(assignedMemberNameTwo);
        patientRMPage.verifyingMemberHasLabel(assignedMemberNameTwo, 'secondary');

        // Going to the "Assigned Patients" section of MG Member which has "Backup" label as a result of previous step
        patientDashboardPage.clickTheButton('//a[text()="'+assignedMemberNameTwo+'"]');
        mgMemberMenuDashboardPage.goToMGMemberMenuSection('Assigned Patients');

        //Checking that mentioned above MG Member has "Backup" label for a mentioned Patient
        mgMemberMenuAssignedPatientsPage.clickTheButton(changeButton);
        mgMemberMenuAssignedPatientsPage.verifyingPatientHasLabel(patientName, 'secondary');

        //PostCondition: unAssigning both Patients
        mgMemberMenuAssignedPatientsPage.clickTheButton('//a[text()="'+patientName+'"]');
        patientDashboardPage.goToPatientMenuSection('responsible_members');
        mgMemberMenuAssignedPatientsPage.clickTheButton(changeButton);
        patientRMPage.unAssigningMGMember(assignedMemberNameOne);
        patientRMPage.unAssigningMGMember(assignedMemberNameTwo);
        patientRMPage.verifyingUnassignedMember(assignedMemberNameOne);
        patientRMPage.verifyingUnassignedMember(assignedMemberNameTwo);
    });

    it('Checking that labels are not resetting after page refreshing', () => {
        // Going to MG Member -> "Assigned Patients" page and assigning two Patients
        at(dashboardPage).goToHeaderCategory(membersLink);
        membersPage.clickTheButton(upperMemberName);
        mgMemberMenuDashboardPage.goToMGMemberMenuSection('Assigned Patients');
        mgMemberMenuAssignedPatientsPage.clickTheButton(changeButton);
        const assignedPatient1 = mgMemberMenuAssignedPatientsPage.assigningPatient();
        const assignedPatient1Name = mgMemberMenuAssignedPatientsPage.getTextOfElement(assignedPatient1);
        const assignedPatient2 = mgMemberMenuAssignedPatientsPage.assigningPatient();
        const assignedPatient2Name = mgMemberMenuAssignedPatientsPage.getTextOfElement(assignedPatient2);

        // Setting "Primary" label for both Patients
        mgMemberMenuAssignedPatientsPage.settingPrimaryForPatient(assignedPatient1Name);
        mgMemberMenuAssignedPatientsPage.settingPrimaryForPatient(assignedPatient2Name);

        // Refreshing the page
        refreshingCurrentPage();

        //Verifying that both "Primary" labels remain
        mgMemberMenuAssignedPatientsPage.verifyingPatientHasLabel(assignedPatient1Name, 'primary');
        mgMemberMenuAssignedPatientsPage.verifyingPatientHasLabel(assignedPatient2Name, 'primary');

        //PostCondition: unAssigning both Patients
        mgMemberMenuAssignedPatientsPage.unAssigningPatient(assignedPatient1Name);
        mgMemberMenuAssignedPatientsPage.unAssigningPatient(assignedPatient2Name);
        mgMemberMenuAssignedPatientsPage.verifyingUnassignedPatient(assignedPatient1Name);
        mgMemberMenuAssignedPatientsPage.verifyingUnassignedPatient(assignedPatient2Name);
    });

    it('Verify the ability for Patient having only one Responsible Member with a "Primary" label', () =>{
        //Going to some Patient's "Responsible Members" page
        const patientName2 = dashboardPage.getFirstPatientName();
        dashboardPage.clickTheButton(firstPatientName);
        patientDashboardPage.goToPatientMenuSection('responsible_members');

        //Assigning two "RMs" for this Patient
        patientDashboardPage.clickTheButton(changeButton);
        const assignedMember1 = patientRMPage.assigningMGMember();
        const assignedMemberName1 = patientRMPage.getTextOfElement(assignedMember1);
        const assignedMember2 = patientRMPage.assigningMGMember();
        const assignedMemberName2 = patientRMPage.getTextOfElement(assignedMember2);

        // For one of the above two RMs set "Primary" label
        patientRMPage.settingPrimaryMGMember(assignedMemberName1);

        // Going to page of not "Primary" RM from the previous step -> "Assigned Patients" section
        patientDashboardPage.clickTheButton('//a[text()="'+assignedMemberName2+'"]');
        mgMemberMenuDashboardPage.goToMGMemberMenuSection('Assigned Patients');

        // Finding mentioned in previous steps Patient and set "Primary" label for him
        mgMemberMenuAssignedPatientsPage.clickTheButton(changeButton);
        mgMemberMenuAssignedPatientsPage.settingPrimaryForPatient(patientName2);

        //Returning back to the Patient's page -> "Responsible Members" section
        mgMemberMenuAssignedPatientsPage.returningToMentionedPatientDashboard(patientName2);
        patientDashboardPage.goToPatientMenuSection('responsible_members');
        patientDashboardPage.clickTheButton(changeButton);

        //Checking that
        // Not "Primary" RM becomes a "Primary" on a Patient's side
        // and
        // The one RM which was "Primary" becomes "not Primary"
        patientRMPage.verifyingMemberHasLabel(assignedMemberName2, 'primary');
        patientRMPage.verifyingMemberDoesNotHaveLabel(assignedMemberName1, 'primary');

        //PostCondition: unAssigning two assigned MG Members from a Patient
        patientRMPage.unAssigningMGMember(assignedMemberName1);
        patientRMPage.unAssigningMGMember(assignedMemberName2);
        patientRMPage.verifyingUnassignedMember(assignedMemberName1);
        patientRMPage.verifyingUnassignedMember(assignedMemberName2);
    });
});
