/* eslint-disable no-console */
import { openLoginPage } from '../../functions/BaseFunctions';
import { at } from '../../utils/PageFactory';
import loginPage from '../../pages/LoginPage';
import { companyAdminEmail, companyAdminPassword, mgAdminMainEmail, mgAdminPassword } from '../../model/Credentials';
import dashboardPage from '../../pages/DashboardPage';
import patientDashboardPage from '../../pages/patientMenu/PatientDashboardPage';
import patientMenuClinicalTrialsPage from '../../pages/patientMenu/PatientClinicalTrialsPage';
import ctMGPage from '../../pages/clinicalTrialMenu/CTMedicalGroupsPage';
import ctMGPatientsPage from '../../pages/clinicalTrialMenu/CTMGPatientsPage';
import { headerPageTitle } from '../../elements/HeaderElements';
import { patientName } from '../../elements/DashboardPageElements';
import { patientMenuUpperCTTitleLink } from '../../elements/patientMenu/PatientClinicalTrialsElements';
import basePage from '../../pages/BasePage';
import { changeAssignedObjectButton } from '../../elements/mgMemberMenu/MGMemberClinicalTrialsElements';
import clinicalTrialsPage from '../../pages/ClinicalTrialsPage';
import ctDashboardPage from '../../pages/clinicalTrialMenu/CTDashboardPage';

describe('Testing "Active/Not Active" objects functionality', () => {

    afterEach('Clean browser session', () => {
        at(basePage).cleanBrowserSession();
    });

    it('Check functionality of "Active/Not Active" statuses for MG and CT', () => {
        //Logging in as MG Admin, go to some Patient Dashboard and get:
        //Patient's name, his MG and CT titles
        //Logging out
        openLoginPage();
        at(loginPage).login(mgAdminMainEmail, mgAdminPassword);
        dashboardPage.clickTheButton(patientName);
        patientDashboardPage.goToPatientMenuSection('clinical_trials');
        const patientMG = patientMenuClinicalTrialsPage.getTextOfElement(headerPageTitle);
        const patientCT = patientMenuClinicalTrialsPage.getTextOfElement(patientMenuUpperCTTitleLink);
        at(patientMenuClinicalTrialsPage).loggingOut();

        //Logging in as CRO Admin and go to "Patient's CT -> Medical Groups" section
        //and unAssign Patient's MG from CT
        //Logging Out as CRO Admin
        at(loginPage).login(companyAdminEmail, companyAdminPassword);
        clinicalTrialsPage.findCTAndClick(patientCT);
        ctDashboardPage.goToCTMenuSectionAsCRO('Medical Groups');
        ctDashboardPage.clickTheButton(changeAssignedObjectButton);
        ctMGPage.doAssignActionForMG(patientMG, 'UnAssign');

        //Go to "Patient's CT -> MG Patients" page and
        //check that status of this MG in 'Not Active'
        ctMGPage.goToCTMenuSectionAsCRO('MG Patients');
        ctMGPatientsPage.checkStatusOfMG(patientMG, 'Not Active');
        at(ctMGPatientsPage).loggingOut();

        //Logging in as MG Admin from MG which was unassigned from CT
        //and check that the CT has status 'Not Active'
        at(loginPage).login(mgAdminMainEmail, mgAdminPassword);
        dashboardPage.checkStatusOfCT(patientCT, 'Not Active');
        at(dashboardPage).loggingOut();

        //Postcondition: returning everything to initial statuses
        at(loginPage).login(companyAdminEmail, companyAdminPassword);
        clinicalTrialsPage.findCTAndClick(patientCT);
        ctDashboardPage.goToCTMenuSectionAsCRO('Medical Groups');
        ctDashboardPage.clickTheButton(changeAssignedObjectButton);
        ctMGPage.doAssignActionForMG(patientMG, 'Assign');
    });

});
