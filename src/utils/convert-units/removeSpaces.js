// use this function if you need to remove spaces in a string
export default function removeSpaces(value) {
    return value.replace(/\s+/g, '');
}
