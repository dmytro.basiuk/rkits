/**
 * @param num The number to round
 * @param precision The number of decimal places to preserve
 */
export default function roundUp(num, precision) {
    precision = Math.pow(10, precision);
    return Math.round(num * precision) / precision;
}
